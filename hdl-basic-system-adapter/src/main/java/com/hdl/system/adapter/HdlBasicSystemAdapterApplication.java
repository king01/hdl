package com.hdl.system.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HdlBasicSystemAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(HdlBasicSystemAdapterApplication.class, args);
    }

}
