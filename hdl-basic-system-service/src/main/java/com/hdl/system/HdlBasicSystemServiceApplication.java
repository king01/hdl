package com.hdl.system;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableApolloConfig
public class HdlBasicSystemServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(HdlBasicSystemServiceApplication.class, args);
    }

}
