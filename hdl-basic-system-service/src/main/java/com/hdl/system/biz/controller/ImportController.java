package com.hdl.system.biz.controller;

import com.hdl.system.biz.service.ImportService;
import com.hdl.system.constant.ImportCodeEnum;
import com.hdl.system.constant.SystemErrorCode;
import com.nc.framework.core.web.Result;
import com.nc.framework.core.web.ResultHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/import")
public class ImportController extends ResultHelper {


    @Resource
    private Map<String, ImportService> givenTypeHandlerMap;


    @ApiOperation(value = "数据导入",notes = "数据导入")
    @PostMapping("/{type}/import")
    public Result<Void> upload(
            @PathVariable(value = "type") String importType,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        if (null == file) {
            return ResultHelper.error(SystemErrorCode.UPLOAD_FILE_IS_EMPTY);
        }
        ImportCodeEnum importTypeEnum = ImportCodeEnum.valueOf(importType);
        ImportService givenType = givenTypeHandlerMap.get(importTypeEnum.getName());
        givenType.analysis(file.getInputStream());
        return ResultHelper.success();
    }


}
