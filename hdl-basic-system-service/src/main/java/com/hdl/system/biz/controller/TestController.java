package com.hdl.system.biz.controller;

import com.nc.framework.core.web.Result;
import com.nc.framework.core.web.ResultHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController extends ResultHelper {

    @RequestMapping("list")
    public Result<Void> getList() {
        return ResultHelper.success();
    }

}
