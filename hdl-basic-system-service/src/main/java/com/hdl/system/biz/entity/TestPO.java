package com.hdl.system.biz.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName(value = "test_bom")
public class TestPO {

    /**
     * BOM id
     */
    @TableId
    private Long id;

    /**
     * 产品名称
     */
    private String name;

    /**
     * 产品图号
     */
    @TableField(value = "drawing_num")
    private String drawingNum;

    /**
     * 版本
     */
    private String version;

    /**
     * BOM描述
     */
    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDrawingNum() {
        return drawingNum;
    }

    public void setDrawingNum(String drawingNum) {
        this.drawingNum = drawingNum;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public TestPO(Long id, String name, String drawingNum, String version, String content) {
        this.id = id;
        this.name = name;
        this.drawingNum = drawingNum;
        this.version = version;
        this.content = content;
    }

    public TestPO() {
    }

    @Override
    public String toString() {
        return "TestPO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", drawingNum='" + drawingNum + '\'' +
                ", version='" + version + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
