package com.hdl.system.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hdl.system.biz.entity.TestPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TestMapper extends BaseMapper<TestPO> {

    /**
     * 批量插入
     */
    int insertBatch(@Param(value = "list") List<TestPO> list);

    /**
     * 批量更新
     */
    int updateBatch(@Param(value = "list") List<TestPO> list);

    /**
     * 批量删除
     */
    int removeBatchIds(@Param(value = "list") List<Long> id);

}
