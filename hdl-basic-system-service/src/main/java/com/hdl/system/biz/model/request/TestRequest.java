package com.hdl.system.biz.model.request;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class TestRequest {

    @ApiModelProperty(value = "产品名称", required = true)
    @NotNull(message = "产品名称不能为空")
    private String name;

    @ApiModelProperty(value = "产品图号", required = true)
    @NotNull(message = "产品图号不能为空")
    private String drawingNum;

    @ApiModelProperty(value = "版本")
    private String version;

    @ApiModelProperty(value = "BOM描述")
    private String content;

}
