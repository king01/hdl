package com.hdl.system.biz.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

public class TestResponse {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "编号")
    private Long id;

    @ApiModelProperty(value = "产品名称")
    private String name;

    @ApiModelProperty(value = "产品图号")
    private String drawingNum;

    @ApiModelProperty(value = "版本")
    private String version;

    @ApiModelProperty(value = "BOM描述")
    private String content;

}
