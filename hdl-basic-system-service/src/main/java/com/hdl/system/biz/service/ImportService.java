package com.hdl.system.biz.service;

import java.io.InputStream;

public interface ImportService {

    /**
     * excel解析
     */
    void analysis(InputStream in);

}
