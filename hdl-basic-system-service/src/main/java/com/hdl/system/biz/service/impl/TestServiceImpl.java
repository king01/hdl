package com.hdl.system.biz.service.impl;

import com.hdl.system.biz.entity.TestPO;
import com.hdl.system.biz.service.ImportService;
import com.hdl.system.biz.service.TestService;
import com.hdl.system.util.ExcelUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@Service
public class TestServiceImpl implements TestService, ImportService {


    @Override
    public void analysis(InputStream in) {
        List<TestPO> warehouses = ExcelUtils.readExcel(in, TestPO.class);
        Map<String, TestPO> warehouseMap = warehouses.stream().collect(toMap(TestPO::getName, item->item));
        if (CollectionUtils.isEmpty(warehouseMap)){
            return;
        }
    }
}
