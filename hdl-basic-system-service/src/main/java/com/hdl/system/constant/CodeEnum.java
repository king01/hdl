package com.hdl.system.constant;

public enum CodeEnum {

    BC(1001, "白菜"),
    XHS(1002, "西红柿")

    ;

    private CodeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    private int code;

    private String name;

}
