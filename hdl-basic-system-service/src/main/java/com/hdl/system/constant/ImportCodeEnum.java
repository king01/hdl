package com.hdl.system.constant;

public enum ImportCodeEnum {

    GC("GC", "factoryService", "gongchang")
    ;

    ImportCodeEnum(String code, String serviceName, String name){
        this.code = code;
        this.serviceName = serviceName;
        this.name = name;
    };

    private String code;

    private String serviceName;

    private String name;

    public String getCode() {
        return code;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getName() {
        return name;
    }
}
