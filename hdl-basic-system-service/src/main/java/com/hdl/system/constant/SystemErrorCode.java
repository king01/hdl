package com.hdl.system.constant;

import com.nc.framework.core.error.ErrorCode;

public enum SystemErrorCode implements ErrorCode {

    UPLOAD_FILE_IS_EMPTY(10011001, "上传文件为空"),
    ;


    private SystemErrorCode(int status, String message) {
        this.status = status;
        this.message = message;
    }


    private int status;

    /**
     * 响应消息
     */
    private String message;

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
