package com.hdl.system.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger接口文档配置
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("HDL系统管理 - 基础管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hdl.system.biz.controller"))
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("HDL系统管理 - 基础管理 APIs")
                .description("# HDL系统管理 - 基础管理 APIs")
                .termsOfServiceUrl("http://localhost:12001/")
                .license("wangshuai1@nancal.com")
                .version("1.0.0")
                .build();
    }

}
