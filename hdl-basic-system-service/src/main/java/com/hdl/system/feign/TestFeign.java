package com.hdl.system.feign;

import com.hdl.system.hystrix.TestFeignHystrix;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "hdl-basic-system-service", path = "/system-user", fallbackFactory = TestFeignHystrix.class)
public interface TestFeign {



}
