package com.hdl.system.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * excel 工具类
 * @author wang. shuai
 */
public class ExcelUtils {

    private static Logger log = LoggerFactory.getLogger(ExcelUtils.class);

    /**
     * 读取excel
     * @param in     输入流
     * @param tClass 实体类
     */
    public static <T> List<T> readExcel(InputStream in, Class<T> tClass){
        List<T> list = new ArrayList<>();
        // 这里需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        EasyExcel.read(in, tClass, new AnalysisEventListener<T>() {
            // 每解析一条数据都会调用该方法
            @Override
            public void invoke(T entity, AnalysisContext analysisContext) {
                log.info("解析一条Bean对象：{}", JSON.toJSONString(entity));
                list.add(entity);
            }

            // 解析完毕的回调方法
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                log.info("excel文件读取完毕！");
            }
        }).sheet().doRead();

        return list;
    }

    /**
     * 写入Excel
     */
    public static <T> void writeExcel(String path, List<T> list) {
        if (CollectionUtils.isEmpty(list)){
            return;
        }
        T entity = list.get(0);
        EasyExcel.write(path, entity.getClass())
                .sheet().doWrite(list);
    }

}
