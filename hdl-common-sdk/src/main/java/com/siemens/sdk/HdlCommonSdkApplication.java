package com.siemens.sdk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.NoRepositoryBean;

@SpringBootApplication
@EnableCaching
public class HdlCommonSdkApplication {

    public static void main(String[] args) {
        SpringApplication.run(HdlCommonSdkApplication.class, args);
    }

}
