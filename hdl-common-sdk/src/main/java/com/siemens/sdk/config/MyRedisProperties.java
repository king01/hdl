package com.siemens.sdk.config;

/**
 * @ClassName: RedisProperties
 * @Description: redis维护
 * @Author: swang
 * @Date: 2021/7/21 10:46
 */
public interface MyRedisProperties {

    // 字典配置
    public static final String DICT_KEY = "HDL:DICT";
    // 系统配置
    public static final String CONFIG_KEY = "HDL:CONFIG";
}
