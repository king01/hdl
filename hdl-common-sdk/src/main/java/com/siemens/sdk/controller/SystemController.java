package com.siemens.sdk.controller;

import com.siemens.sdk.domain.po.DictTypePO;
import com.siemens.sdk.domain.po.SystemConfigPO;
import com.siemens.sdk.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName: SystemController
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 10:06
 */
@RestController
@RequestMapping("/system")
public class SystemController {

    @Autowired
    private CommonService dictService;

    @RequestMapping("/query")
    @ResponseBody
    public List<DictTypePO> query() {
        return dictService.queryDictList();
    }


    @RequestMapping("/config")
    public List<SystemConfigPO> querySystemConfig() {
       return dictService.querySystemConfig();
    }
}
