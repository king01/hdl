package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 11:00:01 
 */
@Entity
@Table ( name ="t_aps_order" )
public class ApsOrderPO  implements Serializable {

	private static final long serialVersionUID =  2139477486807485046L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 排产编号
	 */
   	@Column(name = "aps_order_no" )
	private String apsOrderNo;

	/**
	 * 订单编号
	 */
   	@Column(name = "order_no" )
	private String orderNo;

	/**
	 * 排产号
	 */
   	@Column(name = "aps_no" )
	private String apsNo;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave_no" )
	private String deliveryWaveNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 订单来源:10021001=内部;10021002=外部
	 */
   	@Column(name = "order_source" )
	private String orderSource;

	/**
	 * 排产单状态:10031001=已排产；10031002=未排产
	 */
   	@Column(name = "aps_order_status" )
	private String apsOrderStatus;

	/**
	 * 交货日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 商品别名
	 */
   	@Column(name = "sku_alias" )
	private String skuAlias;

	/**
	 * 商品规格
	 */
   	@Column(name = "sku_spec" )
	private String skuSpec;

	/**
	 * 满箱交付数量
	 */
   	@Column(name = "full_delivery_quantity" )
	private Long fullDeliveryQuantity;

	/**
	 * 半箱交付数量
	 */
   	@Column(name = "half_delivery_quantity" )
	private Long halfDeliveryQuantity;

	/**
	 * 商品单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 商品单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 优先级:10041001=一般；10041002=高
	 */
   	@Column(name = "priority" )
	private String priority;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getApsOrderNo() {
    return apsOrderNo;
  }

  public void setApsOrderNo(String apsOrderNo) {
    this.apsOrderNo = apsOrderNo;
  }


  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }


  public String getApsNo() {
    return apsNo;
  }

  public void setApsNo(String apsNo) {
    this.apsNo = apsNo;
  }


  public String getDeliveryWaveNo() {
    return deliveryWaveNo;
  }

  public void setDeliveryWaveNo(String deliveryWaveNo) {
    this.deliveryWaveNo = deliveryWaveNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getOrderSource() {
    return orderSource;
  }

  public void setOrderSource(String orderSource) {
    this.orderSource = orderSource;
  }


  public String getApsOrderStatus() {
    return apsOrderStatus;
  }

  public void setApsOrderStatus(String apsOrderStatus) {
    this.apsOrderStatus = apsOrderStatus;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public String getSkuAlias() {
    return skuAlias;
  }

  public void setSkuAlias(String skuAlias) {
    this.skuAlias = skuAlias;
  }


  public String getSkuSpec() {
    return skuSpec;
  }

  public void setSkuSpec(String skuSpec) {
    this.skuSpec = skuSpec;
  }


  public Long getFullDeliveryQuantity() {
    return fullDeliveryQuantity;
  }

  public void setFullDeliveryQuantity(Long fullDeliveryQuantity) {
    this.fullDeliveryQuantity = fullDeliveryQuantity;
  }


  public Long getHalfDeliveryQuantity() {
    return halfDeliveryQuantity;
  }

  public void setHalfDeliveryQuantity(Long halfDeliveryQuantity) {
    this.halfDeliveryQuantity = halfDeliveryQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getPriority() {
    return priority;
  }

  public void setPriority(String priority) {
    this.priority = priority;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
