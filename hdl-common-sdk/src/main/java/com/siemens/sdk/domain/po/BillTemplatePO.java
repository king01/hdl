package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 11:00:01 
 */
@Entity
@Table ( name ="t_bill_template" )
public class BillTemplatePO  implements Serializable {

	private static final long serialVersionUID =  5845596475178864044L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 模板编号
	 */
   	@Column(name = "template_no" )
	private String templateNo;

	/**
	 * 模板名称
	 */
   	@Column(name = "template_name" )
	private String templateName;

	/**
	 * 单据描述
	 */
   	@Column(name = "remark" )
	private String remark;

	/**
	 * 文件名称
	 */
   	@Column(name = "file_name" )
	private String fileName;

	/**
	 * 文件扩展名
	 */
   	@Column(name = "file_ext" )
	private String fileExt;

	/**
	 * 文件大小(单位Byte)
	 */
   	@Column(name = "file_size" )
	private BigDecimal fileSize;

	/**
	 * 文件路径
	 */
   	@Column(name = "file_url" )
	private String fileUrl;

	/**
	 * 是否启用:10011001=启用；10011002=停用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getTemplateNo() {
    return templateNo;
  }

  public void setTemplateNo(String templateNo) {
    this.templateNo = templateNo;
  }


  public String getTemplateName() {
    return templateName;
  }

  public void setTemplateName(String templateName) {
    this.templateName = templateName;
  }


  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }


  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }


  public String getFileExt() {
    return fileExt;
  }

  public void setFileExt(String fileExt) {
    this.fileExt = fileExt;
  }


  public BigDecimal getFileSize() {
    return fileSize;
  }

  public void setFileSize(BigDecimal fileSize) {
    this.fileSize = fileSize;
  }


  public String getFileUrl() {
    return fileUrl;
  }

  public void setFileUrl(String fileUrl) {
    this.fileUrl = fileUrl;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
