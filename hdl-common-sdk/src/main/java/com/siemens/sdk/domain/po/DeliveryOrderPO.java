package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 11:00:01 
 */
@Entity
@Table ( name ="t_delivery_order" )
public class DeliveryOrderPO  implements Serializable {

	private static final long serialVersionUID =  8888462202689407817L;

	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 交付订单号
	 */
   	@Column(name = "delivery_order_no" )
	private String deliveryOrderNo;

	/**
	 * 接收单号
	 */
   	@Column(name = "receive_order_no" )
	private String receiveOrderNo;

	/**
	 * 门店编号
	 */
   	@Column(name = "delivery_shop_no" )
	private String deliveryShopNo;

	/**
	 * 门店名称
	 */
   	@Column(name = "delivery_shop_name" )
	private String deliveryShopName;

	/**
	 * 波次编号
	 */
   	@Column(name = "delivery_wave_no" )
	private String deliveryWaveNo;

	/**
	 * 订单类型：10051001=正常；10051002=补单
	 */
   	@Column(name = "order_type" )
	private String orderType;

	/**
	 * 订单状态：10061001=已下单；10061002=进行中；10061003=已完成
	 */
   	@Column(name = "order_status" )
	private String orderStatus;

	/**
	 * 下单时间
	 */
   	@Column(name = "order_create_time" )
	private Date orderCreateTime;

	/**
	 * 交货周期（天）
	 */
   	@Column(name = "delivery_cycle" )
	private String deliveryCycle;

	/**
	 * 交货日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 商品别名
	 */
   	@Column(name = "sku_alias" )
	private String skuAlias;

	/**
	 * 商品规格
	 */
   	@Column(name = "sku_spec" )
	private String skuSpec;

	/**
	 * 商品单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 商品单位
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 交付数量
	 */
   	@Column(name = "delivery_quantity" )
	private BigDecimal deliveryQuantity;

	/**
	 * 换算后的单位编码
	 */
   	@Column(name = "after_conversion_unit_code" )
	private String afterConversionUnitCode;

	/**
	 * 换算后的单位名称
	 */
   	@Column(name = "after_conversion_unit_name" )
	private String afterConversionUnitName;

	/**
	 * 换算后的数量
	 */
   	@Column(name = "after_conversion_quantity" )
	private BigDecimal afterConversionQuantity;

	/**
	 * 换算后的收货数量
	 */
   	@Column(name = "after_conversion_receiving_quantity" )
	private BigDecimal afterConversionReceivingQuantity;

	/**
	 * 换算比
	 */
   	@Column(name = "conversion_ratio" )
	private BigDecimal conversionRatio;

	/**
	 * 允差值上浮动
	 */
   	@Column(name = "floating_tolerance" )
	private BigDecimal floatingTolerance;

	/**
	 * 允差值下浮动
	 */
   	@Column(name = "lower_tolerance" )
	private BigDecimal lowerTolerance;

	/**
	 * 截单时间
	 */
   	@Column(name = "abort_time" )
	private Date abortTime;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getDeliveryOrderNo() {
    return deliveryOrderNo;
  }

  public void setDeliveryOrderNo(String deliveryOrderNo) {
    this.deliveryOrderNo = deliveryOrderNo;
  }


  public String getReceiveOrderNo() {
    return receiveOrderNo;
  }

  public void setReceiveOrderNo(String receiveOrderNo) {
    this.receiveOrderNo = receiveOrderNo;
  }


  public String getDeliveryShopNo() {
    return deliveryShopNo;
  }

  public void setDeliveryShopNo(String deliveryShopNo) {
    this.deliveryShopNo = deliveryShopNo;
  }


  public String getDeliveryShopName() {
    return deliveryShopName;
  }

  public void setDeliveryShopName(String deliveryShopName) {
    this.deliveryShopName = deliveryShopName;
  }


  public String getDeliveryWaveNo() {
    return deliveryWaveNo;
  }

  public void setDeliveryWaveNo(String deliveryWaveNo) {
    this.deliveryWaveNo = deliveryWaveNo;
  }


  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }


  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }


  public Date getOrderCreateTime() {
    return orderCreateTime;
  }

  public void setOrderCreateTime(Date orderCreateTime) {
    this.orderCreateTime = orderCreateTime;
  }


  public String getDeliveryCycle() {
    return deliveryCycle;
  }

  public void setDeliveryCycle(String deliveryCycle) {
    this.deliveryCycle = deliveryCycle;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public String getSkuAlias() {
    return skuAlias;
  }

  public void setSkuAlias(String skuAlias) {
    this.skuAlias = skuAlias;
  }


  public String getSkuSpec() {
    return skuSpec;
  }

  public void setSkuSpec(String skuSpec) {
    this.skuSpec = skuSpec;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public BigDecimal getDeliveryQuantity() {
    return deliveryQuantity;
  }

  public void setDeliveryQuantity(BigDecimal deliveryQuantity) {
    this.deliveryQuantity = deliveryQuantity;
  }


  public String getAfterConversionUnitCode() {
    return afterConversionUnitCode;
  }

  public void setAfterConversionUnitCode(String afterConversionUnitCode) {
    this.afterConversionUnitCode = afterConversionUnitCode;
  }


  public String getAfterConversionUnitName() {
    return afterConversionUnitName;
  }

  public void setAfterConversionUnitName(String afterConversionUnitName) {
    this.afterConversionUnitName = afterConversionUnitName;
  }


  public BigDecimal getAfterConversionQuantity() {
    return afterConversionQuantity;
  }

  public void setAfterConversionQuantity(BigDecimal afterConversionQuantity) {
    this.afterConversionQuantity = afterConversionQuantity;
  }


  public BigDecimal getAfterConversionReceivingQuantity() {
    return afterConversionReceivingQuantity;
  }

  public void setAfterConversionReceivingQuantity(BigDecimal afterConversionReceivingQuantity) {
    this.afterConversionReceivingQuantity = afterConversionReceivingQuantity;
  }


  public BigDecimal getConversionRatio() {
    return conversionRatio;
  }

  public void setConversionRatio(BigDecimal conversionRatio) {
    this.conversionRatio = conversionRatio;
  }


  public BigDecimal getFloatingTolerance() {
    return floatingTolerance;
  }

  public void setFloatingTolerance(BigDecimal floatingTolerance) {
    this.floatingTolerance = floatingTolerance;
  }


  public BigDecimal getLowerTolerance() {
    return lowerTolerance;
  }

  public void setLowerTolerance(BigDecimal lowerTolerance) {
    this.lowerTolerance = lowerTolerance;
  }


  public Date getAbortTime() {
    return abortTime;
  }

  public void setAbortTime(Date abortTime) {
    this.abortTime = abortTime;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
