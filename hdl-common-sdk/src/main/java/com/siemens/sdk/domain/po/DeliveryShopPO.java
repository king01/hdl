package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 11:00:01 
 */
@Entity
@Table ( name ="t_delivery_shop" )
public class DeliveryShopPO  implements Serializable {

	private static final long serialVersionUID =  4706898920676826669L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 门店编码
	 */
   	@Column(name = "delivery_no" )
	private String deliveryNo;

	/**
	 * 门店名称
	 */
   	@Column(name = "delivery_name" )
	private String deliveryName;

	/**
	 * 城市区域编码
	 */
   	@Column(name = "region_code" )
	private String regionCode;

	/**
	 * 城市区域名称
	 */
   	@Column(name = "region_name" )
	private String regionName;

	/**
	 * 是否启用:10011001=启用；10011002=停用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 地址
	 */
   	@Column(name = "delivery_address" )
	private String deliveryAddress;

	/**
	 * 收货联系人
	 */
   	@Column(name = "delivery_contact" )
	private String deliveryContact;

	/**
	 * 收货联系人电话
	 */
   	@Column(name = "delivery_contact_tel" )
	private String deliveryContactTel;

	/**
	 * 描述
	 */
   	@Column(name = "desc" )
	private String desc;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getDeliveryNo() {
    return deliveryNo;
  }

  public void setDeliveryNo(String deliveryNo) {
    this.deliveryNo = deliveryNo;
  }


  public String getDeliveryName() {
    return deliveryName;
  }

  public void setDeliveryName(String deliveryName) {
    this.deliveryName = deliveryName;
  }


  public String getRegionCode() {
    return regionCode;
  }

  public void setRegionCode(String regionCode) {
    this.regionCode = regionCode;
  }


  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public String getDeliveryAddress() {
    return deliveryAddress;
  }

  public void setDeliveryAddress(String deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }


  public String getDeliveryContact() {
    return deliveryContact;
  }

  public void setDeliveryContact(String deliveryContact) {
    this.deliveryContact = deliveryContact;
  }


  public String getDeliveryContactTel() {
    return deliveryContactTel;
  }

  public void setDeliveryContactTel(String deliveryContactTel) {
    this.deliveryContactTel = deliveryContactTel;
  }


  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
