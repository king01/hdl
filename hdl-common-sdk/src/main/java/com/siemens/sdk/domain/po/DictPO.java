package com.siemens.sdk.domain.po;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @ClassName: DictPO
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 11:16
 */
@Data
@Entity
@Table(name = "t_dict")
@EqualsAndHashCode// 自动生成get、set、toString、equals方法
@AllArgsConstructor // 全参构造方法
@NoArgsConstructor // 无参构造方法
@Accessors(chain = true)
public class DictPO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 自增长策略
    private Long id;

    @Column(nullable = false)
    private Long dictSeq;

    @Column(nullable = false)
    private String dictLabel;

    @Column(nullable = false)
    private String dictVal;

    @Column(nullable = false)
    private String dictDesc;

    @Column(name = "is_enable")
    private String isEnable;

//    @JsonBackReference
    @ManyToOne(cascade={CascadeType.MERGE,CascadeType.REFRESH},optional=false)
    @JoinColumn(name="dict_type_no")//设置在article表中的关联字段(外键)
    @JSONField(serialize = false)
    private DictTypePO dictTypePO;

    @Override
    public String toString() {
        return "DictPO{ id='"+id+"'}";
    }

}
