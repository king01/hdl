package com.siemens.sdk.domain.po;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: DictTypePO
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 10:11
 */
@Data
@Entity
@Table(name = "t_dict_type")
@AllArgsConstructor // 全参构造方法
@NoArgsConstructor // 无参构造方法
@Accessors(chain = true) // 链式编程
public class DictTypePO implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 自增长策略
    private Long id;

    @Column(name = "subject_name")
    private String subjectName;

    @Column(name = "instance_name")
    private String instanceName;

    @Column(name = "dict_type_no")
    private String dictTypeNo;

    @Column(name = "dict_type_name")
    private String dictTypeName;

    @Column(name = "is_enable")
    private String isEnable;

    @OneToMany(mappedBy = "dictTypePO",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    private List<DictPO> dictPOList;
}
