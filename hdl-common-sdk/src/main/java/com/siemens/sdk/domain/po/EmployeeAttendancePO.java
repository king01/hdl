package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_employee_attendance" )
public class EmployeeAttendancePO  implements Serializable {

	private static final long serialVersionUID =  5776458021063058510L;

	/**
	 * 考勤记录编号
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 员工工号
	 */
   	@Column(name = "employee_no" )
	private String employeeNo;

	/**
	 * 员工姓名
	 */
   	@Column(name = "employee_name" )
	private String employeeName;

	/**
	 * 考勤日期
	 */
   	@Column(name = "attendance_date" )
	private Date attendanceDate;

	/**
	 * 车间所属类型：20031001=预生产车间，20031002=分装车间
	 */
   	@Column(name = "belong_type" )
	private String belongType;

	/**
	 * 车间编码
	 */
   	@Column(name = "work_room_no" )
	private String workRoomNo;

	/**
	 * 车间名称
	 */
   	@Column(name = "work_room_name" )
	private String workRoomName;

	/**
	 * 班组编号
	 */
   	@Column(name = "team_no" )
	private String teamNo;

	/**
	 * 班组名称
	 */
   	@Column(name = "team_name" )
	private String teamName;

	/**
	 * 考勤地点
	 */
   	@Column(name = "attendance_position" )
	private String attendancePosition;

	/**
	 * 到岗打卡时间
	 */
   	@Column(name = "arrive_clock_time" )
	private Date arriveClockTime;

	/**
	 * 离岗打卡时间
	 */
   	@Column(name = "leave_clock_time" )
	private Date leaveClockTime;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getEmployeeNo() {
    return employeeNo;
  }

  public void setEmployeeNo(String employeeNo) {
    this.employeeNo = employeeNo;
  }


  public String getEmployeeName() {
    return employeeName;
  }

  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }


  public Date getAttendanceDate() {
    return attendanceDate;
  }

  public void setAttendanceDate(Date attendanceDate) {
    this.attendanceDate = attendanceDate;
  }


  public String getBelongType() {
    return belongType;
  }

  public void setBelongType(String belongType) {
    this.belongType = belongType;
  }


  public String getWorkRoomNo() {
    return workRoomNo;
  }

  public void setWorkRoomNo(String workRoomNo) {
    this.workRoomNo = workRoomNo;
  }


  public String getWorkRoomName() {
    return workRoomName;
  }

  public void setWorkRoomName(String workRoomName) {
    this.workRoomName = workRoomName;
  }


  public String getTeamNo() {
    return teamNo;
  }

  public void setTeamNo(String teamNo) {
    this.teamNo = teamNo;
  }


  public String getTeamName() {
    return teamName;
  }

  public void setTeamName(String teamName) {
    this.teamName = teamName;
  }


  public String getAttendancePosition() {
    return attendancePosition;
  }

  public void setAttendancePosition(String attendancePosition) {
    this.attendancePosition = attendancePosition;
  }


  public Date getArriveClockTime() {
    return arriveClockTime;
  }

  public void setArriveClockTime(Date arriveClockTime) {
    this.arriveClockTime = arriveClockTime;
  }


  public Date getLeaveClockTime() {
    return leaveClockTime;
  }

  public void setLeaveClockTime(Date leaveClockTime) {
    this.leaveClockTime = leaveClockTime;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
