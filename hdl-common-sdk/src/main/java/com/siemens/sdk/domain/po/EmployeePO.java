package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_employee" )
public class EmployeePO  implements Serializable {

	private static final long serialVersionUID =  2071927969722593586L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 员工工号
	 */
   	@Column(name = "employee_no" )
	private String employeeNo;

	/**
	 * 员工姓名
	 */
   	@Column(name = "employee_name" )
	private String employeeName;

	/**
	 * 性别：10061001=男；10061002=女；10061003=未知
	 */
   	@Column(name = "sex" )
	private String sex;

	/**
	 * 出生日期
	 */
   	@Column(name = "birthday" )
	private Date birthday;

	/**
	 * 政治面貌：10071001=共青团员；10071002=党员；10071003=群众
	 */
   	@Column(name = "political_status" )
	private String politicalStatus;

	/**
	 * 学历：10081001=小学学历；10081002=中学学历；10081003=专科学历；10081004=本科学历；10081005=硕士学历；10081006=博士学历
	 */
   	@Column(name = "education" )
	private String education;

	/**
	 * 入职时间
	 */
   	@Column(name = "join_time" )
	private Date joinTime;

	/**
	 * 联系电话
	 */
   	@Column(name = "contact_tel" )
	private String contactTel;

	/**
	 * 联系地址
	 */
   	@Column(name = "contact_address" )
	private String contactAddress;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getEmployeeNo() {
    return employeeNo;
  }

  public void setEmployeeNo(String employeeNo) {
    this.employeeNo = employeeNo;
  }


  public String getEmployeeName() {
    return employeeName;
  }

  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }


  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }


  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }


  public String getPoliticalStatus() {
    return politicalStatus;
  }

  public void setPoliticalStatus(String politicalStatus) {
    this.politicalStatus = politicalStatus;
  }


  public String getEducation() {
    return education;
  }

  public void setEducation(String education) {
    this.education = education;
  }


  public Date getJoinTime() {
    return joinTime;
  }

  public void setJoinTime(Date joinTime) {
    this.joinTime = joinTime;
  }


  public String getContactTel() {
    return contactTel;
  }

  public void setContactTel(String contactTel) {
    this.contactTel = contactTel;
  }


  public String getContactAddress() {
    return contactAddress;
  }

  public void setContactAddress(String contactAddress) {
    this.contactAddress = contactAddress;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
