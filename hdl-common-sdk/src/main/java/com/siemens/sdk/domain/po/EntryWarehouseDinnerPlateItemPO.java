package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_entry_warehouse_dinner_plate_item" )
public class EntryWarehouseDinnerPlateItemPO  implements Serializable {

	private static final long serialVersionUID =  373384407969081411L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 成品入库周转箱主键ID
	 */
   	@Column(name = "entry_warehouse_turnover_no" )
	private Long entryWarehouseTurnoverNo;

	/**
	 * 商品编码
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 数量
	 */
   	@Column(name = "quantity" )
	private Long quantity;

	/**
	 * 单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 餐盘规格编码
	 */
   	@Column(name = "vehicle_spec_no" )
	private String vehicleSpecNo;

	/**
	 * 餐盘规格名称
	 */
   	@Column(name = "vehicle_spec_name" )
	private String vehicleSpecName;

	/**
	 * 餐盘RFID
	 */
   	@Column(name = "rfid" )
	private String rfid;

	/**
	 * 实际重量
	 */
   	@Column(name = "actual_weight" )
	private BigDecimal actualWeight;

	/**
	 * 实际重量单位编码
	 */
   	@Column(name = "actual_weight_unit_code" )
	private String actualWeightUnitCode;

	/**
	 * 实际重量单位名称
	 */
   	@Column(name = "actual_weight_unit_name" )
	private String actualWeightUnitName;

	/**
	 * 排列顺序
	 */
   	@Column(name = "order_seq" )
	private Long orderSeq;

	/**
	 * 排列位置说明
	 */
   	@Column(name = "position_explain" )
	private String positionExplain;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public Long getEntryWarehouseTurnoverNo() {
    return entryWarehouseTurnoverNo;
  }

  public void setEntryWarehouseTurnoverNo(Long entryWarehouseTurnoverNo) {
    this.entryWarehouseTurnoverNo = entryWarehouseTurnoverNo;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Long getQuantity() {
    return quantity;
  }

  public void setQuantity(Long quantity) {
    this.quantity = quantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getVehicleSpecNo() {
    return vehicleSpecNo;
  }

  public void setVehicleSpecNo(String vehicleSpecNo) {
    this.vehicleSpecNo = vehicleSpecNo;
  }


  public String getVehicleSpecName() {
    return vehicleSpecName;
  }

  public void setVehicleSpecName(String vehicleSpecName) {
    this.vehicleSpecName = vehicleSpecName;
  }


  public String getRfid() {
    return rfid;
  }

  public void setRfid(String rfid) {
    this.rfid = rfid;
  }


  public BigDecimal getActualWeight() {
    return actualWeight;
  }

  public void setActualWeight(BigDecimal actualWeight) {
    this.actualWeight = actualWeight;
  }


  public String getActualWeightUnitCode() {
    return actualWeightUnitCode;
  }

  public void setActualWeightUnitCode(String actualWeightUnitCode) {
    this.actualWeightUnitCode = actualWeightUnitCode;
  }


  public String getActualWeightUnitName() {
    return actualWeightUnitName;
  }

  public void setActualWeightUnitName(String actualWeightUnitName) {
    this.actualWeightUnitName = actualWeightUnitName;
  }


  public Long getOrderSeq() {
    return orderSeq;
  }

  public void setOrderSeq(Long orderSeq) {
    this.orderSeq = orderSeq;
  }


  public String getPositionExplain() {
    return positionExplain;
  }

  public void setPositionExplain(String positionExplain) {
    this.positionExplain = positionExplain;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
