package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_entry_warehouse_turnover" )
public class EntryWarehouseTurnoverPO  implements Serializable {

	private static final long serialVersionUID =  5797904274879804397L;

	/**
	 * 半成品入库周转箱主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 半成品入库编号
	 */
   	@Column(name = "entry_warehouse_no" )
	private String entryWarehouseNo;

	/**
	 * 半成品工单编号
	 */
   	@Column(name = "semi_work_order_no" )
	private String semiWorkOrderNo;

	/**
	 * 半成品派工编号
	 */
   	@Column(name = "semi_production_dispatch_no" )
	private String semiProductionDispatchNo;

	/**
	 * 周转箱规格编码
	 */
   	@Column(name = "turnover_spec_no" )
	private String turnoverSpecNo;

	/**
	 * 周转箱规格名称
	 */
   	@Column(name = "turnover_spec_name" )
	private String turnoverSpecName;

	/**
	 * 周转箱RFID
	 */
   	@Column(name = "rfid" )
	private String rfid;

	/**
	 * 二维码
	 */
   	@Column(name = "qr_code" )
	private String qrCode;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getEntryWarehouseNo() {
    return entryWarehouseNo;
  }

  public void setEntryWarehouseNo(String entryWarehouseNo) {
    this.entryWarehouseNo = entryWarehouseNo;
  }


  public String getSemiWorkOrderNo() {
    return semiWorkOrderNo;
  }

  public void setSemiWorkOrderNo(String semiWorkOrderNo) {
    this.semiWorkOrderNo = semiWorkOrderNo;
  }


  public String getSemiProductionDispatchNo() {
    return semiProductionDispatchNo;
  }

  public void setSemiProductionDispatchNo(String semiProductionDispatchNo) {
    this.semiProductionDispatchNo = semiProductionDispatchNo;
  }


  public String getTurnoverSpecNo() {
    return turnoverSpecNo;
  }

  public void setTurnoverSpecNo(String turnoverSpecNo) {
    this.turnoverSpecNo = turnoverSpecNo;
  }


  public String getTurnoverSpecName() {
    return turnoverSpecName;
  }

  public void setTurnoverSpecName(String turnoverSpecName) {
    this.turnoverSpecName = turnoverSpecName;
  }


  public String getRfid() {
    return rfid;
  }

  public void setRfid(String rfid) {
    this.rfid = rfid;
  }


  public String getQrCode() {
    return qrCode;
  }

  public void setQrCode(String qrCode) {
    this.qrCode = qrCode;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
