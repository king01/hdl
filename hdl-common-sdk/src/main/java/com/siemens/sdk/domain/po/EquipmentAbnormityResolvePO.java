package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_equipment_abnormity_resolve" )
public class EquipmentAbnormityResolvePO  implements Serializable {

	private static final long serialVersionUID =  928515007652333016L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 异常处理编号
	 */
   	@Column(name = "abnormity_resolve_no" )
	private String abnormityResolveNo;

	/**
	 * 异常编号
	 */
   	@Column(name = "abnormity_no" )
	private String abnormityNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 处理状态:10101001=待处理；10101002=处理中；10101003=已解决；10101004=已拒绝
	 */
   	@Column(name = "resolve_status" )
	private String resolveStatus;

	/**
	 * 接受时间
	 */
   	@Column(name = "accept_time" )
	private Date acceptTime;

	/**
	 * 接受人
	 */
   	@Column(name = "accept_user_name" )
	private String acceptUserName;

	/**
	 * 处理操作
	 */
   	@Column(name = "resolve_operate_type" )
	private String resolveOperateType;

	/**
	 * 处理意见
	 */
   	@Column(name = "resolve_type" )
	private String resolveType;

	/**
	 * 预计划开始时间
	 */
   	@Column(name = "repair_start_time" )
	private Date repairStartTime;

	/**
	 * 预计划结束时间
	 */
   	@Column(name = "repair_end_time" )
	private Date repairEndTime;

	/**
	 * 维修方式
	 */
   	@Column(name = "repair_type" )
	private String repairType;

	/**
	 * 维修说明
	 */
   	@Column(name = "repair_explain" )
	private String repairExplain;

	/**
	 * 实际开始时间
	 */
   	@Column(name = "actual_start_time" )
	private Date actualStartTime;

	/**
	 * 实际结束时间
	 */
   	@Column(name = "actual_end_time" )
	private Date actualEndTime;

	/**
	 * 处理人
	 */
   	@Column(name = "handle_user_name" )
	private String handleUserName;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getAbnormityResolveNo() {
    return abnormityResolveNo;
  }

  public void setAbnormityResolveNo(String abnormityResolveNo) {
    this.abnormityResolveNo = abnormityResolveNo;
  }


  public String getAbnormityNo() {
    return abnormityNo;
  }

  public void setAbnormityNo(String abnormityNo) {
    this.abnormityNo = abnormityNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getResolveStatus() {
    return resolveStatus;
  }

  public void setResolveStatus(String resolveStatus) {
    this.resolveStatus = resolveStatus;
  }


  public Date getAcceptTime() {
    return acceptTime;
  }

  public void setAcceptTime(Date acceptTime) {
    this.acceptTime = acceptTime;
  }


  public String getAcceptUserName() {
    return acceptUserName;
  }

  public void setAcceptUserName(String acceptUserName) {
    this.acceptUserName = acceptUserName;
  }


  public String getResolveOperateType() {
    return resolveOperateType;
  }

  public void setResolveOperateType(String resolveOperateType) {
    this.resolveOperateType = resolveOperateType;
  }


  public String getResolveType() {
    return resolveType;
  }

  public void setResolveType(String resolveType) {
    this.resolveType = resolveType;
  }


  public Date getRepairStartTime() {
    return repairStartTime;
  }

  public void setRepairStartTime(Date repairStartTime) {
    this.repairStartTime = repairStartTime;
  }


  public Date getRepairEndTime() {
    return repairEndTime;
  }

  public void setRepairEndTime(Date repairEndTime) {
    this.repairEndTime = repairEndTime;
  }


  public String getRepairType() {
    return repairType;
  }

  public void setRepairType(String repairType) {
    this.repairType = repairType;
  }


  public String getRepairExplain() {
    return repairExplain;
  }

  public void setRepairExplain(String repairExplain) {
    this.repairExplain = repairExplain;
  }


  public Date getActualStartTime() {
    return actualStartTime;
  }

  public void setActualStartTime(Date actualStartTime) {
    this.actualStartTime = actualStartTime;
  }


  public Date getActualEndTime() {
    return actualEndTime;
  }

  public void setActualEndTime(Date actualEndTime) {
    this.actualEndTime = actualEndTime;
  }


  public String getHandleUserName() {
    return handleUserName;
  }

  public void setHandleUserName(String handleUserName) {
    this.handleUserName = handleUserName;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
