package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_equipment_check_item" )
public class EquipmentCheckItemPO  implements Serializable {

	private static final long serialVersionUID =  6133786642946738540L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 检查项编号
	 */
   	@Column(name = "check_item_no" )
	private String checkItemNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 检查项序号
	 */
   	@Column(name = "check_item_seq" )
	private BigDecimal checkItemSeq;

	/**
	 * 检查项名称
	 */
   	@Column(name = "check_item_name" )
	private String checkItemName;

	/**
	 * 检查项内容
	 */
   	@Column(name = "check_item_context" )
	private String checkItemContext;

	/**
	 * 设备编码
	 */
   	@Column(name = "equipment_no" )
	private String equipmentNo;

	/**
	 * 设备名称
	 */
   	@Column(name = "equipment_name" )
	private String equipmentName;

	/**
	 * 车间所属类型
	 */
   	@Column(name = "belong_type" )
	private String belongType;

	/**
	 * 所属车间/产线编码
	 */
   	@Column(name = "belong_no" )
	private String belongNo;

	/**
	 * 所属车间/产线名称
	 */
   	@Column(name = "belong_name" )
	private String belongName;

	/**
	 * 是否启用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getCheckItemNo() {
    return checkItemNo;
  }

  public void setCheckItemNo(String checkItemNo) {
    this.checkItemNo = checkItemNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public BigDecimal getCheckItemSeq() {
    return checkItemSeq;
  }

  public void setCheckItemSeq(BigDecimal checkItemSeq) {
    this.checkItemSeq = checkItemSeq;
  }


  public String getCheckItemName() {
    return checkItemName;
  }

  public void setCheckItemName(String checkItemName) {
    this.checkItemName = checkItemName;
  }


  public String getCheckItemContext() {
    return checkItemContext;
  }

  public void setCheckItemContext(String checkItemContext) {
    this.checkItemContext = checkItemContext;
  }


  public String getEquipmentNo() {
    return equipmentNo;
  }

  public void setEquipmentNo(String equipmentNo) {
    this.equipmentNo = equipmentNo;
  }


  public String getEquipmentName() {
    return equipmentName;
  }

  public void setEquipmentName(String equipmentName) {
    this.equipmentName = equipmentName;
  }


  public String getBelongType() {
    return belongType;
  }

  public void setBelongType(String belongType) {
    this.belongType = belongType;
  }


  public String getBelongNo() {
    return belongNo;
  }

  public void setBelongNo(String belongNo) {
    this.belongNo = belongNo;
  }


  public String getBelongName() {
    return belongName;
  }

  public void setBelongName(String belongName) {
    this.belongName = belongName;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
