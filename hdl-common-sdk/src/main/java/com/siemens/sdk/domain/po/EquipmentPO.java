package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_equipment" )
public class EquipmentPO  implements Serializable {

	private static final long serialVersionUID =  9001416692762126101L;

	/**
	 * 设备编号
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 设备编码
	 */
   	@Column(name = "equipment_no" )
	private String equipmentNo;

	/**
	 * 设备名称
	 */
   	@Column(name = "equipment_name" )
	private String equipmentName;

	/**
	 * 设备型号
	 */
   	@Column(name = "model" )
	private String model;

	/**
	 * 设备状态：10091001=启用；10091002=停用；10091003=维修
	 */
   	@Column(name = "equipment_state" )
	private String equipmentState;

	/**
	 * 所属类型：20031001=预生产车间，20031002=分装车间
	 */
   	@Column(name = "belong_type" )
	private String belongType;

	/**
	 * 所属车间/产线编码
	 */
   	@Column(name = "belong_no" )
	private String belongNo;

	/**
	 * 所属车间/产线名称
	 */
   	@Column(name = "belong_name" )
	private String belongName;

	/**
	 * 能耗(kw/h)
	 */
   	@Column(name = "energy_consumption" )
	private BigDecimal energyConsumption;

	/**
	 * 设备用途
	 */
   	@Column(name = "purpose" )
	private String purpose;

	/**
	 * 出厂日期
	 */
   	@Column(name = "production_date" )
	private Date productionDate;

	/**
	 * 质保时长(年)
	 */
   	@Column(name = "expiration_length" )
	private Long expirationLength;

	/**
	 * 是否启用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getEquipmentNo() {
    return equipmentNo;
  }

  public void setEquipmentNo(String equipmentNo) {
    this.equipmentNo = equipmentNo;
  }


  public String getEquipmentName() {
    return equipmentName;
  }

  public void setEquipmentName(String equipmentName) {
    this.equipmentName = equipmentName;
  }


  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }


  public String getEquipmentState() {
    return equipmentState;
  }

  public void setEquipmentState(String equipmentState) {
    this.equipmentState = equipmentState;
  }


  public String getBelongType() {
    return belongType;
  }

  public void setBelongType(String belongType) {
    this.belongType = belongType;
  }


  public String getBelongNo() {
    return belongNo;
  }

  public void setBelongNo(String belongNo) {
    this.belongNo = belongNo;
  }


  public String getBelongName() {
    return belongName;
  }

  public void setBelongName(String belongName) {
    this.belongName = belongName;
  }


  public BigDecimal getEnergyConsumption() {
    return energyConsumption;
  }

  public void setEnergyConsumption(BigDecimal energyConsumption) {
    this.energyConsumption = energyConsumption;
  }


  public String getPurpose() {
    return purpose;
  }

  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }


  public Date getProductionDate() {
    return productionDate;
  }

  public void setProductionDate(Date productionDate) {
    this.productionDate = productionDate;
  }


  public Long getExpirationLength() {
    return expirationLength;
  }

  public void setExpirationLength(Long expirationLength) {
    this.expirationLength = expirationLength;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
