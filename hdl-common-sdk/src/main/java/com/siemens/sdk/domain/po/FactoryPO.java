package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_factory" )
public class FactoryPO  implements Serializable {

	private static final long serialVersionUID =  4070455535898909904L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂编码
	 */
   	@Column(name = "factory_no" )
	private String factoryNo;

	/**
	 * 工厂名称
	 */
   	@Column(name = "factory_name" )
	private String factoryName;

	/**
	 * 工厂状态:10011001=启用；10011002=停用
	 */
   	@Column(name = "factory_status" )
	private String factoryStatus;

	/**
	 * 工厂类型:10121001=内部；10121002=第三方
	 */
   	@Column(name = "factory_type" )
	private String factoryType;

	/**
	 * 省区域编码
	 */
   	@Column(name = "province_region_code" )
	private String provinceRegionCode;

	/**
	 * 省区域名称
	 */
   	@Column(name = "province_region_name" )
	private String provinceRegionName;

	/**
	 * 城市区域编码
	 */
   	@Column(name = "city_region_code" )
	private String cityRegionCode;

	/**
	 * 城市区域名称
	 */
   	@Column(name = "city_region_name" )
	private String cityRegionName;

	/**
	 * 详细地址
	 */
   	@Column(name = "address" )
	private String address;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 联系人
	 */
   	@Column(name = "contact_person" )
	private String contactPerson;

	/**
	 * 联系电话
	 */
   	@Column(name = "contact_tel" )
	private String contactTel;

	/**
	 * 是否启用：10011001=启用；10011002=停用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getFactoryNo() {
    return factoryNo;
  }

  public void setFactoryNo(String factoryNo) {
    this.factoryNo = factoryNo;
  }


  public String getFactoryName() {
    return factoryName;
  }

  public void setFactoryName(String factoryName) {
    this.factoryName = factoryName;
  }


  public String getFactoryStatus() {
    return factoryStatus;
  }

  public void setFactoryStatus(String factoryStatus) {
    this.factoryStatus = factoryStatus;
  }


  public String getFactoryType() {
    return factoryType;
  }

  public void setFactoryType(String factoryType) {
    this.factoryType = factoryType;
  }


  public String getProvinceRegionCode() {
    return provinceRegionCode;
  }

  public void setProvinceRegionCode(String provinceRegionCode) {
    this.provinceRegionCode = provinceRegionCode;
  }


  public String getProvinceRegionName() {
    return provinceRegionName;
  }

  public void setProvinceRegionName(String provinceRegionName) {
    this.provinceRegionName = provinceRegionName;
  }


  public String getCityRegionCode() {
    return cityRegionCode;
  }

  public void setCityRegionCode(String cityRegionCode) {
    this.cityRegionCode = cityRegionCode;
  }


  public String getCityRegionName() {
    return cityRegionName;
  }

  public void setCityRegionName(String cityRegionName) {
    this.cityRegionName = cityRegionName;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getContactPerson() {
    return contactPerson;
  }

  public void setContactPerson(String contactPerson) {
    this.contactPerson = contactPerson;
  }


  public String getContactTel() {
    return contactTel;
  }

  public void setContactTel(String contactTel) {
    this.contactTel = contactTel;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
