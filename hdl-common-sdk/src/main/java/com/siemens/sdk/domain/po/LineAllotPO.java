package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_line_allot" )
public class LineAllotPO  implements Serializable {

	private static final long serialVersionUID =  8533278215788703088L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 调拨单号
	 */
   	@Column(name = "allot_bill_no" )
	private String allotBillNo;

	/**
	 * 调拨状态：10131001=未调拨；10131002=已调拨
	 */
   	@Column(name = "status" )
	private String status;

	/**
	 * 调拨类型：10141001=载具调拨；10141002=无聊调拨
	 */
   	@Column(name = "allot_type" )
	private String allotType;

	/**
	 * 来源线边编码
	 */
   	@Column(name = "src_line_no" )
	private String srcLineNo;

	/**
	 * 来源线边名称
	 */
   	@Column(name = "src_line_name" )
	private String srcLineName;

	/**
	 * 目标线边编码
	 */
   	@Column(name = "dst_line_no" )
	private String dstLineNo;

	/**
	 * 线边名称
	 */
   	@Column(name = "dst_line_name" )
	private String dstLineName;

	/**
	 * 交货日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 波次编号
	 */
   	@Column(name = "delivery_wave_no" )
	private String deliveryWaveNo;

	/**
	 * 申请时间
	 */
   	@Column(name = "apply_time" )
	private Date applyTime;

	/**
	 * 完成时间
	 */
   	@Column(name = "end_time" )
	private Date endTime;

	/**
	 * 调拨描述
	 */
   	@Column(name = "allot_desc" )
	private String allotDesc;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 是否删除：0=未删除；1=已删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getAllotBillNo() {
    return allotBillNo;
  }

  public void setAllotBillNo(String allotBillNo) {
    this.allotBillNo = allotBillNo;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getAllotType() {
    return allotType;
  }

  public void setAllotType(String allotType) {
    this.allotType = allotType;
  }


  public String getSrcLineNo() {
    return srcLineNo;
  }

  public void setSrcLineNo(String srcLineNo) {
    this.srcLineNo = srcLineNo;
  }


  public String getSrcLineName() {
    return srcLineName;
  }

  public void setSrcLineName(String srcLineName) {
    this.srcLineName = srcLineName;
  }


  public String getDstLineNo() {
    return dstLineNo;
  }

  public void setDstLineNo(String dstLineNo) {
    this.dstLineNo = dstLineNo;
  }


  public String getDstLineName() {
    return dstLineName;
  }

  public void setDstLineName(String dstLineName) {
    this.dstLineName = dstLineName;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public String getDeliveryWaveNo() {
    return deliveryWaveNo;
  }

  public void setDeliveryWaveNo(String deliveryWaveNo) {
    this.deliveryWaveNo = deliveryWaveNo;
  }


  public Date getApplyTime() {
    return applyTime;
  }

  public void setApplyTime(Date applyTime) {
    this.applyTime = applyTime;
  }


  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }


  public String getAllotDesc() {
    return allotDesc;
  }

  public void setAllotDesc(String allotDesc) {
    this.allotDesc = allotDesc;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
