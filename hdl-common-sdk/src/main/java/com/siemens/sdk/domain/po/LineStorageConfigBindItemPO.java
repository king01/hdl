package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_line_storage_config_bind_item" )
public class LineStorageConfigBindItemPO  implements Serializable {

	private static final long serialVersionUID =  8585087955463794751L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 配置绑定明细编号
	 */
   	@Column(name = "config_bind_item_no" )
	private String configBindItemNo;

	/**
	 * 绑定编号
	 */
   	@Column(name = "line_bind_no" )
	private String lineBindNo;

	/**
	 * 配置明细编号
	 */
   	@Column(name = "config_item_no" )
	private String configItemNo;

	/**
	 * 线边编码
	 */
   	@Column(name = "line_no" )
	private String lineNo;

	/**
	 * 线边名称
	 */
   	@Column(name = "line_name" )
	private String lineName;

	/**
	 * 配置编号
	 */
   	@Column(name = "config_no" )
	private String configNo;

	/**
	 * 配置名称
	 */
   	@Column(name = "config_name" )
	private String configName;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getConfigBindItemNo() {
    return configBindItemNo;
  }

  public void setConfigBindItemNo(String configBindItemNo) {
    this.configBindItemNo = configBindItemNo;
  }


  public String getLineBindNo() {
    return lineBindNo;
  }

  public void setLineBindNo(String lineBindNo) {
    this.lineBindNo = lineBindNo;
  }


  public String getConfigItemNo() {
    return configItemNo;
  }

  public void setConfigItemNo(String configItemNo) {
    this.configItemNo = configItemNo;
  }


  public String getLineNo() {
    return lineNo;
  }

  public void setLineNo(String lineNo) {
    this.lineNo = lineNo;
  }


  public String getLineName() {
    return lineName;
  }

  public void setLineName(String lineName) {
    this.lineName = lineName;
  }


  public String getConfigNo() {
    return configNo;
  }

  public void setConfigNo(String configNo) {
    this.configNo = configNo;
  }


  public String getConfigName() {
    return configName;
  }

  public void setConfigName(String configName) {
    this.configName = configName;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
