package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_line_storage_config_item" )
public class LineStorageConfigItemPO  implements Serializable {

	private static final long serialVersionUID =  516381544328875320L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 配置明细编号
	 */
   	@Column(name = "config_item_no" )
	private String configItemNo;

	/**
	 * 配置编号
	 */
   	@Column(name = "config_no" )
	private String configNo;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 商品别名
	 */
   	@Column(name = "sku_alias" )
	private String skuAlias;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 库存分配数量
	 */
   	@Column(name = "distribution_quantity" )
	private Long distributionQuantity;

	/**
	 * 安全库存数量
	 */
   	@Column(name = "safe_quantity" )
	private Long safeQuantity;

	/**
	 * 单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getConfigItemNo() {
    return configItemNo;
  }

  public void setConfigItemNo(String configItemNo) {
    this.configItemNo = configItemNo;
  }


  public String getConfigNo() {
    return configNo;
  }

  public void setConfigNo(String configNo) {
    this.configNo = configNo;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public String getSkuAlias() {
    return skuAlias;
  }

  public void setSkuAlias(String skuAlias) {
    this.skuAlias = skuAlias;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Long getDistributionQuantity() {
    return distributionQuantity;
  }

  public void setDistributionQuantity(Long distributionQuantity) {
    this.distributionQuantity = distributionQuantity;
  }


  public Long getSafeQuantity() {
    return safeQuantity;
  }

  public void setSafeQuantity(Long safeQuantity) {
    this.safeQuantity = safeQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
