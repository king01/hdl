package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_line_storage_inventory_item" )
public class LineStorageInventoryItemPO  implements Serializable {

	private static final long serialVersionUID =  5911623930929080301L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 盘点明细编号
	 */
   	@Column(name = "inventory_item_no" )
	private String inventoryItemNo;

	/**
	 * 盘点编号
	 */
   	@Column(name = "inventory_no" )
	private String inventoryNo;

	/**
	 * 线边编码
	 */
   	@Column(name = "line_no" )
	private String lineNo;

	/**
	 * 线边名称
	 */
   	@Column(name = "line_name" )
	private String lineName;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 剩余库存数量
	 */
   	@Column(name = "residue_quantity" )
	private Long residueQuantity;

	/**
	 * 库存单位编码
	 */
   	@Column(name = "residue_unit_code" )
	private String residueUnitCode;

	/**
	 * 库存单位名称
	 */
   	@Column(name = "residue_unit_name" )
	private String residueUnitName;

	/**
	 * 剩余质保时间(h)
	 */
   	@Column(name = "residue_warranty_hours" )
	private BigDecimal residueWarrantyHours;

	/**
	 * 盘点数量
	 */
   	@Column(name = "inventory_quantity" )
	private Long inventoryQuantity;

	/**
	 * 实际盘点数量
	 */
   	@Column(name = "actual_inventory_quantity" )
	private Long actualInventoryQuantity;

	/**
	 * 盘点单位编码
	 */
   	@Column(name = "inventory_unit_code" )
	private String inventoryUnitCode;

	/**
	 * 盘点单位名称
	 */
   	@Column(name = "inventory_unit_name" )
	private String inventoryUnitName;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 盘点时间
	 */
   	@Column(name = "inventory_time" )
	private Date inventoryTime;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getInventoryItemNo() {
    return inventoryItemNo;
  }

  public void setInventoryItemNo(String inventoryItemNo) {
    this.inventoryItemNo = inventoryItemNo;
  }


  public String getInventoryNo() {
    return inventoryNo;
  }

  public void setInventoryNo(String inventoryNo) {
    this.inventoryNo = inventoryNo;
  }


  public String getLineNo() {
    return lineNo;
  }

  public void setLineNo(String lineNo) {
    this.lineNo = lineNo;
  }


  public String getLineName() {
    return lineName;
  }

  public void setLineName(String lineName) {
    this.lineName = lineName;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Long getResidueQuantity() {
    return residueQuantity;
  }

  public void setResidueQuantity(Long residueQuantity) {
    this.residueQuantity = residueQuantity;
  }


  public String getResidueUnitCode() {
    return residueUnitCode;
  }

  public void setResidueUnitCode(String residueUnitCode) {
    this.residueUnitCode = residueUnitCode;
  }


  public String getResidueUnitName() {
    return residueUnitName;
  }

  public void setResidueUnitName(String residueUnitName) {
    this.residueUnitName = residueUnitName;
  }


  public BigDecimal getResidueWarrantyHours() {
    return residueWarrantyHours;
  }

  public void setResidueWarrantyHours(BigDecimal residueWarrantyHours) {
    this.residueWarrantyHours = residueWarrantyHours;
  }


  public Long getInventoryQuantity() {
    return inventoryQuantity;
  }

  public void setInventoryQuantity(Long inventoryQuantity) {
    this.inventoryQuantity = inventoryQuantity;
  }


  public Long getActualInventoryQuantity() {
    return actualInventoryQuantity;
  }

  public void setActualInventoryQuantity(Long actualInventoryQuantity) {
    this.actualInventoryQuantity = actualInventoryQuantity;
  }


  public String getInventoryUnitCode() {
    return inventoryUnitCode;
  }

  public void setInventoryUnitCode(String inventoryUnitCode) {
    this.inventoryUnitCode = inventoryUnitCode;
  }


  public String getInventoryUnitName() {
    return inventoryUnitName;
  }

  public void setInventoryUnitName(String inventoryUnitName) {
    this.inventoryUnitName = inventoryUnitName;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Date getInventoryTime() {
    return inventoryTime;
  }

  public void setInventoryTime(Date inventoryTime) {
    this.inventoryTime = inventoryTime;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
