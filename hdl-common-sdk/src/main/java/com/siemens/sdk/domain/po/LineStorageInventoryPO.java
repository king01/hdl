package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_line_storage_inventory" )
public class LineStorageInventoryPO  implements Serializable {

	private static final long serialVersionUID =  2050341548805467308L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 盘点编号
	 */
   	@Column(name = "inventory_no" )
	private String inventoryNo;

	/**
	 * 线边编码
	 */
   	@Column(name = "line_no" )
	private String lineNo;

	/**
	 * 线边名称
	 */
   	@Column(name = "line_name" )
	private String lineName;

	/**
	 * 盘点状态
	 */
   	@Column(name = "inventory_status" )
	private String inventoryStatus;

	/**
	 * 盘点日期
	 */
   	@Column(name = "inventory_date" )
	private Date inventoryDate;

	/**
	 * 生产波次编码
	 */
   	@Column(name = "delivery_wave_no" )
	private String deliveryWaveNo;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getInventoryNo() {
    return inventoryNo;
  }

  public void setInventoryNo(String inventoryNo) {
    this.inventoryNo = inventoryNo;
  }


  public String getLineNo() {
    return lineNo;
  }

  public void setLineNo(String lineNo) {
    this.lineNo = lineNo;
  }


  public String getLineName() {
    return lineName;
  }

  public void setLineName(String lineName) {
    this.lineName = lineName;
  }


  public String getInventoryStatus() {
    return inventoryStatus;
  }

  public void setInventoryStatus(String inventoryStatus) {
    this.inventoryStatus = inventoryStatus;
  }


  public Date getInventoryDate() {
    return inventoryDate;
  }

  public void setInventoryDate(Date inventoryDate) {
    this.inventoryDate = inventoryDate;
  }


  public String getDeliveryWaveNo() {
    return deliveryWaveNo;
  }

  public void setDeliveryWaveNo(String deliveryWaveNo) {
    this.deliveryWaveNo = deliveryWaveNo;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
