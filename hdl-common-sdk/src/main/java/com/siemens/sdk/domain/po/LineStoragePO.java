package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_line_storage" )
public class LineStoragePO  implements Serializable {

	private static final long serialVersionUID =  665119554985636634L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 线边编码
	 */
   	@Column(name = "line_no" )
	private String lineNo;

	/**
	 * 线边名称
	 */
   	@Column(name = "line_name" )
	private String lineName;

	/**
	 * 线边状态：10011001=启用；10011002=停用
	 */
   	@Column(name = "line_status" )
	private String lineStatus;

	/**
	 * 车间所属类型
	 */
   	@Column(name = "belong_type" )
	private String belongType;

	/**
	 * 所属车间/产线编码
	 */
   	@Column(name = "belong_no" )
	private String belongNo;

	/**
	 * 所属车间/产线名称
	 */
   	@Column(name = "belong_name" )
	private String belongName;

	/**
	 * 温区：冷冻；冷藏；常温
	 */
   	@Column(name = "warm_zone" )
	private String warmZone;

	/**
	 * 库存数量
	 */
   	@Column(name = "stock_quantity" )
	private Long stockQuantity;

	/**
	 * 库存单位编码
	 */
   	@Column(name = "stock_unit_code" )
	private String stockUnitCode;

	/**
	 * 库存单位名称
	 */
   	@Column(name = "stock_unit_name" )
	private String stockUnitName;

	/**
	 * 是否自动叫料：0=否，1=是
	 */
   	@Column(name = "is_auto_call_material" )
	private Long isAutoCallMaterial;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getLineNo() {
    return lineNo;
  }

  public void setLineNo(String lineNo) {
    this.lineNo = lineNo;
  }


  public String getLineName() {
    return lineName;
  }

  public void setLineName(String lineName) {
    this.lineName = lineName;
  }


  public String getLineStatus() {
    return lineStatus;
  }

  public void setLineStatus(String lineStatus) {
    this.lineStatus = lineStatus;
  }


  public String getBelongType() {
    return belongType;
  }

  public void setBelongType(String belongType) {
    this.belongType = belongType;
  }


  public String getBelongNo() {
    return belongNo;
  }

  public void setBelongNo(String belongNo) {
    this.belongNo = belongNo;
  }


  public String getBelongName() {
    return belongName;
  }

  public void setBelongName(String belongName) {
    this.belongName = belongName;
  }


  public String getWarmZone() {
    return warmZone;
  }

  public void setWarmZone(String warmZone) {
    this.warmZone = warmZone;
  }


  public Long getStockQuantity() {
    return stockQuantity;
  }

  public void setStockQuantity(Long stockQuantity) {
    this.stockQuantity = stockQuantity;
  }


  public String getStockUnitCode() {
    return stockUnitCode;
  }

  public void setStockUnitCode(String stockUnitCode) {
    this.stockUnitCode = stockUnitCode;
  }


  public String getStockUnitName() {
    return stockUnitName;
  }

  public void setStockUnitName(String stockUnitName) {
    this.stockUnitName = stockUnitName;
  }


  public Long getIsAutoCallMaterial() {
    return isAutoCallMaterial;
  }

  public void setIsAutoCallMaterial(Long isAutoCallMaterial) {
    this.isAutoCallMaterial = isAutoCallMaterial;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
