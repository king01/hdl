package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_material" )
public class MaterialPO  implements Serializable {

	private static final long serialVersionUID =  2872620571444430061L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 物料编码
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 物料分类(日配)：10151001=日配；10151002=计划
	 */
   	@Column(name = "material_clazz" )
	private String materialClazz;

	/**
	 * 物料类型(半成品/原料)
	 */
   	@Column(name = "material_type" )
	private String materialType;

	/**
	 * 最小采购数量
	 */
   	@Column(name = "min_purchase_quantity" )
	private Long minPurchaseQuantity;

	/**
	 * 组件单位代码
	 */
   	@Column(name = "purchase_unit_code" )
	private String purchaseUnitCode;

	/**
	 * 组件单位名称
	 */
   	@Column(name = "purchase_unit_name" )
	private String purchaseUnitName;

	/**
	 * 采购截止时间
	 */
   	@Column(name = "purchase_stop_time" )
	private Date purchaseStopTime;

	/**
	 * 采购周期
	 */
   	@Column(name = "purchase_cycle" )
	private String purchaseCycle;

	/**
	 * 到货周期(day)
	 */
   	@Column(name = "arrive_cycle" )
	private Long arriveCycle;

	/**
	 * 保质期(h)
	 */
   	@Column(name = "quality_duration_hours" )
	private Long qualityDurationHours;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public String getMaterialClazz() {
    return materialClazz;
  }

  public void setMaterialClazz(String materialClazz) {
    this.materialClazz = materialClazz;
  }


  public String getMaterialType() {
    return materialType;
  }

  public void setMaterialType(String materialType) {
    this.materialType = materialType;
  }


  public Long getMinPurchaseQuantity() {
    return minPurchaseQuantity;
  }

  public void setMinPurchaseQuantity(Long minPurchaseQuantity) {
    this.minPurchaseQuantity = minPurchaseQuantity;
  }


  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public void setPurchaseUnitCode(String purchaseUnitCode) {
    this.purchaseUnitCode = purchaseUnitCode;
  }


  public String getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public void setPurchaseUnitName(String purchaseUnitName) {
    this.purchaseUnitName = purchaseUnitName;
  }


  public Date getPurchaseStopTime() {
    return purchaseStopTime;
  }

  public void setPurchaseStopTime(Date purchaseStopTime) {
    this.purchaseStopTime = purchaseStopTime;
  }


  public String getPurchaseCycle() {
    return purchaseCycle;
  }

  public void setPurchaseCycle(String purchaseCycle) {
    this.purchaseCycle = purchaseCycle;
  }


  public Long getArriveCycle() {
    return arriveCycle;
  }

  public void setArriveCycle(Long arriveCycle) {
    this.arriveCycle = arriveCycle;
  }


  public Long getQualityDurationHours() {
    return qualityDurationHours;
  }

  public void setQualityDurationHours(Long qualityDurationHours) {
    this.qualityDurationHours = qualityDurationHours;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
