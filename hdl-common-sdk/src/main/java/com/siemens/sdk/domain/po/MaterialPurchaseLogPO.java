package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_material_purchase_log" )
public class MaterialPurchaseLogPO  implements Serializable {

	private static final long serialVersionUID =  1635973376738322313L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 采购日志编号
	 */
   	@Column(name = "purchase_log_no" )
	private String purchaseLogNo;

	/**
	 * 物料需求明细
	 */
   	@Column(name = "material_item_no" )
	private String materialItemNo;

	/**
	 * 物料编码
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 物料需求数量
	 */
   	@Column(name = "material_quantity" )
	private Long materialQuantity;

	/**
	 * 单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 截止采购时间
	 */
   	@Column(name = "abort_purchase_time" )
	private Date abortPurchaseTime;

	/**
	 * 采购操作方式：10151001=日配；10151002=计划
	 */
   	@Column(name = "purchase_operation_type" )
	private String purchaseOperationType;

	/**
	 * 推送采购时间
	 */
   	@Column(name = "push_purchase_time" )
	private Date pushPurchaseTime;

	/**
	 * 采购系统
	 */
   	@Column(name = "purchase_system" )
	private String purchaseSystem;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getPurchaseLogNo() {
    return purchaseLogNo;
  }

  public void setPurchaseLogNo(String purchaseLogNo) {
    this.purchaseLogNo = purchaseLogNo;
  }


  public String getMaterialItemNo() {
    return materialItemNo;
  }

  public void setMaterialItemNo(String materialItemNo) {
    this.materialItemNo = materialItemNo;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Long getMaterialQuantity() {
    return materialQuantity;
  }

  public void setMaterialQuantity(Long materialQuantity) {
    this.materialQuantity = materialQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Date getAbortPurchaseTime() {
    return abortPurchaseTime;
  }

  public void setAbortPurchaseTime(Date abortPurchaseTime) {
    this.abortPurchaseTime = abortPurchaseTime;
  }


  public String getPurchaseOperationType() {
    return purchaseOperationType;
  }

  public void setPurchaseOperationType(String purchaseOperationType) {
    this.purchaseOperationType = purchaseOperationType;
  }


  public Date getPushPurchaseTime() {
    return pushPurchaseTime;
  }

  public void setPushPurchaseTime(Date pushPurchaseTime) {
    this.pushPurchaseTime = pushPurchaseTime;
  }


  public String getPurchaseSystem() {
    return purchaseSystem;
  }

  public void setPurchaseSystem(String purchaseSystem) {
    this.purchaseSystem = purchaseSystem;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
