package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_material_resolve_item" )
public class MaterialResolveItemPO  implements Serializable {

	private static final long serialVersionUID =  4108774772935797983L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 物料处理编号
	 */
   	@Column(name = "material_resolve_item_no" )
	private String materialResolveItemNo;

	/**
	 * 异常处理编号
	 */
   	@Column(name = "abnormity_resolve_no" )
	private String abnormityResolveNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 物料处理方式(补料/报废)
	 */
   	@Column(name = "resolve_mode" )
	private String resolveMode;

	/**
	 * 物料领料/补料编号
	 */
   	@Column(name = "material_operate_no" )
	private String materialOperateNo;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 交付数量
	 */
   	@Column(name = "delivery_quantity" )
	private Long deliveryQuantity;

	/**
	 * 物料单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 物料单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getMaterialResolveItemNo() {
    return materialResolveItemNo;
  }

  public void setMaterialResolveItemNo(String materialResolveItemNo) {
    this.materialResolveItemNo = materialResolveItemNo;
  }


  public String getAbnormityResolveNo() {
    return abnormityResolveNo;
  }

  public void setAbnormityResolveNo(String abnormityResolveNo) {
    this.abnormityResolveNo = abnormityResolveNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getResolveMode() {
    return resolveMode;
  }

  public void setResolveMode(String resolveMode) {
    this.resolveMode = resolveMode;
  }


  public String getMaterialOperateNo() {
    return materialOperateNo;
  }

  public void setMaterialOperateNo(String materialOperateNo) {
    this.materialOperateNo = materialOperateNo;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Long getDeliveryQuantity() {
    return deliveryQuantity;
  }

  public void setDeliveryQuantity(Long deliveryQuantity) {
    this.deliveryQuantity = deliveryQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
