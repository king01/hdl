package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_material_return_item" )
public class MaterialReturnItemPO  implements Serializable {

	private static final long serialVersionUID =  368855831000461934L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 物料明细编号
	 */
   	@Column(name = "return_item_no" )
	private String returnItemNo;

	/**
	 * 退库记录编号
	 */
   	@Column(name = "return_record_no" )
	private String returnRecordNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 退库单号
	 */
   	@Column(name = "return_bill_no" )
	private String returnBillNo;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 报损数量
	 */
   	@Column(name = "demage_quantity" )
	private Long demageQuantity;

	/**
	 * 物料单位编码
	 */
   	@Column(name = "material_unit_code" )
	private String materialUnitCode;

	/**
	 * 物料单位名称
	 */
   	@Column(name = "material_unit_name" )
	private String materialUnitName;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getReturnItemNo() {
    return returnItemNo;
  }

  public void setReturnItemNo(String returnItemNo) {
    this.returnItemNo = returnItemNo;
  }


  public String getReturnRecordNo() {
    return returnRecordNo;
  }

  public void setReturnRecordNo(String returnRecordNo) {
    this.returnRecordNo = returnRecordNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getReturnBillNo() {
    return returnBillNo;
  }

  public void setReturnBillNo(String returnBillNo) {
    this.returnBillNo = returnBillNo;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Long getDemageQuantity() {
    return demageQuantity;
  }

  public void setDemageQuantity(Long demageQuantity) {
    this.demageQuantity = demageQuantity;
  }


  public String getMaterialUnitCode() {
    return materialUnitCode;
  }

  public void setMaterialUnitCode(String materialUnitCode) {
    this.materialUnitCode = materialUnitCode;
  }


  public String getMaterialUnitName() {
    return materialUnitName;
  }

  public void setMaterialUnitName(String materialUnitName) {
    this.materialUnitName = materialUnitName;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
