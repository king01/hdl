package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_metric_unit_conversion" )
public class MetricUnitConversionPO  implements Serializable {

	private static final long serialVersionUID =  5289583522809377575L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 换算编号
	 */
   	@Column(name = "conversion_no" )
	private String conversionNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 物料编码
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 原单位编码
	 */
   	@Column(name = "src_unit_code" )
	private String srcUnitCode;

	/**
	 * 原单位名称
	 */
   	@Column(name = "src_unit_name" )
	private String srcUnitName;

	/**
	 * 目标单位编码
	 */
   	@Column(name = "dst_unit_code" )
	private String dstUnitCode;

	/**
	 * 目标单位名称
	 */
   	@Column(name = "dst_unit_name" )
	private String dstUnitName;

	/**
	 * 分母
	 */
   	@Column(name = "denominator" )
	private Long denominator;

	/**
	 * 分子
	 */
   	@Column(name = "numerator" )
	private Long numerator;

	/**
	 * 换算结果
	 */
   	@Column(name = "conversion_result" )
	private BigDecimal conversionResult;

	/**
	 * 是否启用：10011001=启用；10011002=停用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getConversionNo() {
    return conversionNo;
  }

  public void setConversionNo(String conversionNo) {
    this.conversionNo = conversionNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public String getSrcUnitCode() {
    return srcUnitCode;
  }

  public void setSrcUnitCode(String srcUnitCode) {
    this.srcUnitCode = srcUnitCode;
  }


  public String getSrcUnitName() {
    return srcUnitName;
  }

  public void setSrcUnitName(String srcUnitName) {
    this.srcUnitName = srcUnitName;
  }


  public String getDstUnitCode() {
    return dstUnitCode;
  }

  public void setDstUnitCode(String dstUnitCode) {
    this.dstUnitCode = dstUnitCode;
  }


  public String getDstUnitName() {
    return dstUnitName;
  }

  public void setDstUnitName(String dstUnitName) {
    this.dstUnitName = dstUnitName;
  }


  public Long getDenominator() {
    return denominator;
  }

  public void setDenominator(Long denominator) {
    this.denominator = denominator;
  }


  public Long getNumerator() {
    return numerator;
  }

  public void setNumerator(Long numerator) {
    this.numerator = numerator;
  }


  public BigDecimal getConversionResult() {
    return conversionResult;
  }

  public void setConversionResult(BigDecimal conversionResult) {
    this.conversionResult = conversionResult;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
