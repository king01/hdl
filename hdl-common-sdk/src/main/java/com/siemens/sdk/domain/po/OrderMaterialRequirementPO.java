package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_order_material_requirement" )
public class OrderMaterialRequirementPO  implements Serializable {

	private static final long serialVersionUID =  6832144638338522466L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 需求编号
	 */
   	@Column(name = "demand_no" )
	private String demandNo;

	/**
	 * 订单号
	 */
   	@Column(name = "delivery_order_no" )
	private String deliveryOrderNo;

	/**
	 * 订单来源：10021001=内部；10021002=外部
	 */
   	@Column(name = "order_source" )
	private String orderSource;

	/**
	 * 订单类型
	 */
   	@Column(name = "order_type" )
	private String orderType;

	/**
	 * 门店编码
	 */
   	@Column(name = "delivery_shop_no" )
	private String deliveryShopNo;

	/**
	 * 波次编号
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 菜品编码
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 菜品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 满箱数量
	 */
   	@Column(name = "full_quantity" )
	private Long fullQuantity;

	/**
	 * 半箱数量
	 */
   	@Column(name = "half_quantity" )
	private Long halfQuantity;

	/**
	 * 交付单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 交付单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 是否删除：0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getDemandNo() {
    return demandNo;
  }

  public void setDemandNo(String demandNo) {
    this.demandNo = demandNo;
  }


  public String getDeliveryOrderNo() {
    return deliveryOrderNo;
  }

  public void setDeliveryOrderNo(String deliveryOrderNo) {
    this.deliveryOrderNo = deliveryOrderNo;
  }


  public String getOrderSource() {
    return orderSource;
  }

  public void setOrderSource(String orderSource) {
    this.orderSource = orderSource;
  }


  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }


  public String getDeliveryShopNo() {
    return deliveryShopNo;
  }

  public void setDeliveryShopNo(String deliveryShopNo) {
    this.deliveryShopNo = deliveryShopNo;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Long getFullQuantity() {
    return fullQuantity;
  }

  public void setFullQuantity(Long fullQuantity) {
    this.fullQuantity = fullQuantity;
  }


  public Long getHalfQuantity() {
    return halfQuantity;
  }

  public void setHalfQuantity(Long halfQuantity) {
    this.halfQuantity = halfQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
