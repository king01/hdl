package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_pre_aps_work_order" )
public class PreApsWorkOrderPO  implements Serializable {

	private static final long serialVersionUID =  244025454050812050L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 预排产编号
	 */
   	@Column(name = "pre_aps_order_no" )
	private Long preApsOrderNo;

	/**
	 * 结果编号
	 */
   	@Column(name = "result_no" )
	private Long resultNo;

	/**
	 * 预生产日期
	 */
   	@Column(name = "pre_production_date" )
	private Date preProductionDate;

	/**
	 * 排产法则：30011001=最早交期法则；30011002=最短作业时间法则；30011003=关键性比率法则；30011004=最小能耗法则；30011005=其他法则
	 */
   	@Column(name = "aps_rule" )
	private String apsRule;

	/**
	 * 分装线数量
	 */
   	@Column(name = "line_number" )
	private Long lineNumber;

	/**
	 * 交付数量
	 */
   	@Column(name = "delivery_quantity" )
	private Long deliveryQuantity;

	/**
	 * 交付单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 交付单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private String deliveryWave;

	/**
	 * 是否启用；0=未启用；1=已启用
	 */
   	@Column(name = "is_enable" )
	private Long isEnable;

	/**
	 * 预开始时间
	 */
   	@Column(name = "pre_start_time" )
	private Date preStartTime;

	/**
	 * 预结束时间
	 */
   	@Column(name = "pre_end_time" )
	private Date preEndTime;

	/**
	 * 产能利用率
	 */
   	@Column(name = "production_used_rate" )
	private BigDecimal productionUsedRate;

	/**
	 * 是否删除；0=未删除；1=已删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public Long getPreApsOrderNo() {
    return preApsOrderNo;
  }

  public void setPreApsOrderNo(Long preApsOrderNo) {
    this.preApsOrderNo = preApsOrderNo;
  }


  public Long getResultNo() {
    return resultNo;
  }

  public void setResultNo(Long resultNo) {
    this.resultNo = resultNo;
  }


  public Date getPreProductionDate() {
    return preProductionDate;
  }

  public void setPreProductionDate(Date preProductionDate) {
    this.preProductionDate = preProductionDate;
  }


  public String getApsRule() {
    return apsRule;
  }

  public void setApsRule(String apsRule) {
    this.apsRule = apsRule;
  }


  public Long getLineNumber() {
    return lineNumber;
  }

  public void setLineNumber(Long lineNumber) {
    this.lineNumber = lineNumber;
  }


  public Long getDeliveryQuantity() {
    return deliveryQuantity;
  }

  public void setDeliveryQuantity(Long deliveryQuantity) {
    this.deliveryQuantity = deliveryQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(String deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public Long getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(Long isEnable) {
    this.isEnable = isEnable;
  }


  public Date getPreStartTime() {
    return preStartTime;
  }

  public void setPreStartTime(Date preStartTime) {
    this.preStartTime = preStartTime;
  }


  public Date getPreEndTime() {
    return preEndTime;
  }

  public void setPreEndTime(Date preEndTime) {
    this.preEndTime = preEndTime;
  }


  public BigDecimal getProductionUsedRate() {
    return productionUsedRate;
  }

  public void setProductionUsedRate(BigDecimal productionUsedRate) {
    this.productionUsedRate = productionUsedRate;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
