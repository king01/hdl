package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_pre_semi_product_work_order" )
public class PreSemiProductWorkOrderPO  implements Serializable {

	private static final long serialVersionUID =  7757273149499878224L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 预计划半成品工单编号
	 */
   	@Column(name = "pre_semi_order_no" )
	private String preSemiOrderNo;

	/**
	 * 预排产编号
	 */
   	@Column(name = "pre_aps_order_no" )
	private String preApsOrderNo;

	/**
	 * 预计划成品工单编号
	 */
   	@Column(name = "pre_work_order_no" )
	private String preWorkOrderNo;

	/**
	 * 排产号
	 */
   	@Column(name = "aps_no" )
	private String apsNo;

	/**
	 * 排产法则：30011001=最早交期法则；30011002=最短作业时间法则；30011003=关键性比率法则；30011004=最小能耗法则；30011005=其他法则
	 */
   	@Column(name = "aps_rule" )
	private String apsRule;

	/**
	 * 工单状态
	 */
   	@Column(name = "work_order_status" )
	private String workOrderStatus;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 物料编号
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 交付数量
	 */
   	@Column(name = "full_delivery_quantity" )
	private Long fullDeliveryQuantity;

	/**
	 * 物料单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 物料单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 预开始时间
	 */
   	@Column(name = "pre_start_time" )
	private Date preStartTime;

	/**
	 * 预结束时间
	 */
   	@Column(name = "pre_end_time" )
	private Date preEndTime;

	/**
	 * 生产日期
	 */
   	@Column(name = "production_date" )
	private Date productionDate;

	/**
	 * 预生产车间编码
	 */
   	@Column(name = "pre_work_room_no" )
	private Long preWorkRoomNo;

	/**
	 * 预生产车间名称
	 */
   	@Column(name = "pre_work_room_name" )
	private String preWorkRoomName;

	/**
	 * 预估人工数
	 */
   	@Column(name = "pre_worker_number" )
	private Long preWorkerNumber;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getPreSemiOrderNo() {
    return preSemiOrderNo;
  }

  public void setPreSemiOrderNo(String preSemiOrderNo) {
    this.preSemiOrderNo = preSemiOrderNo;
  }


  public String getPreApsOrderNo() {
    return preApsOrderNo;
  }

  public void setPreApsOrderNo(String preApsOrderNo) {
    this.preApsOrderNo = preApsOrderNo;
  }


  public String getPreWorkOrderNo() {
    return preWorkOrderNo;
  }

  public void setPreWorkOrderNo(String preWorkOrderNo) {
    this.preWorkOrderNo = preWorkOrderNo;
  }


  public String getApsNo() {
    return apsNo;
  }

  public void setApsNo(String apsNo) {
    this.apsNo = apsNo;
  }


  public String getApsRule() {
    return apsRule;
  }

  public void setApsRule(String apsRule) {
    this.apsRule = apsRule;
  }


  public String getWorkOrderStatus() {
    return workOrderStatus;
  }

  public void setWorkOrderStatus(String workOrderStatus) {
    this.workOrderStatus = workOrderStatus;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Long getFullDeliveryQuantity() {
    return fullDeliveryQuantity;
  }

  public void setFullDeliveryQuantity(Long fullDeliveryQuantity) {
    this.fullDeliveryQuantity = fullDeliveryQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Date getPreStartTime() {
    return preStartTime;
  }

  public void setPreStartTime(Date preStartTime) {
    this.preStartTime = preStartTime;
  }


  public Date getPreEndTime() {
    return preEndTime;
  }

  public void setPreEndTime(Date preEndTime) {
    this.preEndTime = preEndTime;
  }


  public Date getProductionDate() {
    return productionDate;
  }

  public void setProductionDate(Date productionDate) {
    this.productionDate = productionDate;
  }


  public Long getPreWorkRoomNo() {
    return preWorkRoomNo;
  }

  public void setPreWorkRoomNo(Long preWorkRoomNo) {
    this.preWorkRoomNo = preWorkRoomNo;
  }


  public String getPreWorkRoomName() {
    return preWorkRoomName;
  }

  public void setPreWorkRoomName(String preWorkRoomName) {
    this.preWorkRoomName = preWorkRoomName;
  }


  public Long getPreWorkerNumber() {
    return preWorkerNumber;
  }

  public void setPreWorkerNumber(Long preWorkerNumber) {
    this.preWorkerNumber = preWorkerNumber;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
