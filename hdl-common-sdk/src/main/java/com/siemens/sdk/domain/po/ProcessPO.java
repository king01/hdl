package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_process" )
public class ProcessPO  implements Serializable {

	private static final long serialVersionUID =  7537883222389545890L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工序编号
	 */
   	@Column(name = "process_no" )
	private String processNo;

	/**
	 * 工艺编号
	 */
   	@Column(name = "technology_no" )
	private String technologyNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 工序名称
	 */
   	@Column(name = "process_name" )
	private String processName;

	/**
	 * 工序描述
	 */
   	@Column(name = "description" )
	private String description;

	/**
	 * 排列序号
	 */
   	@Column(name = "order_seq" )
	private Long orderSeq;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 是否启用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getProcessNo() {
    return processNo;
  }

  public void setProcessNo(String processNo) {
    this.processNo = processNo;
  }


  public String getTechnologyNo() {
    return technologyNo;
  }

  public void setTechnologyNo(String technologyNo) {
    this.technologyNo = technologyNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getProcessName() {
    return processName;
  }

  public void setProcessName(String processName) {
    this.processName = processName;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Long getOrderSeq() {
    return orderSeq;
  }

  public void setOrderSeq(Long orderSeq) {
    this.orderSeq = orderSeq;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
