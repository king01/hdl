package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_product" )
public class ProductPO  implements Serializable {

	private static final long serialVersionUID =  7959521569689527268L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private Long skuNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 工厂编码
	 */
   	@Column(name = "factory_no" )
	private String factoryNo;

	/**
	 * 工厂名称
	 */
   	@Column(name = "factory_name" )
	private String factoryName;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 商品状态：30081001=正常；30081002=下架；30081003=全部
	 */
   	@Column(name = "sku_status" )
	private String skuStatus;

	/**
	 * 单位数量
	 */
   	@Column(name = "unit_quantity" )
	private Long unitQuantity;

	/**
	 * 单位编号
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 餐盒型号
	 */
   	@Column(name = "dinner_plate_model" )
	private String dinnerPlateModel;

	/**
	 * 餐盒名称
	 */
   	@Column(name = "dinner_plate_name" )
	private String dinnerPlateName;

	/**
	 * 周转箱型号
	 */
   	@Column(name = "turnover_model" )
	private String turnoverModel;

	/**
	 * 周转箱名称
	 */
   	@Column(name = "turnover_name" )
	private String turnoverName;

	/**
	 * 菜品保质期
	 */
   	@Column(name = "quality_duration_hours" )
	private Long qualityDurationHours;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(Long skuNo) {
    this.skuNo = skuNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getFactoryNo() {
    return factoryNo;
  }

  public void setFactoryNo(String factoryNo) {
    this.factoryNo = factoryNo;
  }


  public String getFactoryName() {
    return factoryName;
  }

  public void setFactoryName(String factoryName) {
    this.factoryName = factoryName;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public String getSkuStatus() {
    return skuStatus;
  }

  public void setSkuStatus(String skuStatus) {
    this.skuStatus = skuStatus;
  }


  public Long getUnitQuantity() {
    return unitQuantity;
  }

  public void setUnitQuantity(Long unitQuantity) {
    this.unitQuantity = unitQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getDinnerPlateModel() {
    return dinnerPlateModel;
  }

  public void setDinnerPlateModel(String dinnerPlateModel) {
    this.dinnerPlateModel = dinnerPlateModel;
  }


  public String getDinnerPlateName() {
    return dinnerPlateName;
  }

  public void setDinnerPlateName(String dinnerPlateName) {
    this.dinnerPlateName = dinnerPlateName;
  }


  public String getTurnoverModel() {
    return turnoverModel;
  }

  public void setTurnoverModel(String turnoverModel) {
    this.turnoverModel = turnoverModel;
  }


  public String getTurnoverName() {
    return turnoverName;
  }

  public void setTurnoverName(String turnoverName) {
    this.turnoverName = turnoverName;
  }


  public Long getQualityDurationHours() {
    return qualityDurationHours;
  }

  public void setQualityDurationHours(Long qualityDurationHours) {
    this.qualityDurationHours = qualityDurationHours;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
