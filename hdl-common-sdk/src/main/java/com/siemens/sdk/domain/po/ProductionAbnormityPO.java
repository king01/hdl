package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_abnormity" )
public class ProductionAbnormityPO  implements Serializable {

	private static final long serialVersionUID =  3201632328629002112L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 异常编号
	 */
   	@Column(name = "abnormity_no" )
	private String abnormityNo;

	/**
	 * 来源模块
	 */
   	@Column(name = "source_module" )
	private String sourceModule;

	/**
	 * 来源数据编号
	 */
   	@Column(name = "source_data_no" )
	private String sourceDataNo;

	/**
	 * 详细分类(设备/质量)
	 */
   	@Column(name = "abnormity_clazz" )
	private String abnormityClazz;

	/**
	 * 质量异常（物料/半成品）
	 */
   	@Column(name = "abnormal_quality" )
	private String abnormalQuality;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 工单类型
	 */
   	@Column(name = "work_order_type" )
	private String workOrderType;

	/**
	 * 工单编号
	 */
   	@Column(name = "work_order_no" )
	private String workOrderNo;

	/**
	 * 派工单号
	 */
   	@Column(name = "dispatch_no" )
	private String dispatchNo;

	/**
	 * 生产执行编号
	 */
   	@Column(name = "production_execute_no" )
	private String productionExecuteNo;

	/**
	 * 车间所属类型
	 */
   	@Column(name = "belong_type" )
	private String belongType;

	/**
	 * 所属车间/产线编码
	 */
   	@Column(name = "belong_no" )
	private String belongNo;

	/**
	 * 所属车间/产线名称
	 */
   	@Column(name = "belong_name" )
	private String belongName;

	/**
	 * 异常对象编码
	 */
   	@Column(name = "abnormity_object_no" )
	private String abnormityObjectNo;

	/**
	 * 异常对象名称
	 */
   	@Column(name = "abnormity_object_name" )
	private String abnormityObjectName;

	/**
	 * 异常状态
	 */
   	@Column(name = "abnormity_status" )
	private String abnormityStatus;

	/**
	 * 异常类型
	 */
   	@Column(name = "abnormity_type" )
	private String abnormityType;

	/**
	 * 异常描述
	 */
   	@Column(name = "abnormity_desc" )
	private String abnormityDesc;

	/**
	 * 生产班组编号
	 */
   	@Column(name = "team_no" )
	private String teamNo;

	/**
	 * 生产班组名称
	 */
   	@Column(name = "team_name" )
	private String teamName;

	/**
	 * 上报人名称
	 */
   	@Column(name = "report_user_name" )
	private String reportUserName;

	/**
	 * 上报时间
	 */
   	@Column(name = "report_time" )
	private Date reportTime;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getAbnormityNo() {
    return abnormityNo;
  }

  public void setAbnormityNo(String abnormityNo) {
    this.abnormityNo = abnormityNo;
  }


  public String getSourceModule() {
    return sourceModule;
  }

  public void setSourceModule(String sourceModule) {
    this.sourceModule = sourceModule;
  }


  public String getSourceDataNo() {
    return sourceDataNo;
  }

  public void setSourceDataNo(String sourceDataNo) {
    this.sourceDataNo = sourceDataNo;
  }


  public String getAbnormityClazz() {
    return abnormityClazz;
  }

  public void setAbnormityClazz(String abnormityClazz) {
    this.abnormityClazz = abnormityClazz;
  }


  public String getAbnormalQuality() {
    return abnormalQuality;
  }

  public void setAbnormalQuality(String abnormalQuality) {
    this.abnormalQuality = abnormalQuality;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getWorkOrderType() {
    return workOrderType;
  }

  public void setWorkOrderType(String workOrderType) {
    this.workOrderType = workOrderType;
  }


  public String getWorkOrderNo() {
    return workOrderNo;
  }

  public void setWorkOrderNo(String workOrderNo) {
    this.workOrderNo = workOrderNo;
  }


  public String getDispatchNo() {
    return dispatchNo;
  }

  public void setDispatchNo(String dispatchNo) {
    this.dispatchNo = dispatchNo;
  }


  public String getProductionExecuteNo() {
    return productionExecuteNo;
  }

  public void setProductionExecuteNo(String productionExecuteNo) {
    this.productionExecuteNo = productionExecuteNo;
  }


  public String getBelongType() {
    return belongType;
  }

  public void setBelongType(String belongType) {
    this.belongType = belongType;
  }


  public String getBelongNo() {
    return belongNo;
  }

  public void setBelongNo(String belongNo) {
    this.belongNo = belongNo;
  }


  public String getBelongName() {
    return belongName;
  }

  public void setBelongName(String belongName) {
    this.belongName = belongName;
  }


  public String getAbnormityObjectNo() {
    return abnormityObjectNo;
  }

  public void setAbnormityObjectNo(String abnormityObjectNo) {
    this.abnormityObjectNo = abnormityObjectNo;
  }


  public String getAbnormityObjectName() {
    return abnormityObjectName;
  }

  public void setAbnormityObjectName(String abnormityObjectName) {
    this.abnormityObjectName = abnormityObjectName;
  }


  public String getAbnormityStatus() {
    return abnormityStatus;
  }

  public void setAbnormityStatus(String abnormityStatus) {
    this.abnormityStatus = abnormityStatus;
  }


  public String getAbnormityType() {
    return abnormityType;
  }

  public void setAbnormityType(String abnormityType) {
    this.abnormityType = abnormityType;
  }


  public String getAbnormityDesc() {
    return abnormityDesc;
  }

  public void setAbnormityDesc(String abnormityDesc) {
    this.abnormityDesc = abnormityDesc;
  }


  public String getTeamNo() {
    return teamNo;
  }

  public void setTeamNo(String teamNo) {
    this.teamNo = teamNo;
  }


  public String getTeamName() {
    return teamName;
  }

  public void setTeamName(String teamName) {
    this.teamName = teamName;
  }


  public String getReportUserName() {
    return reportUserName;
  }

  public void setReportUserName(String reportUserName) {
    this.reportUserName = reportUserName;
  }


  public Date getReportTime() {
    return reportTime;
  }

  public void setReportTime(Date reportTime) {
    this.reportTime = reportTime;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
