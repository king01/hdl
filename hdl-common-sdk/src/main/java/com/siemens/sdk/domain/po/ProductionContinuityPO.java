package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_continuity" )
public class ProductionContinuityPO  implements Serializable {

	private static final long serialVersionUID =  8342388258128291248L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 连续性编号
	 */
   	@Column(name = "continue_no" )
	private Long continueNo;

	/**
	 * 菜品编码
	 */
   	@Column(name = "sku_no" )
	private Long skuNo;

	/**
	 * 菜品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 商品单位编码
	 */
   	@Column(name = "unit_no" )
	private Long unitNo;

	/**
	 * 商品单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 最小连续性
	 */
   	@Column(name = "min" )
	private Long min;

	/**
	 * 最大连续性
	 */
   	@Column(name = "max" )
	private Long max;

	/**
	 * 是否启用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public Long getContinueNo() {
    return continueNo;
  }

  public void setContinueNo(Long continueNo) {
    this.continueNo = continueNo;
  }


  public Long getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(Long skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Long getUnitNo() {
    return unitNo;
  }

  public void setUnitNo(Long unitNo) {
    this.unitNo = unitNo;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Long getMin() {
    return min;
  }

  public void setMin(Long min) {
    this.min = min;
  }


  public Long getMax() {
    return max;
  }

  public void setMax(Long max) {
    this.max = max;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
