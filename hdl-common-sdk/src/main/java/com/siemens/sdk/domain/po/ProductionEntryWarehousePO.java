package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_entry_warehouse" )
public class ProductionEntryWarehousePO  implements Serializable {

	private static final long serialVersionUID =  7956507940162082759L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 入库编号
	 */
   	@Column(name = "entry_warehouse_no" )
	private String entryWarehouseNo;

	/**
	 * 分装生产执行编号
	 */
   	@Column(name = "execute_no" )
	private String executeNo;

	/**
	 * 成品工单编号
	 */
   	@Column(name = "work_order_no" )
	private String workOrderNo;

	/**
	 * 成品派工编号
	 */
   	@Column(name = "production_dispatch_no" )
	private String productionDispatchNo;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 入库数量
	 */
   	@Column(name = "entry_quantity" )
	private Long entryQuantity;

	/**
	 * 商品单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 商品单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 入库类型：30071001=正常入库；30071002=越库交货至产线
	 */
   	@Column(name = "entry_type" )
	private String entryType;

	/**
	 * 入库申请时间
	 */
   	@Column(name = "entry_apply_time" )
	private Date entryApplyTime;

	/**
	 * 入库完成时间
	 */
   	@Column(name = "entry_end_time" )
	private Date entryEndTime;

	/**
	 * 入库单号
	 */
   	@Column(name = "entry_bill_no" )
	private String entryBillNo;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getEntryWarehouseNo() {
    return entryWarehouseNo;
  }

  public void setEntryWarehouseNo(String entryWarehouseNo) {
    this.entryWarehouseNo = entryWarehouseNo;
  }


  public String getExecuteNo() {
    return executeNo;
  }

  public void setExecuteNo(String executeNo) {
    this.executeNo = executeNo;
  }


  public String getWorkOrderNo() {
    return workOrderNo;
  }

  public void setWorkOrderNo(String workOrderNo) {
    this.workOrderNo = workOrderNo;
  }


  public String getProductionDispatchNo() {
    return productionDispatchNo;
  }

  public void setProductionDispatchNo(String productionDispatchNo) {
    this.productionDispatchNo = productionDispatchNo;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Long getEntryQuantity() {
    return entryQuantity;
  }

  public void setEntryQuantity(Long entryQuantity) {
    this.entryQuantity = entryQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getEntryType() {
    return entryType;
  }

  public void setEntryType(String entryType) {
    this.entryType = entryType;
  }


  public Date getEntryApplyTime() {
    return entryApplyTime;
  }

  public void setEntryApplyTime(Date entryApplyTime) {
    this.entryApplyTime = entryApplyTime;
  }


  public Date getEntryEndTime() {
    return entryEndTime;
  }

  public void setEntryEndTime(Date entryEndTime) {
    this.entryEndTime = entryEndTime;
  }


  public String getEntryBillNo() {
    return entryBillNo;
  }

  public void setEntryBillNo(String entryBillNo) {
    this.entryBillNo = entryBillNo;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
