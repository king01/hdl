package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_entry_warehouse_turnover" )
public class ProductionEntryWarehouseTurnoverPO  implements Serializable {

	private static final long serialVersionUID =  6151395461730216685L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 成品入库周转箱编号
	 */
   	@Column(name = "entry_warehouse_turnover_no" )
	private String entryWarehouseTurnoverNo;

	/**
	 * 成品入库编号
	 */
   	@Column(name = "entry_warehouse_no" )
	private String entryWarehouseNo;

	/**
	 * 成品工单编号
	 */
   	@Column(name = "work_order_no" )
	private String workOrderNo;

	/**
	 * 成品派工编号
	 */
   	@Column(name = "production_dispatch_no" )
	private String productionDispatchNo;

	/**
	 * 周转箱规格编码
	 */
   	@Column(name = "turnover_spec_no" )
	private String turnoverSpecNo;

	/**
	 * 周转箱规格名称
	 */
   	@Column(name = "turnover_spec_name" )
	private String turnoverSpecName;

	/**
	 * 周转箱RFID
	 */
   	@Column(name = "rfid" )
	private String rfid;

	/**
	 * 二维码
	 */
   	@Column(name = "qr_code" )
	private String qrCode;

	/**
	 * 堆垛号
	 */
   	@Column(name = "stack_no" )
	private String stackNo;

	/**
	 * 堆垛顺序
	 */
   	@Column(name = "stack_order" )
	private String stackOrder;

	/**
	 * 餐盘数量
	 */
   	@Column(name = "dinner_plate_quantity" )
	private Long dinnerPlateQuantity;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getEntryWarehouseTurnoverNo() {
    return entryWarehouseTurnoverNo;
  }

  public void setEntryWarehouseTurnoverNo(String entryWarehouseTurnoverNo) {
    this.entryWarehouseTurnoverNo = entryWarehouseTurnoverNo;
  }


  public String getEntryWarehouseNo() {
    return entryWarehouseNo;
  }

  public void setEntryWarehouseNo(String entryWarehouseNo) {
    this.entryWarehouseNo = entryWarehouseNo;
  }


  public String getWorkOrderNo() {
    return workOrderNo;
  }

  public void setWorkOrderNo(String workOrderNo) {
    this.workOrderNo = workOrderNo;
  }


  public String getProductionDispatchNo() {
    return productionDispatchNo;
  }

  public void setProductionDispatchNo(String productionDispatchNo) {
    this.productionDispatchNo = productionDispatchNo;
  }


  public String getTurnoverSpecNo() {
    return turnoverSpecNo;
  }

  public void setTurnoverSpecNo(String turnoverSpecNo) {
    this.turnoverSpecNo = turnoverSpecNo;
  }


  public String getTurnoverSpecName() {
    return turnoverSpecName;
  }

  public void setTurnoverSpecName(String turnoverSpecName) {
    this.turnoverSpecName = turnoverSpecName;
  }


  public String getRfid() {
    return rfid;
  }

  public void setRfid(String rfid) {
    this.rfid = rfid;
  }


  public String getQrCode() {
    return qrCode;
  }

  public void setQrCode(String qrCode) {
    this.qrCode = qrCode;
  }


  public String getStackNo() {
    return stackNo;
  }

  public void setStackNo(String stackNo) {
    this.stackNo = stackNo;
  }


  public String getStackOrder() {
    return stackOrder;
  }

  public void setStackOrder(String stackOrder) {
    this.stackOrder = stackOrder;
  }


  public Long getDinnerPlateQuantity() {
    return dinnerPlateQuantity;
  }

  public void setDinnerPlateQuantity(Long dinnerPlateQuantity) {
    this.dinnerPlateQuantity = dinnerPlateQuantity;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
