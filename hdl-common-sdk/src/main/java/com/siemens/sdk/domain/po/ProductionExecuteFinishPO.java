package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_execute_finish" )
public class ProductionExecuteFinishPO  implements Serializable {

	private static final long serialVersionUID =  5926195859807875228L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 完工编号
	 */
   	@Column(name = "finish_work_no" )
	private String finishWorkNo;

	/**
	 * 生产执行编号
	 */
   	@Column(name = "production_execute_no" )
	private String productionExecuteNo;

	/**
	 * 派工编号
	 */
   	@Column(name = "dispatch_no" )
	private String dispatchNo;

	/**
	 * 完工方式：30101001=人工；30101002=自动
	 */
   	@Column(name = "timesheet_mode" )
	private String timesheetMode;

	/**
	 * 完工来源：数据字典，半成品完工，成品完工
	 */
   	@Column(name = "timesheet_source" )
	private String timesheetSource;

	/**
	 * 工单编号
	 */
   	@Column(name = "order_on" )
	private String orderOn;

	/**
	 * 交付商品编码
	 */
   	@Column(name = "delivery_sku_no" )
	private String deliverySkuNo;

	/**
	 * 交付商品名称
	 */
   	@Column(name = "delivery_sku_name" )
	private String deliverySkuName;

	/**
	 * 交付单位编码
	 */
   	@Column(name = "delivery_unit_code" )
	private String deliveryUnitCode;

	/**
	 * 交付单位名称
	 */
   	@Column(name = "delivery_unit_name" )
	private String deliveryUnitName;

	/**
	 * 计划交付数量
	 */
   	@Column(name = "plan_delivery_quantity" )
	private Long planDeliveryQuantity;

	/**
	 * 实际交付数量
	 */
   	@Column(name = "actual_delivery_quantity" )
	private Long actualDeliveryQuantity;

	/**
	 * 剔除数量
	 */
   	@Column(name = "reject_quantity" )
	private Long rejectQuantity;

	/**
	 * 上报时间
	 */
   	@Column(name = "report_data_time" )
	private Date reportDataTime;

	/**
	 * 数据来源
	 */
   	@Column(name = "data_source" )
	private String dataSource;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getFinishWorkNo() {
    return finishWorkNo;
  }

  public void setFinishWorkNo(String finishWorkNo) {
    this.finishWorkNo = finishWorkNo;
  }


  public String getProductionExecuteNo() {
    return productionExecuteNo;
  }

  public void setProductionExecuteNo(String productionExecuteNo) {
    this.productionExecuteNo = productionExecuteNo;
  }


  public String getDispatchNo() {
    return dispatchNo;
  }

  public void setDispatchNo(String dispatchNo) {
    this.dispatchNo = dispatchNo;
  }


  public String getTimesheetMode() {
    return timesheetMode;
  }

  public void setTimesheetMode(String timesheetMode) {
    this.timesheetMode = timesheetMode;
  }


  public String getTimesheetSource() {
    return timesheetSource;
  }

  public void setTimesheetSource(String timesheetSource) {
    this.timesheetSource = timesheetSource;
  }


  public String getOrderOn() {
    return orderOn;
  }

  public void setOrderOn(String orderOn) {
    this.orderOn = orderOn;
  }


  public String getDeliverySkuNo() {
    return deliverySkuNo;
  }

  public void setDeliverySkuNo(String deliverySkuNo) {
    this.deliverySkuNo = deliverySkuNo;
  }


  public String getDeliverySkuName() {
    return deliverySkuName;
  }

  public void setDeliverySkuName(String deliverySkuName) {
    this.deliverySkuName = deliverySkuName;
  }


  public String getDeliveryUnitCode() {
    return deliveryUnitCode;
  }

  public void setDeliveryUnitCode(String deliveryUnitCode) {
    this.deliveryUnitCode = deliveryUnitCode;
  }


  public String getDeliveryUnitName() {
    return deliveryUnitName;
  }

  public void setDeliveryUnitName(String deliveryUnitName) {
    this.deliveryUnitName = deliveryUnitName;
  }


  public Long getPlanDeliveryQuantity() {
    return planDeliveryQuantity;
  }

  public void setPlanDeliveryQuantity(Long planDeliveryQuantity) {
    this.planDeliveryQuantity = planDeliveryQuantity;
  }


  public Long getActualDeliveryQuantity() {
    return actualDeliveryQuantity;
  }

  public void setActualDeliveryQuantity(Long actualDeliveryQuantity) {
    this.actualDeliveryQuantity = actualDeliveryQuantity;
  }


  public Long getRejectQuantity() {
    return rejectQuantity;
  }

  public void setRejectQuantity(Long rejectQuantity) {
    this.rejectQuantity = rejectQuantity;
  }


  public Date getReportDataTime() {
    return reportDataTime;
  }

  public void setReportDataTime(Date reportDataTime) {
    this.reportDataTime = reportDataTime;
  }


  public String getDataSource() {
    return dataSource;
  }

  public void setDataSource(String dataSource) {
    this.dataSource = dataSource;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
