package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_execute" )
public class ProductionExecutePO  implements Serializable {

	private static final long serialVersionUID =  8880139674939673470L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 分装生产执行编号
	 */
   	@Column(name = "execute_no" )
	private String executeNo;

	/**
	 * 成品工单编号
	 */
   	@Column(name = "work_order_no" )
	private String workOrderNo;

	/**
	 * 成品派工编号
	 */
   	@Column(name = "production_dispatch_no" )
	private String productionDispatchNo;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 交付数量
	 */
   	@Column(name = "delivery_quantity" )
	private Long deliveryQuantity;

	/**
	 * 商品单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 商品单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 实际开始时间
	 */
   	@Column(name = "actual_start_time" )
	private Date actualStartTime;

	/**
	 * 实际结束时间
	 */
   	@Column(name = "actual_end_time" )
	private Date actualEndTime;

	/**
	 * 执行比例
	 */
   	@Column(name = "execute_ratio" )
	private BigDecimal executeRatio;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getExecuteNo() {
    return executeNo;
  }

  public void setExecuteNo(String executeNo) {
    this.executeNo = executeNo;
  }


  public String getWorkOrderNo() {
    return workOrderNo;
  }

  public void setWorkOrderNo(String workOrderNo) {
    this.workOrderNo = workOrderNo;
  }


  public String getProductionDispatchNo() {
    return productionDispatchNo;
  }

  public void setProductionDispatchNo(String productionDispatchNo) {
    this.productionDispatchNo = productionDispatchNo;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Long getDeliveryQuantity() {
    return deliveryQuantity;
  }

  public void setDeliveryQuantity(Long deliveryQuantity) {
    this.deliveryQuantity = deliveryQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Date getActualStartTime() {
    return actualStartTime;
  }

  public void setActualStartTime(Date actualStartTime) {
    this.actualStartTime = actualStartTime;
  }


  public Date getActualEndTime() {
    return actualEndTime;
  }

  public void setActualEndTime(Date actualEndTime) {
    this.actualEndTime = actualEndTime;
  }


  public BigDecimal getExecuteRatio() {
    return executeRatio;
  }

  public void setExecuteRatio(BigDecimal executeRatio) {
    this.executeRatio = executeRatio;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
