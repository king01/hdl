package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_execute_timesheet_item" )
public class ProductionExecuteTimesheetItemPO  implements Serializable {

	private static final long serialVersionUID =  7984920447585441369L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 报工明细编号
	 */
   	@Column(name = "timesheet_item_no" )
	private String timesheetItemNo;

	/**
	 * 报工编号
	 */
   	@Column(name = "timesheet_no" )
	private String timesheetNo;

	/**
	 * 报工方式：30091001=人工；30091002=自动
	 */
   	@Column(name = "timesheet_mode" )
	private String timesheetMode;

	/**
	 * 报工来源
	 */
   	@Column(name = "timesheet_source" )
	private String timesheetSource;

	/**
	 * 工单编号
	 */
   	@Column(name = "order_no" )
	private String orderNo;

	/**
	 * 周转箱规格编码
	 */
   	@Column(name = "turnover_spec_no" )
	private String turnoverSpecNo;

	/**
	 * 周转箱规格名称
	 */
   	@Column(name = "turnover_spec_name" )
	private String turnoverSpecName;

	/**
	 * 周转箱RFID
	 */
   	@Column(name = "rfid" )
	private String rfid;

	/**
	 * 二维码
	 */
   	@Column(name = "qr_code" )
	private String qrCode;

	/**
	 * 报工时间
	 */
   	@Column(name = "timesheet_time" )
	private Date timesheetTime;

	/**
	 * 堆垛号(打包序号)
	 */
   	@Column(name = "stack_no" )
	private Long stackNo;

	/**
	 * 堆垛顺序：上位/下位
	 */
   	@Column(name = "stack_order" )
	private String stackOrder;

	/**
	 * 数据来源
	 */
   	@Column(name = "data_source" )
	private String dataSource;

	/**
	 * 餐盘数量
	 */
   	@Column(name = "dinner_plate_quantity" )
	private Long dinnerPlateQuantity;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getTimesheetItemNo() {
    return timesheetItemNo;
  }

  public void setTimesheetItemNo(String timesheetItemNo) {
    this.timesheetItemNo = timesheetItemNo;
  }


  public String getTimesheetNo() {
    return timesheetNo;
  }

  public void setTimesheetNo(String timesheetNo) {
    this.timesheetNo = timesheetNo;
  }


  public String getTimesheetMode() {
    return timesheetMode;
  }

  public void setTimesheetMode(String timesheetMode) {
    this.timesheetMode = timesheetMode;
  }


  public String getTimesheetSource() {
    return timesheetSource;
  }

  public void setTimesheetSource(String timesheetSource) {
    this.timesheetSource = timesheetSource;
  }


  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }


  public String getTurnoverSpecNo() {
    return turnoverSpecNo;
  }

  public void setTurnoverSpecNo(String turnoverSpecNo) {
    this.turnoverSpecNo = turnoverSpecNo;
  }


  public String getTurnoverSpecName() {
    return turnoverSpecName;
  }

  public void setTurnoverSpecName(String turnoverSpecName) {
    this.turnoverSpecName = turnoverSpecName;
  }


  public String getRfid() {
    return rfid;
  }

  public void setRfid(String rfid) {
    this.rfid = rfid;
  }


  public String getQrCode() {
    return qrCode;
  }

  public void setQrCode(String qrCode) {
    this.qrCode = qrCode;
  }


  public Date getTimesheetTime() {
    return timesheetTime;
  }

  public void setTimesheetTime(Date timesheetTime) {
    this.timesheetTime = timesheetTime;
  }


  public Long getStackNo() {
    return stackNo;
  }

  public void setStackNo(Long stackNo) {
    this.stackNo = stackNo;
  }


  public String getStackOrder() {
    return stackOrder;
  }

  public void setStackOrder(String stackOrder) {
    this.stackOrder = stackOrder;
  }


  public String getDataSource() {
    return dataSource;
  }

  public void setDataSource(String dataSource) {
    this.dataSource = dataSource;
  }


  public Long getDinnerPlateQuantity() {
    return dinnerPlateQuantity;
  }

  public void setDinnerPlateQuantity(Long dinnerPlateQuantity) {
    this.dinnerPlateQuantity = dinnerPlateQuantity;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
