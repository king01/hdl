package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_execute_timesheet" )
public class ProductionExecuteTimesheetPO  implements Serializable {

	private static final long serialVersionUID =  1582859632803862068L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 报工编号
	 */
   	@Column(name = "timesheet_no" )
	private String timesheetNo;

	/**
	 * 生产执行编号
	 */
   	@Column(name = "production_execute_no" )
	private String productionExecuteNo;

	/**
	 * 派工编号
	 */
   	@Column(name = "dispatch_no" )
	private String dispatchNo;

	/**
	 * 报工方式：30091001=人工；30091002=自动
	 */
   	@Column(name = "timesheet_mode" )
	private String timesheetMode;

	/**
	 * 报工来源
	 */
   	@Column(name = "timesheet_source" )
	private String timesheetSource;

	/**
	 * 工单编号
	 */
   	@Column(name = "order_no" )
	private String orderNo;

	/**
	 * 交付商品编码
	 */
   	@Column(name = "delivery_sku_no" )
	private String deliverySkuNo;

	/**
	 * 交付商品名称
	 */
   	@Column(name = "delivery_sku_name" )
	private String deliverySkuName;

	/**
	 * 交付数量
	 */
   	@Column(name = "delivery_quantity" )
	private Long deliveryQuantity;

	/**
	 * 交付单位编码
	 */
   	@Column(name = "delivery_unit_code" )
	private String deliveryUnitCode;

	/**
	 * 交付单位名称
	 */
   	@Column(name = "delivery_unit_name" )
	private String deliveryUnitName;

	/**
	 * 报工时间
	 */
   	@Column(name = "timesheet_time" )
	private Date timesheetTime;

	/**
	 * 数据来源
	 */
   	@Column(name = "data_source" )
	private String dataSource;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getTimesheetNo() {
    return timesheetNo;
  }

  public void setTimesheetNo(String timesheetNo) {
    this.timesheetNo = timesheetNo;
  }


  public String getProductionExecuteNo() {
    return productionExecuteNo;
  }

  public void setProductionExecuteNo(String productionExecuteNo) {
    this.productionExecuteNo = productionExecuteNo;
  }


  public String getDispatchNo() {
    return dispatchNo;
  }

  public void setDispatchNo(String dispatchNo) {
    this.dispatchNo = dispatchNo;
  }


  public String getTimesheetMode() {
    return timesheetMode;
  }

  public void setTimesheetMode(String timesheetMode) {
    this.timesheetMode = timesheetMode;
  }


  public String getTimesheetSource() {
    return timesheetSource;
  }

  public void setTimesheetSource(String timesheetSource) {
    this.timesheetSource = timesheetSource;
  }


  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }


  public String getDeliverySkuNo() {
    return deliverySkuNo;
  }

  public void setDeliverySkuNo(String deliverySkuNo) {
    this.deliverySkuNo = deliverySkuNo;
  }


  public String getDeliverySkuName() {
    return deliverySkuName;
  }

  public void setDeliverySkuName(String deliverySkuName) {
    this.deliverySkuName = deliverySkuName;
  }


  public Long getDeliveryQuantity() {
    return deliveryQuantity;
  }

  public void setDeliveryQuantity(Long deliveryQuantity) {
    this.deliveryQuantity = deliveryQuantity;
  }


  public String getDeliveryUnitCode() {
    return deliveryUnitCode;
  }

  public void setDeliveryUnitCode(String deliveryUnitCode) {
    this.deliveryUnitCode = deliveryUnitCode;
  }


  public String getDeliveryUnitName() {
    return deliveryUnitName;
  }

  public void setDeliveryUnitName(String deliveryUnitName) {
    this.deliveryUnitName = deliveryUnitName;
  }


  public Date getTimesheetTime() {
    return timesheetTime;
  }

  public void setTimesheetTime(Date timesheetTime) {
    this.timesheetTime = timesheetTime;
  }


  public String getDataSource() {
    return dataSource;
  }

  public void setDataSource(String dataSource) {
    this.dataSource = dataSource;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
