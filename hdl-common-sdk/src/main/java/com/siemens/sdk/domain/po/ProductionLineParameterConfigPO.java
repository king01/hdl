package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_line_parameter_config" )
public class ProductionLineParameterConfigPO  implements Serializable {

	private static final long serialVersionUID =  5544159786147995079L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 产线参数编号
	 */
   	@Column(name = "parameter_no" )
	private String parameterNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 产线编号
	 */
   	@Column(name = "production_line_no" )
	private String productionLineNo;

	/**
	 * 产线名称
	 */
   	@Column(name = "production_line_name" )
	private String productionLineName;

	/**
	 * 平均换线用时(分钟)
	 */
   	@Column(name = "change_line_average_duration" )
	private Long changeLineAverageDuration;

	/**
	 * 线边编号
	 */
   	@Column(name = "line_no" )
	private String lineNo;

	/**
	 * 线边名称
	 */
   	@Column(name = "line_name" )
	private String lineName;

	/**
	 * 平均到料用时(分钟)
	 */
   	@Column(name = "material_arrive_average_duration" )
	private Long materialArriveAverageDuration;

	/**
	 * 平均入库用时(分钟)
	 */
   	@Column(name = "entry_warehouse_average_duration" )
	private Long entryWarehouseAverageDuration;

	/**
	 * 是否启用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getParameterNo() {
    return parameterNo;
  }

  public void setParameterNo(String parameterNo) {
    this.parameterNo = parameterNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getProductionLineNo() {
    return productionLineNo;
  }

  public void setProductionLineNo(String productionLineNo) {
    this.productionLineNo = productionLineNo;
  }


  public String getProductionLineName() {
    return productionLineName;
  }

  public void setProductionLineName(String productionLineName) {
    this.productionLineName = productionLineName;
  }


  public Long getChangeLineAverageDuration() {
    return changeLineAverageDuration;
  }

  public void setChangeLineAverageDuration(Long changeLineAverageDuration) {
    this.changeLineAverageDuration = changeLineAverageDuration;
  }


  public String getLineNo() {
    return lineNo;
  }

  public void setLineNo(String lineNo) {
    this.lineNo = lineNo;
  }


  public String getLineName() {
    return lineName;
  }

  public void setLineName(String lineName) {
    this.lineName = lineName;
  }


  public Long getMaterialArriveAverageDuration() {
    return materialArriveAverageDuration;
  }

  public void setMaterialArriveAverageDuration(Long materialArriveAverageDuration) {
    this.materialArriveAverageDuration = materialArriveAverageDuration;
  }


  public Long getEntryWarehouseAverageDuration() {
    return entryWarehouseAverageDuration;
  }

  public void setEntryWarehouseAverageDuration(Long entryWarehouseAverageDuration) {
    this.entryWarehouseAverageDuration = entryWarehouseAverageDuration;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
