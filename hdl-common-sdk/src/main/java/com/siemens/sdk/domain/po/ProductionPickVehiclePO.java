package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_pick_vehicle" )
public class ProductionPickVehiclePO  implements Serializable {

	private static final long serialVersionUID =  1185827616467215180L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 成品载具领料编号
	 */
   	@Column(name = "production_vehicle_pick_no" )
	private Long productionVehiclePickNo;

	/**
	 * 成品载具需求编号
	 */
   	@Column(name = "vehicle_requirement_no" )
	private Long vehicleRequirementNo;

	/**
	 * 成品工单编号
	 */
   	@Column(name = "work_order_id" )
	private Long workOrderId;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 载具编码
	 */
   	@Column(name = "vehicle_code" )
	private Long vehicleCode;

	/**
	 * 载具名称
	 */
   	@Column(name = "vehicle_name" )
	private String vehicleName;

	/**
	 * 领具数量
	 */
   	@Column(name = "pick_quantity" )
	private Long pickQuantity;

	/**
	 * 物料单位编码
	 */
   	@Column(name = "vehicle_unit_code" )
	private Long vehicleUnitCode;

	/**
	 * 物料单位名称
	 */
   	@Column(name = "vehicle_unit_name" )
	private String vehicleUnitName;

	/**
	 * 领料时间
	 */
   	@Column(name = "pick_time" )
	private Date pickTime;

	/**
	 * 领料单号
	 */
   	@Column(name = "pick_bill_number" )
	private Long pickBillNumber;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getProductionVehiclePickNo() {
    return productionVehiclePickNo;
  }

  public void setProductionVehiclePickNo(Long productionVehiclePickNo) {
    this.productionVehiclePickNo = productionVehiclePickNo;
  }


  public Long getVehicleRequirementNo() {
    return vehicleRequirementNo;
  }

  public void setVehicleRequirementNo(Long vehicleRequirementNo) {
    this.vehicleRequirementNo = vehicleRequirementNo;
  }


  public Long getWorkOrderId() {
    return workOrderId;
  }

  public void setWorkOrderId(Long workOrderId) {
    this.workOrderId = workOrderId;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public Long getVehicleCode() {
    return vehicleCode;
  }

  public void setVehicleCode(Long vehicleCode) {
    this.vehicleCode = vehicleCode;
  }


  public String getVehicleName() {
    return vehicleName;
  }

  public void setVehicleName(String vehicleName) {
    this.vehicleName = vehicleName;
  }


  public Long getPickQuantity() {
    return pickQuantity;
  }

  public void setPickQuantity(Long pickQuantity) {
    this.pickQuantity = pickQuantity;
  }


  public Long getVehicleUnitCode() {
    return vehicleUnitCode;
  }

  public void setVehicleUnitCode(Long vehicleUnitCode) {
    this.vehicleUnitCode = vehicleUnitCode;
  }


  public String getVehicleUnitName() {
    return vehicleUnitName;
  }

  public void setVehicleUnitName(String vehicleUnitName) {
    this.vehicleUnitName = vehicleUnitName;
  }


  public Date getPickTime() {
    return pickTime;
  }

  public void setPickTime(Date pickTime) {
    this.pickTime = pickTime;
  }


  public Long getPickBillNumber() {
    return pickBillNumber;
  }

  public void setPickBillNumber(Long pickBillNumber) {
    this.pickBillNumber = pickBillNumber;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
