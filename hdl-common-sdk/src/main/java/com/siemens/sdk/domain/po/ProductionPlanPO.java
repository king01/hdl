package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_plan" )
public class ProductionPlanPO  implements Serializable {

	private static final long serialVersionUID =  8172981268694081942L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 计划编号
	 */
   	@Column(name = "plan_no" )
	private String planNo;

	/**
	 * 计划状态
	 */
   	@Column(name = "plan_status" )
	private String planStatus;

	/**
	 * 菜品编号
	 */
   	@Column(name = "sku_no" )
	private Long skuNo;

	/**
	 * 菜品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 满箱数量
	 */
   	@Column(name = "full_quantity" )
	private Long fullQuantity;

	/**
	 * 半箱数量
	 */
   	@Column(name = "half_quantity" )
	private Long halfQuantity;

	/**
	 * 交付单位编号
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 交付单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 优先级：10041001=一般；10041002=高
	 */
   	@Column(name = "priority" )
	private String priority;

	/**
	 * 描述
	 */
   	@Column(name = "desc" )
	private String desc;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getPlanNo() {
    return planNo;
  }

  public void setPlanNo(String planNo) {
    this.planNo = planNo;
  }


  public String getPlanStatus() {
    return planStatus;
  }

  public void setPlanStatus(String planStatus) {
    this.planStatus = planStatus;
  }


  public Long getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(Long skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Long getFullQuantity() {
    return fullQuantity;
  }

  public void setFullQuantity(Long fullQuantity) {
    this.fullQuantity = fullQuantity;
  }


  public Long getHalfQuantity() {
    return halfQuantity;
  }

  public void setHalfQuantity(Long halfQuantity) {
    this.halfQuantity = halfQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getPriority() {
    return priority;
  }

  public void setPriority(String priority) {
    this.priority = priority;
  }


  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
