package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_relation_bind" )
public class ProductionRelationBindPO  implements Serializable {

	private static final long serialVersionUID =  3047172677655463628L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 产线参数编号
	 */
   	@Column(name = "parameter_no" )
	private String parameterNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 产线编码
	 */
   	@Column(name = "production_line_no" )
	private String productionLineNo;

	/**
	 * 产线名称
	 */
   	@Column(name = "production_line_name" )
	private String productionLineName;

	/**
	 * 菜品编码
	 */
   	@Column(name = "sku_no" )
	private Long skuNo;

	/**
	 * 菜品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 是否启用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 产能参数
	 */
   	@Column(name = "capacity_val" )
	private BigDecimal capacityVal;

	/**
	 * 产能单位编码
	 */
   	@Column(name = "capacity_unit_code" )
	private String capacityUnitCode;

	/**
	 * 产能单位名称
	 */
   	@Column(name = "capacity_unit_name" )
	private String capacityUnitName;

	/**
	 * 是否人工依赖：30171001=是；30171002=否
	 */
   	@Column(name = "is_depend_human" )
	private String isDependHuman;

	/**
	 * 标准用工(人)
	 */
   	@Column(name = "standard_depend_human" )
	private Long standardDependHuman;

	/**
	 * 最大用工(人)
	 */
   	@Column(name = "max_depend_human" )
	private Long maxDependHuman;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getParameterNo() {
    return parameterNo;
  }

  public void setParameterNo(String parameterNo) {
    this.parameterNo = parameterNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getProductionLineNo() {
    return productionLineNo;
  }

  public void setProductionLineNo(String productionLineNo) {
    this.productionLineNo = productionLineNo;
  }


  public String getProductionLineName() {
    return productionLineName;
  }

  public void setProductionLineName(String productionLineName) {
    this.productionLineName = productionLineName;
  }


  public Long getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(Long skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public BigDecimal getCapacityVal() {
    return capacityVal;
  }

  public void setCapacityVal(BigDecimal capacityVal) {
    this.capacityVal = capacityVal;
  }


  public String getCapacityUnitCode() {
    return capacityUnitCode;
  }

  public void setCapacityUnitCode(String capacityUnitCode) {
    this.capacityUnitCode = capacityUnitCode;
  }


  public String getCapacityUnitName() {
    return capacityUnitName;
  }

  public void setCapacityUnitName(String capacityUnitName) {
    this.capacityUnitName = capacityUnitName;
  }


  public String getIsDependHuman() {
    return isDependHuman;
  }

  public void setIsDependHuman(String isDependHuman) {
    this.isDependHuman = isDependHuman;
  }


  public Long getStandardDependHuman() {
    return standardDependHuman;
  }

  public void setStandardDependHuman(Long standardDependHuman) {
    this.standardDependHuman = standardDependHuman;
  }


  public Long getMaxDependHuman() {
    return maxDependHuman;
  }

  public void setMaxDependHuman(Long maxDependHuman) {
    this.maxDependHuman = maxDependHuman;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
