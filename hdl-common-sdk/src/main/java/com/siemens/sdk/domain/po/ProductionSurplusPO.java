package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_surplus" )
public class ProductionSurplusPO  implements Serializable {

	private static final long serialVersionUID =  2267439667410399191L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 剩余产能编号
	 */
   	@Column(name = "surplus_no" )
	private Long surplusNo;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 商品别名
	 */
   	@Column(name = "sku_alias" )
	private String skuAlias;

	/**
	 * 商品规格
	 */
   	@Column(name = "sku_spec" )
	private String skuSpec;

	/**
	 * 商品单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 商品单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 原料类别：30121001=日配；30121002=库存；30121003=全部
	 */
   	@Column(name = "material_type" )
	private String materialType;

	/**
	 * 近期占比
	 */
   	@Column(name = "near_future_ratio" )
	private BigDecimal nearFutureRatio;

	/**
	 * 剩余产能
	 */
   	@Column(name = "surplus_quantity" )
	private Long surplusQuantity;

	/**
	 * 上浮比例
	 */
   	@Column(name = "raise_ratio" )
	private BigDecimal raiseRatio;

	/**
	 * 补单限制数量
	 */
   	@Column(name = "append_order_quantity_limit" )
	private Long appendOrderQuantityLimit;

	/**
	 * 生产日期
	 */
   	@Column(name = "production_date" )
	private Date productionDate;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public Long getSurplusNo() {
    return surplusNo;
  }

  public void setSurplusNo(Long surplusNo) {
    this.surplusNo = surplusNo;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public String getSkuAlias() {
    return skuAlias;
  }

  public void setSkuAlias(String skuAlias) {
    this.skuAlias = skuAlias;
  }


  public String getSkuSpec() {
    return skuSpec;
  }

  public void setSkuSpec(String skuSpec) {
    this.skuSpec = skuSpec;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getMaterialType() {
    return materialType;
  }

  public void setMaterialType(String materialType) {
    this.materialType = materialType;
  }


  public BigDecimal getNearFutureRatio() {
    return nearFutureRatio;
  }

  public void setNearFutureRatio(BigDecimal nearFutureRatio) {
    this.nearFutureRatio = nearFutureRatio;
  }


  public Long getSurplusQuantity() {
    return surplusQuantity;
  }

  public void setSurplusQuantity(Long surplusQuantity) {
    this.surplusQuantity = surplusQuantity;
  }


  public BigDecimal getRaiseRatio() {
    return raiseRatio;
  }

  public void setRaiseRatio(BigDecimal raiseRatio) {
    this.raiseRatio = raiseRatio;
  }


  public Long getAppendOrderQuantityLimit() {
    return appendOrderQuantityLimit;
  }

  public void setAppendOrderQuantityLimit(Long appendOrderQuantityLimit) {
    this.appendOrderQuantityLimit = appendOrderQuantityLimit;
  }


  public Date getProductionDate() {
    return productionDate;
  }

  public void setProductionDate(Date productionDate) {
    this.productionDate = productionDate;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
