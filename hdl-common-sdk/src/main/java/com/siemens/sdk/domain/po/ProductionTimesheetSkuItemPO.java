package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_timesheet_sku_item" )
public class ProductionTimesheetSkuItemPO  implements Serializable {

	private static final long serialVersionUID =  7280550913718861841L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 报工菜品明细编号
	 */
   	@Column(name = "timesheet_item_sku_no" )
	private String timesheetItemSkuNo;

	/**
	 * 报工明细编号
	 */
   	@Column(name = "timesheet_item_no" )
	private String timesheetItemNo;

	/**
	 * 报工编号
	 */
   	@Column(name = "timesheet_no" )
	private String timesheetNo;

	/**
	 * 报工方式：30091001=手动；30091002=自动
	 */
   	@Column(name = "timesheet_mode" )
	private String timesheetMode;

	/**
	 * 商品编码
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 数量
	 */
   	@Column(name = "quantity" )
	private Long quantity;

	/**
	 * 单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 餐盘规格编码
	 */
   	@Column(name = "vehicle_spec_no" )
	private String vehicleSpecNo;

	/**
	 * 餐盘规格名称
	 */
   	@Column(name = "vehicle_spec_name" )
	private String vehicleSpecName;

	/**
	 * 餐盘RFID
	 */
   	@Column(name = "rfid" )
	private String rfid;

	/**
	 * 二维码
	 */
   	@Column(name = "qr_code" )
	private String qrCode;

	/**
	 * 实际重量
	 */
   	@Column(name = "actual_weight" )
	private Long actualWeight;

	/**
	 * 实际重量单位编码
	 */
   	@Column(name = "actual_weight_unit_code" )
	private String actualWeightUnitCode;

	/**
	 * 实际重量单位名称
	 */
   	@Column(name = "actual_weight_unit_name" )
	private String actualWeightUnitName;

	/**
	 * 排列顺序
	 */
   	@Column(name = "order_seq" )
	private Long orderSeq;

	/**
	 * 排列位置说明
	 */
   	@Column(name = "position_explain" )
	private String positionExplain;

	/**
	 * 数据来源
	 */
   	@Column(name = "data_source" )
	private String dataSource;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getTimesheetItemSkuNo() {
    return timesheetItemSkuNo;
  }

  public void setTimesheetItemSkuNo(String timesheetItemSkuNo) {
    this.timesheetItemSkuNo = timesheetItemSkuNo;
  }


  public String getTimesheetItemNo() {
    return timesheetItemNo;
  }

  public void setTimesheetItemNo(String timesheetItemNo) {
    this.timesheetItemNo = timesheetItemNo;
  }


  public String getTimesheetNo() {
    return timesheetNo;
  }

  public void setTimesheetNo(String timesheetNo) {
    this.timesheetNo = timesheetNo;
  }


  public String getTimesheetMode() {
    return timesheetMode;
  }

  public void setTimesheetMode(String timesheetMode) {
    this.timesheetMode = timesheetMode;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Long getQuantity() {
    return quantity;
  }

  public void setQuantity(Long quantity) {
    this.quantity = quantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getVehicleSpecNo() {
    return vehicleSpecNo;
  }

  public void setVehicleSpecNo(String vehicleSpecNo) {
    this.vehicleSpecNo = vehicleSpecNo;
  }


  public String getVehicleSpecName() {
    return vehicleSpecName;
  }

  public void setVehicleSpecName(String vehicleSpecName) {
    this.vehicleSpecName = vehicleSpecName;
  }


  public String getRfid() {
    return rfid;
  }

  public void setRfid(String rfid) {
    this.rfid = rfid;
  }


  public String getQrCode() {
    return qrCode;
  }

  public void setQrCode(String qrCode) {
    this.qrCode = qrCode;
  }


  public Long getActualWeight() {
    return actualWeight;
  }

  public void setActualWeight(Long actualWeight) {
    this.actualWeight = actualWeight;
  }


  public String getActualWeightUnitCode() {
    return actualWeightUnitCode;
  }

  public void setActualWeightUnitCode(String actualWeightUnitCode) {
    this.actualWeightUnitCode = actualWeightUnitCode;
  }


  public String getActualWeightUnitName() {
    return actualWeightUnitName;
  }

  public void setActualWeightUnitName(String actualWeightUnitName) {
    this.actualWeightUnitName = actualWeightUnitName;
  }


  public Long getOrderSeq() {
    return orderSeq;
  }

  public void setOrderSeq(Long orderSeq) {
    this.orderSeq = orderSeq;
  }


  public String getPositionExplain() {
    return positionExplain;
  }

  public void setPositionExplain(String positionExplain) {
    this.positionExplain = positionExplain;
  }


  public String getDataSource() {
    return dataSource;
  }

  public void setDataSource(String dataSource) {
    this.dataSource = dataSource;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
