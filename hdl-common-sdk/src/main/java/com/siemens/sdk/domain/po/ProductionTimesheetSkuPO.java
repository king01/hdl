package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_timesheet_sku" )
public class ProductionTimesheetSkuPO  implements Serializable {

	private static final long serialVersionUID =  5760872668510488224L;

	/**
	 * 报工明细菜品编号
	 */
	@Id
   	@Column(name = "timesheet_item_sku_id" )
	private String timesheetItemSkuId;

	/**
	 * 报工明细编号
	 */
   	@Column(name = "timesheet_item_id" )
	private String timesheetItemId;

	/**
	 * 报工编号
	 */
   	@Column(name = "timesheet_id" )
	private String timesheetId;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 报工方式
	 */
   	@Column(name = "timesheet_mode" )
	private String timesheetMode;

	/**
	 * 商品编码
	 */
   	@Column(name = "sku_id" )
	private String skuId;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 数量
	 */
   	@Column(name = "quantity" )
	private BigDecimal quantity;

	/**
	 * 单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 餐盘规格编码
	 */
   	@Column(name = "vehicle_spec_code" )
	private String vehicleSpecCode;

	/**
	 * 餐盘规格名称
	 */
   	@Column(name = "vehicle_spec_name" )
	private String vehicleSpecName;

	/**
	 * 餐盘RFID
	 */
   	@Column(name = "rfid" )
	private String rfid;

	/**
	 * 二维码
	 */
   	@Column(name = "qr_code" )
	private String qrCode;

	/**
	 * 实际重量
	 */
   	@Column(name = "actual_weight" )
	private String actualWeight;

	/**
	 * 实际重量单位编码
	 */
   	@Column(name = "actual_weight_unit_code" )
	private String actualWeightUnitCode;

	/**
	 * 实际重量单位名称
	 */
   	@Column(name = "actual_weight_unit_name" )
	private String actualWeightUnitName;

	/**
	 * 排列顺序
	 */
   	@Column(name = "order_seq" )
	private BigDecimal orderSeq;

	/**
	 * 排列位置说明
	 */
   	@Column(name = "position_explain" )
	private String positionExplain;

	/**
	 * 数据来源
	 */
   	@Column(name = "data_source" )
	private String dataSource;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private BigDecimal creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private BigDecimal createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private BigDecimal updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private BigDecimal updateTime;

  public String getTimesheetItemSkuId() {
    return timesheetItemSkuId;
  }

  public void setTimesheetItemSkuId(String timesheetItemSkuId) {
    this.timesheetItemSkuId = timesheetItemSkuId;
  }


  public String getTimesheetItemId() {
    return timesheetItemId;
  }

  public void setTimesheetItemId(String timesheetItemId) {
    this.timesheetItemId = timesheetItemId;
  }


  public String getTimesheetId() {
    return timesheetId;
  }

  public void setTimesheetId(String timesheetId) {
    this.timesheetId = timesheetId;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getTimesheetMode() {
    return timesheetMode;
  }

  public void setTimesheetMode(String timesheetMode) {
    this.timesheetMode = timesheetMode;
  }


  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getVehicleSpecCode() {
    return vehicleSpecCode;
  }

  public void setVehicleSpecCode(String vehicleSpecCode) {
    this.vehicleSpecCode = vehicleSpecCode;
  }


  public String getVehicleSpecName() {
    return vehicleSpecName;
  }

  public void setVehicleSpecName(String vehicleSpecName) {
    this.vehicleSpecName = vehicleSpecName;
  }


  public String getRfid() {
    return rfid;
  }

  public void setRfid(String rfid) {
    this.rfid = rfid;
  }


  public String getQrCode() {
    return qrCode;
  }

  public void setQrCode(String qrCode) {
    this.qrCode = qrCode;
  }


  public String getActualWeight() {
    return actualWeight;
  }

  public void setActualWeight(String actualWeight) {
    this.actualWeight = actualWeight;
  }


  public String getActualWeightUnitCode() {
    return actualWeightUnitCode;
  }

  public void setActualWeightUnitCode(String actualWeightUnitCode) {
    this.actualWeightUnitCode = actualWeightUnitCode;
  }


  public String getActualWeightUnitName() {
    return actualWeightUnitName;
  }

  public void setActualWeightUnitName(String actualWeightUnitName) {
    this.actualWeightUnitName = actualWeightUnitName;
  }


  public BigDecimal getOrderSeq() {
    return orderSeq;
  }

  public void setOrderSeq(BigDecimal orderSeq) {
    this.orderSeq = orderSeq;
  }


  public String getPositionExplain() {
    return positionExplain;
  }

  public void setPositionExplain(String positionExplain) {
    this.positionExplain = positionExplain;
  }


  public String getDataSource() {
    return dataSource;
  }

  public void setDataSource(String dataSource) {
    this.dataSource = dataSource;
  }


  public BigDecimal getCreator() {
    return creator;
  }

  public void setCreator(BigDecimal creator) {
    this.creator = creator;
  }


  public BigDecimal getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(BigDecimal createdTime) {
    this.createdTime = createdTime;
  }


  public BigDecimal getUpdated() {
    return updated;
  }

  public void setUpdated(BigDecimal updated) {
    this.updated = updated;
  }


  public BigDecimal getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(BigDecimal updateTime) {
    this.updateTime = updateTime;
  }

}
