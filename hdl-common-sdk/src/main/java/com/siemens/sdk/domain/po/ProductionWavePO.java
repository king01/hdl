package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_wave" )
public class ProductionWavePO  implements Serializable {

	private static final long serialVersionUID =  6465155880308948402L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 波次编号
	 */
   	@Column(name = "wave_no" )
	private Long waveNo;

	/**
	 * 波次名称
	 */
   	@Column(name = "wave_name" )
	private String waveName;

	/**
	 * 波次数值
	 */
   	@Column(name = "wave_value" )
	private Long waveValue;

	/**
	 * 波次开始时间
	 */
   	@Column(name = "wave_start_time" )
	private Date waveStartTime;

	/**
	 * 波次结束时间
	 */
   	@Column(name = "wave_end_time" )
	private Date waveEndTime;

	/**
	 * 波次描述
	 */
   	@Column(name = "wave_desc" )
	private String waveDesc;

	/**
	 * 发货时间
	 */
   	@Column(name = "delivery_time" )
	private Date deliveryTime;

	/**
	 * 是否启用：10011001=启用，10011002=停用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public Long getWaveNo() {
    return waveNo;
  }

  public void setWaveNo(Long waveNo) {
    this.waveNo = waveNo;
  }


  public String getWaveName() {
    return waveName;
  }

  public void setWaveName(String waveName) {
    this.waveName = waveName;
  }


  public Long getWaveValue() {
    return waveValue;
  }

  public void setWaveValue(Long waveValue) {
    this.waveValue = waveValue;
  }


  public Date getWaveStartTime() {
    return waveStartTime;
  }

  public void setWaveStartTime(Date waveStartTime) {
    this.waveStartTime = waveStartTime;
  }


  public Date getWaveEndTime() {
    return waveEndTime;
  }

  public void setWaveEndTime(Date waveEndTime) {
    this.waveEndTime = waveEndTime;
  }


  public String getWaveDesc() {
    return waveDesc;
  }

  public void setWaveDesc(String waveDesc) {
    this.waveDesc = waveDesc;
  }


  public Date getDeliveryTime() {
    return deliveryTime;
  }

  public void setDeliveryTime(Date deliveryTime) {
    this.deliveryTime = deliveryTime;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
