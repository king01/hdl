package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_production_work_order_migration" )
public class ProductionWorkOrderMigrationPO  implements Serializable {

	private static final long serialVersionUID =  2744685602860869424L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 成品工单迁移编号
	 */
   	@Column(name = "work_order_migration_no" )
	private String workOrderMigrationNo;

	/**
	 * 成品工单编号
	 */
   	@Column(name = "work_order_no" )
	private String workOrderNo;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private String skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 生产日期
	 */
   	@Column(name = "production_date" )
	private Date productionDate;

	/**
	 * 原产线编码
	 */
   	@Column(name = "src_line_no" )
	private String srcLineNo;

	/**
	 * 原产线名称
	 */
   	@Column(name = "src_line_name" )
	private String srcLineName;

	/**
	 * 目标产线编码
	 */
   	@Column(name = "dest_line_no" )
	private String destLineNo;

	/**
	 * 目标产线名称
	 */
   	@Column(name = "dest_line_name" )
	private String destLineName;

	/**
	 * 迁移时间
	 */
   	@Column(name = "migration_time" )
	private Date migrationTime;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getWorkOrderMigrationNo() {
    return workOrderMigrationNo;
  }

  public void setWorkOrderMigrationNo(String workOrderMigrationNo) {
    this.workOrderMigrationNo = workOrderMigrationNo;
  }


  public String getWorkOrderNo() {
    return workOrderNo;
  }

  public void setWorkOrderNo(String workOrderNo) {
    this.workOrderNo = workOrderNo;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(String skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public Date getProductionDate() {
    return productionDate;
  }

  public void setProductionDate(Date productionDate) {
    this.productionDate = productionDate;
  }


  public String getSrcLineNo() {
    return srcLineNo;
  }

  public void setSrcLineNo(String srcLineNo) {
    this.srcLineNo = srcLineNo;
  }


  public String getSrcLineName() {
    return srcLineName;
  }

  public void setSrcLineName(String srcLineName) {
    this.srcLineName = srcLineName;
  }


  public String getDestLineNo() {
    return destLineNo;
  }

  public void setDestLineNo(String destLineNo) {
    this.destLineNo = destLineNo;
  }


  public String getDestLineName() {
    return destLineName;
  }

  public void setDestLineName(String destLineName) {
    this.destLineName = destLineName;
  }


  public Date getMigrationTime() {
    return migrationTime;
  }

  public void setMigrationTime(Date migrationTime) {
    this.migrationTime = migrationTime;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
