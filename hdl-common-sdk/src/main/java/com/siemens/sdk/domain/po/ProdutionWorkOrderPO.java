package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_prodution_work_order" )
public class ProdutionWorkOrderPO  implements Serializable {

	private static final long serialVersionUID =  2113834873771613956L;

	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 成品工单编号
	 */
   	@Column(name = "work_order_no" )
	private Long workOrderNo;

	/**
	 * 排产编号
	 */
   	@Column(name = "aps_order_no" )
	private Long apsOrderNo;

	/**
	 * 排产号
	 */
   	@Column(name = "aps_no" )
	private Long apsNo;

	/**
	 * 排产法则：30011001=最早交期法则；30011002=最短作业时间法则；30011003=关键性比率法则；30011004=最小能耗法则；30011005=其他法则
	 */
   	@Column(name = "aps_rule" )
	private String apsRule;

	/**
	 * 工单状态
	 */
   	@Column(name = "work_order_status" )
	private String workOrderStatus;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_no" )
	private Long skuNo;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 商品别名
	 */
   	@Column(name = "sku_alias" )
	private String skuAlias;

	/**
	 * 商品规格
	 */
   	@Column(name = "sku_spec" )
	private String skuSpec;

	/**
	 * 满箱交付数量
	 */
   	@Column(name = "full_delivery_quantity" )
	private Long fullDeliveryQuantity;

	/**
	 * 半箱交付数量
	 */
   	@Column(name = "half_delivery_quantity" )
	private Long halfDeliveryQuantity;

	/**
	 * 商品单位编码
	 */
   	@Column(name = "unit_no" )
	private Long unitNo;

	/**
	 * 商品单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 预开始时间
	 */
   	@Column(name = "pre_start_time" )
	private Date preStartTime;

	/**
	 * 预结束时间
	 */
   	@Column(name = "pre_end_time" )
	private Date preEndTime;

	/**
	 * 生产日期
	 */
   	@Column(name = "production_date" )
	private Date productionDate;

	/**
	 * 预生产产线编码
	 */
   	@Column(name = "pre_line_no" )
	private Long preLineNo;

	/**
	 * 预生产产线名称
	 */
   	@Column(name = "pre_line_name" )
	private String preLineName;

	/**
	 * 工单生成类型：30151001=自动；30151002手动
	 */
   	@Column(name = "work_order_type" )
	private String workOrderType;

	/**
	 * 排列序号
	 */
   	@Column(name = "order_seq" )
	private Long orderSeq;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public Long getWorkOrderNo() {
    return workOrderNo;
  }

  public void setWorkOrderNo(Long workOrderNo) {
    this.workOrderNo = workOrderNo;
  }


  public Long getApsOrderNo() {
    return apsOrderNo;
  }

  public void setApsOrderNo(Long apsOrderNo) {
    this.apsOrderNo = apsOrderNo;
  }


  public Long getApsNo() {
    return apsNo;
  }

  public void setApsNo(Long apsNo) {
    this.apsNo = apsNo;
  }


  public String getApsRule() {
    return apsRule;
  }

  public void setApsRule(String apsRule) {
    this.apsRule = apsRule;
  }


  public String getWorkOrderStatus() {
    return workOrderStatus;
  }

  public void setWorkOrderStatus(String workOrderStatus) {
    this.workOrderStatus = workOrderStatus;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public Long getSkuNo() {
    return skuNo;
  }

  public void setSkuNo(Long skuNo) {
    this.skuNo = skuNo;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public String getSkuAlias() {
    return skuAlias;
  }

  public void setSkuAlias(String skuAlias) {
    this.skuAlias = skuAlias;
  }


  public String getSkuSpec() {
    return skuSpec;
  }

  public void setSkuSpec(String skuSpec) {
    this.skuSpec = skuSpec;
  }


  public Long getFullDeliveryQuantity() {
    return fullDeliveryQuantity;
  }

  public void setFullDeliveryQuantity(Long fullDeliveryQuantity) {
    this.fullDeliveryQuantity = fullDeliveryQuantity;
  }


  public Long getHalfDeliveryQuantity() {
    return halfDeliveryQuantity;
  }

  public void setHalfDeliveryQuantity(Long halfDeliveryQuantity) {
    this.halfDeliveryQuantity = halfDeliveryQuantity;
  }


  public Long getUnitNo() {
    return unitNo;
  }

  public void setUnitNo(Long unitNo) {
    this.unitNo = unitNo;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Date getPreStartTime() {
    return preStartTime;
  }

  public void setPreStartTime(Date preStartTime) {
    this.preStartTime = preStartTime;
  }


  public Date getPreEndTime() {
    return preEndTime;
  }

  public void setPreEndTime(Date preEndTime) {
    this.preEndTime = preEndTime;
  }


  public Date getProductionDate() {
    return productionDate;
  }

  public void setProductionDate(Date productionDate) {
    this.productionDate = productionDate;
  }


  public Long getPreLineNo() {
    return preLineNo;
  }

  public void setPreLineNo(Long preLineNo) {
    this.preLineNo = preLineNo;
  }


  public String getPreLineName() {
    return preLineName;
  }

  public void setPreLineName(String preLineName) {
    this.preLineName = preLineName;
  }


  public String getWorkOrderType() {
    return workOrderType;
  }

  public void setWorkOrderType(String workOrderType) {
    this.workOrderType = workOrderType;
  }


  public Long getOrderSeq() {
    return orderSeq;
  }

  public void setOrderSeq(Long orderSeq) {
    this.orderSeq = orderSeq;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
