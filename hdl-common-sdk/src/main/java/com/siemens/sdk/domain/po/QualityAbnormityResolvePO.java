package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.io.InputStream;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_quality_abnormity_resolve" )
public class QualityAbnormityResolvePO  implements Serializable {

	private static final long serialVersionUID =  4788888656186463241L;

	/**
	 * 主键
	 */
	@Id
   	@Column(name = "id" )
	private Long id;

	/**
	 * 异常处理编号
	 */
   	@Column(name = "abnormity_resolve_no" )
	private String abnormityResolveNo;

	/**
	 * 异常编号
	 */
   	@Column(name = "abnormity_no" )
	private String abnormityNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 处理状态：30041001=未处理；30041002=处理中；30041003=已处理
	 */
   	@Column(name = "resolve_status" )
	private String resolveStatus;

	/**
	 * 接受时间
	 */
   	@Column(name = "accept_time" )
	private Date acceptTime;

	/**
	 * 接受人
	 */
   	@Column(name = "accept_user_name" )
	private String acceptUserName;

	/**
	 * 处理操作：30131001=填写处理意见；30131002=确认已解决
	 */
   	@Column(name = "resolve_operate_type" )
	private String resolveOperateType;

	/**
	 * 处理意见：30141001=让步接收；30141002=批次更换；30141003=报废；30141004=正常使用；30141005=操作人员自行处理；30141006=维修
	 */
   	@Column(name = "resolve_type" )
	private String resolveType;

	/**
	 * 处理人
	 */
   	@Column(name = "handle_user_name" )
	private String handleUserName;

	/**
	 * 是否删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getAbnormityResolveNo() {
    return abnormityResolveNo;
  }

  public void setAbnormityResolveNo(String abnormityResolveNo) {
    this.abnormityResolveNo = abnormityResolveNo;
  }


  public String getAbnormityNo() {
    return abnormityNo;
  }

  public void setAbnormityNo(String abnormityNo) {
    this.abnormityNo = abnormityNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getResolveStatus() {
    return resolveStatus;
  }

  public void setResolveStatus(String resolveStatus) {
    this.resolveStatus = resolveStatus;
  }


  public Date getAcceptTime() {
    return acceptTime;
  }

  public void setAcceptTime(Date acceptTime) {
    this.acceptTime = acceptTime;
  }


  public String getAcceptUserName() {
    return acceptUserName;
  }

  public void setAcceptUserName(String acceptUserName) {
    this.acceptUserName = acceptUserName;
  }


  public String getResolveOperateType() {
    return resolveOperateType;
  }

  public void setResolveOperateType(String resolveOperateType) {
    this.resolveOperateType = resolveOperateType;
  }


  public String getResolveType() {
    return resolveType;
  }

  public void setResolveType(String resolveType) {
    this.resolveType = resolveType;
  }


  public String getHandleUserName() {
    return handleUserName;
  }

  public void setHandleUserName(String handleUserName) {
    this.handleUserName = handleUserName;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
