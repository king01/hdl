package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_requipment_maintenance_schedule" )
public class RequipmentMaintenanceSchedulePO  implements Serializable {

	private static final long serialVersionUID =  1617276661451784007L;

	/**
	 * 主键，20位数
	 */
	@Id
   	@Column(name = "ID" )
	private String id;

	/**
	 * 维修编号
	 */
   	@Column(name = "maintenance_no" )
	private String maintenanceNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 车间所属类型：20031001=预生产车间，20031002=分装车间
	 */
   	@Column(name = "belong_type" )
	private String belongType;

	/**
	 * 所属车间/产线编号
	 */
   	@Column(name = "belong_no" )
	private String belongNo;

	/**
	 * 所属车间/产线名称
	 */
   	@Column(name = "belong_name" )
	private String belongName;

	/**
	 * 设备编号
	 */
   	@Column(name = "equipment_no" )
	private String equipmentNo;

	/**
	 * 设备名称
	 */
   	@Column(name = "equipment_name" )
	private String equipmentName;

	/**
	 * 维修类型：20041001=计划检修，20041002=故障处理
	 */
   	@Column(name = "maintenance_type" )
	private String maintenanceType;

	/**
	 * 维修方式：20051001=在线检修，20051002=停线检修
	 */
   	@Column(name = "maintenance_mode" )
	private String maintenanceMode;

	/**
	 * 计划开始时间
	 */
   	@Column(name = "maintenance_start_time" )
	private Date maintenanceStartTime;

	/**
	 * 计划结束时间
	 */
   	@Column(name = "maintenance_end_time" )
	private Date maintenanceEndTime;

	/**
	 * 维修说明
	 */
   	@Column(name = "maintenance_explain" )
	private String maintenanceExplain;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getMaintenanceNo() {
    return maintenanceNo;
  }

  public void setMaintenanceNo(String maintenanceNo) {
    this.maintenanceNo = maintenanceNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getBelongType() {
    return belongType;
  }

  public void setBelongType(String belongType) {
    this.belongType = belongType;
  }


  public String getBelongNo() {
    return belongNo;
  }

  public void setBelongNo(String belongNo) {
    this.belongNo = belongNo;
  }


  public String getBelongName() {
    return belongName;
  }

  public void setBelongName(String belongName) {
    this.belongName = belongName;
  }


  public String getEquipmentNo() {
    return equipmentNo;
  }

  public void setEquipmentNo(String equipmentNo) {
    this.equipmentNo = equipmentNo;
  }


  public String getEquipmentName() {
    return equipmentName;
  }

  public void setEquipmentName(String equipmentName) {
    this.equipmentName = equipmentName;
  }


  public String getMaintenanceType() {
    return maintenanceType;
  }

  public void setMaintenanceType(String maintenanceType) {
    this.maintenanceType = maintenanceType;
  }


  public String getMaintenanceMode() {
    return maintenanceMode;
  }

  public void setMaintenanceMode(String maintenanceMode) {
    this.maintenanceMode = maintenanceMode;
  }


  public Date getMaintenanceStartTime() {
    return maintenanceStartTime;
  }

  public void setMaintenanceStartTime(Date maintenanceStartTime) {
    this.maintenanceStartTime = maintenanceStartTime;
  }


  public Date getMaintenanceEndTime() {
    return maintenanceEndTime;
  }

  public void setMaintenanceEndTime(Date maintenanceEndTime) {
    this.maintenanceEndTime = maintenanceEndTime;
  }


  public String getMaintenanceExplain() {
    return maintenanceExplain;
  }

  public void setMaintenanceExplain(String maintenanceExplain) {
    this.maintenanceExplain = maintenanceExplain;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
