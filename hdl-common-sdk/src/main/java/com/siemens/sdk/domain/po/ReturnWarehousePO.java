package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_return_warehouse" )
public class ReturnWarehousePO  implements Serializable {

	private static final long serialVersionUID =  1143770988044845387L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 退库编号
	 */
   	@Column(name = "return_record_no" )
	private String returnRecordNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 退库申请单号
	 */
   	@Column(name = "return_apply_bill_no" )
	private String returnApplyBillNo;

	/**
	 * 退库状态：20061001=已创建，20061002=已发起，20061003=处理中，20061004=已完成
	 */
   	@Column(name = "status" )
	private String status;

	/**
	 * 退库类型：20071001=正常余料退库，20071002=质量问题退库，20071003=批次更换
	 */
   	@Column(name = "return_type" )
	private String returnType;

	/**
	 * 线边编号
	 */
   	@Column(name = "line_no" )
	private String lineNo;

	/**
	 * 线边名称
	 */
   	@Column(name = "line_name" )
	private String lineName;

	/**
	 * 交货日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave_no" )
	private String deliveryWaveNo;

	/**
	 * 申请时间
	 */
   	@Column(name = "apply_time" )
	private Date applyTime;

	/**
	 * 完成时间
	 */
   	@Column(name = "end_time" )
	private Date endTime;

	/**
	 * 退库单
	 */
   	@Column(name = "return_bill_no" )
	private String returnBillNo;

	/**
	 * 退库描述
	 */
   	@Column(name = "demage_desc" )
	private String demageDesc;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getReturnRecordNo() {
    return returnRecordNo;
  }

  public void setReturnRecordNo(String returnRecordNo) {
    this.returnRecordNo = returnRecordNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getReturnApplyBillNo() {
    return returnApplyBillNo;
  }

  public void setReturnApplyBillNo(String returnApplyBillNo) {
    this.returnApplyBillNo = returnApplyBillNo;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getReturnType() {
    return returnType;
  }

  public void setReturnType(String returnType) {
    this.returnType = returnType;
  }


  public String getLineNo() {
    return lineNo;
  }

  public void setLineNo(String lineNo) {
    this.lineNo = lineNo;
  }


  public String getLineName() {
    return lineName;
  }

  public void setLineName(String lineName) {
    this.lineName = lineName;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public String getDeliveryWaveNo() {
    return deliveryWaveNo;
  }

  public void setDeliveryWaveNo(String deliveryWaveNo) {
    this.deliveryWaveNo = deliveryWaveNo;
  }


  public Date getApplyTime() {
    return applyTime;
  }

  public void setApplyTime(Date applyTime) {
    this.applyTime = applyTime;
  }


  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }


  public String getReturnBillNo() {
    return returnBillNo;
  }

  public void setReturnBillNo(String returnBillNo) {
    this.returnBillNo = returnBillNo;
  }


  public String getDemageDesc() {
    return demageDesc;
  }

  public void setDemageDesc(String demageDesc) {
    this.demageDesc = demageDesc;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
