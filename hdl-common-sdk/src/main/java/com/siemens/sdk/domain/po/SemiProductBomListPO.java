package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_semi_product_bom_list" )
public class SemiProductBomListPO  implements Serializable {

	private static final long serialVersionUID =  4936494149224303606L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 商品Bom编号
	 */
   	@Column(name = "semi_product_bom_no" )
	private String semiProductBomNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 组件编号
	 */
   	@Column(name = "raw_material_no" )
	private String rawMaterialNo;

	/**
	 * 组件名称
	 */
   	@Column(name = "raw_material_name" )
	private String rawMaterialName;

	/**
	 * 组件数量
	 */
   	@Column(name = "unit_quantity" )
	private Long unitQuantity;

	/**
	 * 组件单位编号
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 组件单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 物料标识：20081001=A，20081002=B，20081003=C
	 */
   	@Column(name = "material_flag" )
	private String materialFlag;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSemiProductBomNo() {
    return semiProductBomNo;
  }

  public void setSemiProductBomNo(String semiProductBomNo) {
    this.semiProductBomNo = semiProductBomNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public String getRawMaterialNo() {
    return rawMaterialNo;
  }

  public void setRawMaterialNo(String rawMaterialNo) {
    this.rawMaterialNo = rawMaterialNo;
  }


  public String getRawMaterialName() {
    return rawMaterialName;
  }

  public void setRawMaterialName(String rawMaterialName) {
    this.rawMaterialName = rawMaterialName;
  }


  public Long getUnitQuantity() {
    return unitQuantity;
  }

  public void setUnitQuantity(Long unitQuantity) {
    this.unitQuantity = unitQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getMaterialFlag() {
    return materialFlag;
  }

  public void setMaterialFlag(String materialFlag) {
    this.materialFlag = materialFlag;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
