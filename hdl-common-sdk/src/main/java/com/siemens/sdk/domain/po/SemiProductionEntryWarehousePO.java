package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_semi_production_entry_warehouse" )
public class SemiProductionEntryWarehousePO  implements Serializable {

	private static final long serialVersionUID =  9121472301345291301L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 入库编号
	 */
   	@Column(name = "entry_warehouse_no" )
	private String entryWarehouseNo;

	/**
	 * 预处理生产执行编号
	 */
   	@Column(name = "pre_execute_no" )
	private String preExecuteNo;

	/**
	 * 半成品工单编号
	 */
   	@Column(name = "semi_work_order_no" )
	private String semiWorkOrderNo;

	/**
	 * 半成品派工编号
	 */
   	@Column(name = "semi_production_dispatch_no" )
	private String semiProductionDispatchNo;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 入库数量
	 */
   	@Column(name = "entry_quantity" )
	private Long entryQuantity;

	/**
	 * 物料单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 物料单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 入库类型：20091001=正常，20091002=越库
	 */
   	@Column(name = "entry_type" )
	private String entryType;

	/**
	 * 入库申请时间
	 */
   	@Column(name = "entry_apply_time" )
	private Date entryApplyTime;

	/**
	 * 入库完成时间
	 */
   	@Column(name = "entry_end_time" )
	private Date entryEndTime;

	/**
	 * 入库单号
	 */
   	@Column(name = "entry_bill_no" )
	private String entryBillNo;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getEntryWarehouseNo() {
    return entryWarehouseNo;
  }

  public void setEntryWarehouseNo(String entryWarehouseNo) {
    this.entryWarehouseNo = entryWarehouseNo;
  }


  public String getPreExecuteNo() {
    return preExecuteNo;
  }

  public void setPreExecuteNo(String preExecuteNo) {
    this.preExecuteNo = preExecuteNo;
  }


  public String getSemiWorkOrderNo() {
    return semiWorkOrderNo;
  }

  public void setSemiWorkOrderNo(String semiWorkOrderNo) {
    this.semiWorkOrderNo = semiWorkOrderNo;
  }


  public String getSemiProductionDispatchNo() {
    return semiProductionDispatchNo;
  }

  public void setSemiProductionDispatchNo(String semiProductionDispatchNo) {
    this.semiProductionDispatchNo = semiProductionDispatchNo;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Long getEntryQuantity() {
    return entryQuantity;
  }

  public void setEntryQuantity(Long entryQuantity) {
    this.entryQuantity = entryQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getEntryType() {
    return entryType;
  }

  public void setEntryType(String entryType) {
    this.entryType = entryType;
  }


  public Date getEntryApplyTime() {
    return entryApplyTime;
  }

  public void setEntryApplyTime(Date entryApplyTime) {
    this.entryApplyTime = entryApplyTime;
  }


  public Date getEntryEndTime() {
    return entryEndTime;
  }

  public void setEntryEndTime(Date entryEndTime) {
    this.entryEndTime = entryEndTime;
  }


  public String getEntryBillNo() {
    return entryBillNo;
  }

  public void setEntryBillNo(String entryBillNo) {
    this.entryBillNo = entryBillNo;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
