package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_semi_production_pick_material" )
public class SemiProductionPickMaterialPO  implements Serializable {

	private static final long serialVersionUID =  567398201669676460L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 半成品领料编号
	 */
   	@Column(name = "semi_production_pick_no" )
	private String semiProductionPickNo;

	/**
	 * 半成品物料需求编号
	 */
   	@Column(name = "semi_production_material_requirement_no" )
	private String semiProductionMaterialRequirementNo;

	/**
	 * 半成品工单编号
	 */
   	@Column(name = "semi_work_order_no" )
	private String semiWorkOrderNo;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 领料数量
	 */
   	@Column(name = "pick_quantity" )
	private Long pickQuantity;

	/**
	 * 物料单位编号
	 */
   	@Column(name = "material_unit_code" )
	private String materialUnitCode;

	/**
	 * 物料单位名称
	 */
   	@Column(name = "material_unit_name" )
	private String materialUnitName;

	/**
	 * 领料时间
	 */
   	@Column(name = "pick_time" )
	private Date pickTime;

	/**
	 * 领料单号
	 */
   	@Column(name = "pick_bill_no" )
	private String pickBillNo;

	/**
	 * 领料类型：20101001=原料领料，20101002=半成品领料，20101003=载具领料
	 */
   	@Column(name = "pick_type" )
	private String pickType;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getSemiProductionPickNo() {
    return semiProductionPickNo;
  }

  public void setSemiProductionPickNo(String semiProductionPickNo) {
    this.semiProductionPickNo = semiProductionPickNo;
  }


  public String getSemiProductionMaterialRequirementNo() {
    return semiProductionMaterialRequirementNo;
  }

  public void setSemiProductionMaterialRequirementNo(String semiProductionMaterialRequirementNo) {
    this.semiProductionMaterialRequirementNo = semiProductionMaterialRequirementNo;
  }


  public String getSemiWorkOrderNo() {
    return semiWorkOrderNo;
  }

  public void setSemiWorkOrderNo(String semiWorkOrderNo) {
    this.semiWorkOrderNo = semiWorkOrderNo;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Long getPickQuantity() {
    return pickQuantity;
  }

  public void setPickQuantity(Long pickQuantity) {
    this.pickQuantity = pickQuantity;
  }


  public String getMaterialUnitCode() {
    return materialUnitCode;
  }

  public void setMaterialUnitCode(String materialUnitCode) {
    this.materialUnitCode = materialUnitCode;
  }


  public String getMaterialUnitName() {
    return materialUnitName;
  }

  public void setMaterialUnitName(String materialUnitName) {
    this.materialUnitName = materialUnitName;
  }


  public Date getPickTime() {
    return pickTime;
  }

  public void setPickTime(Date pickTime) {
    this.pickTime = pickTime;
  }


  public String getPickBillNo() {
    return pickBillNo;
  }

  public void setPickBillNo(String pickBillNo) {
    this.pickBillNo = pickBillNo;
  }


  public String getPickType() {
    return pickType;
  }

  public void setPickType(String pickType) {
    this.pickType = pickType;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
