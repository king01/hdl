package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_semi_production_pick_vehicle" )
public class SemiProductionPickVehiclePO  implements Serializable {

	private static final long serialVersionUID =  637949506426109201L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 半成品载具领料编号
	 */
   	@Column(name = "semi_production_vehicle_pick_no" )
	private String semiProductionVehiclePickNo;

	/**
	 * 半成品载具需求编号
	 */
   	@Column(name = "vehicle_requirement_no" )
	private String vehicleRequirementNo;

	/**
	 * 半成品工单编号
	 */
   	@Column(name = "semi_work_order_no" )
	private String semiWorkOrderNo;

	/**
	 * 载具编号
	 */
   	@Column(name = "vehicle_no" )
	private String vehicleNo;

	/**
	 * 载具名称
	 */
   	@Column(name = "vehicle_name" )
	private String vehicleName;

	/**
	 * 领具数量
	 */
   	@Column(name = "pick_quantity" )
	private Long pickQuantity;

	/**
	 * 物料单位编码
	 */
   	@Column(name = "vehicle_unit_code" )
	private String vehicleUnitCode;

	/**
	 * 物料单位名称
	 */
   	@Column(name = "vehicle_unit_name" )
	private String vehicleUnitName;

	/**
	 * 领料时间
	 */
   	@Column(name = "pick_time" )
	private Date pickTime;

	/**
	 * 领料单号
	 */
   	@Column(name = "pick_bill_no" )
	private String pickBillNo;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getSemiProductionVehiclePickNo() {
    return semiProductionVehiclePickNo;
  }

  public void setSemiProductionVehiclePickNo(String semiProductionVehiclePickNo) {
    this.semiProductionVehiclePickNo = semiProductionVehiclePickNo;
  }


  public String getVehicleRequirementNo() {
    return vehicleRequirementNo;
  }

  public void setVehicleRequirementNo(String vehicleRequirementNo) {
    this.vehicleRequirementNo = vehicleRequirementNo;
  }


  public String getSemiWorkOrderNo() {
    return semiWorkOrderNo;
  }

  public void setSemiWorkOrderNo(String semiWorkOrderNo) {
    this.semiWorkOrderNo = semiWorkOrderNo;
  }


  public String getVehicleNo() {
    return vehicleNo;
  }

  public void setVehicleNo(String vehicleNo) {
    this.vehicleNo = vehicleNo;
  }


  public String getVehicleName() {
    return vehicleName;
  }

  public void setVehicleName(String vehicleName) {
    this.vehicleName = vehicleName;
  }


  public Long getPickQuantity() {
    return pickQuantity;
  }

  public void setPickQuantity(Long pickQuantity) {
    this.pickQuantity = pickQuantity;
  }


  public String getVehicleUnitCode() {
    return vehicleUnitCode;
  }

  public void setVehicleUnitCode(String vehicleUnitCode) {
    this.vehicleUnitCode = vehicleUnitCode;
  }


  public String getVehicleUnitName() {
    return vehicleUnitName;
  }

  public void setVehicleUnitName(String vehicleUnitName) {
    this.vehicleUnitName = vehicleUnitName;
  }


  public Date getPickTime() {
    return pickTime;
  }

  public void setPickTime(Date pickTime) {
    this.pickTime = pickTime;
  }


  public String getPickBillNo() {
    return pickBillNo;
  }

  public void setPickBillNo(String pickBillNo) {
    this.pickBillNo = pickBillNo;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
