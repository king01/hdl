package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_semi_production_vehicle_requirement" )
public class SemiProductionVehicleRequirementPO  implements Serializable {

	private static final long serialVersionUID =  3659768564176544134L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 半成品载具需求编号
	 */
   	@Column(name = "vehicle_requirement_no" )
	private String vehicleRequirementNo;

	/**
	 * 半成品工单编号
	 */
   	@Column(name = "semi_work_order_no" )
	private String semiWorkOrderNo;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 交付物料编号
	 */
   	@Column(name = "delivery_material_no" )
	private String deliveryMaterialNo;

	/**
	 * 交付物料名称
	 */
   	@Column(name = "delivery_material_name" )
	private String deliveryMaterialName;

	/**
	 * 交付数量
	 */
   	@Column(name = "delivery_quantity" )
	private Long deliveryQuantity;

	/**
	 * 交付物料单位编号
	 */
   	@Column(name = "delivery_material_unit_code" )
	private String deliveryMaterialUnitCode;

	/**
	 * 交付物料单位名称
	 */
   	@Column(name = "delivery_material_unit_name" )
	private String deliveryMaterialUnitName;

	/**
	 * 预生产车间编号
	 */
   	@Column(name = "pre_work_room_no" )
	private String preWorkRoomNo;

	/**
	 * 预生产车间名称
	 */
   	@Column(name = "pre_work_room_name" )
	private String preWorkRoomName;

	/**
	 * 载具编号
	 */
   	@Column(name = "vehicle_no" )
	private String vehicleNo;

	/**
	 * 载具名称
	 */
   	@Column(name = "vehicle_name" )
	private String vehicleName;

	/**
	 * 载具规格
	 */
   	@Column(name = "vehicle_spec" )
	private String vehicleSpec;

	/**
	 * 载具数量
	 */
   	@Column(name = "vehicle_quantity" )
	private Long vehicleQuantity;

	/**
	 * 领具日期
	 */
   	@Column(name = "pick_vehicle_date" )
	private Date pickVehicleDate;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getVehicleRequirementNo() {
    return vehicleRequirementNo;
  }

  public void setVehicleRequirementNo(String vehicleRequirementNo) {
    this.vehicleRequirementNo = vehicleRequirementNo;
  }


  public String getSemiWorkOrderNo() {
    return semiWorkOrderNo;
  }

  public void setSemiWorkOrderNo(String semiWorkOrderNo) {
    this.semiWorkOrderNo = semiWorkOrderNo;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getDeliveryMaterialNo() {
    return deliveryMaterialNo;
  }

  public void setDeliveryMaterialNo(String deliveryMaterialNo) {
    this.deliveryMaterialNo = deliveryMaterialNo;
  }


  public String getDeliveryMaterialName() {
    return deliveryMaterialName;
  }

  public void setDeliveryMaterialName(String deliveryMaterialName) {
    this.deliveryMaterialName = deliveryMaterialName;
  }


  public Long getDeliveryQuantity() {
    return deliveryQuantity;
  }

  public void setDeliveryQuantity(Long deliveryQuantity) {
    this.deliveryQuantity = deliveryQuantity;
  }


  public String getDeliveryMaterialUnitCode() {
    return deliveryMaterialUnitCode;
  }

  public void setDeliveryMaterialUnitCode(String deliveryMaterialUnitCode) {
    this.deliveryMaterialUnitCode = deliveryMaterialUnitCode;
  }


  public String getDeliveryMaterialUnitName() {
    return deliveryMaterialUnitName;
  }

  public void setDeliveryMaterialUnitName(String deliveryMaterialUnitName) {
    this.deliveryMaterialUnitName = deliveryMaterialUnitName;
  }


  public String getPreWorkRoomNo() {
    return preWorkRoomNo;
  }

  public void setPreWorkRoomNo(String preWorkRoomNo) {
    this.preWorkRoomNo = preWorkRoomNo;
  }


  public String getPreWorkRoomName() {
    return preWorkRoomName;
  }

  public void setPreWorkRoomName(String preWorkRoomName) {
    this.preWorkRoomName = preWorkRoomName;
  }


  public String getVehicleNo() {
    return vehicleNo;
  }

  public void setVehicleNo(String vehicleNo) {
    this.vehicleNo = vehicleNo;
  }


  public String getVehicleName() {
    return vehicleName;
  }

  public void setVehicleName(String vehicleName) {
    this.vehicleName = vehicleName;
  }


  public String getVehicleSpec() {
    return vehicleSpec;
  }

  public void setVehicleSpec(String vehicleSpec) {
    this.vehicleSpec = vehicleSpec;
  }


  public Long getVehicleQuantity() {
    return vehicleQuantity;
  }

  public void setVehicleQuantity(Long vehicleQuantity) {
    this.vehicleQuantity = vehicleQuantity;
  }


  public Date getPickVehicleDate() {
    return pickVehicleDate;
  }

  public void setPickVehicleDate(Date pickVehicleDate) {
    this.pickVehicleDate = pickVehicleDate;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
