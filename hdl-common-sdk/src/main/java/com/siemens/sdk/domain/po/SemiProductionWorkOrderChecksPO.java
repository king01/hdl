package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_semi_production_work_order_checks" )
public class SemiProductionWorkOrderChecksPO  implements Serializable {

	private static final long serialVersionUID =  4449696134700895173L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 开工检查编号
	 */
   	@Column(name = "start_work_item_no" )
	private String startWorkItemNo;

	/**
	 * 半成品工单编号
	 */
   	@Column(name = "semi_work_order_no" )
	private String semiWorkOrderNo;

	/**
	 * 交付日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 交付物料编号
	 */
   	@Column(name = "delivery_material_no" )
	private String deliveryMaterialNo;

	/**
	 * 交付物料名称
	 */
   	@Column(name = "delivery_material_name" )
	private String deliveryMaterialName;

	/**
	 * 交付数量
	 */
   	@Column(name = "delivery_quantity" )
	private Long deliveryQuantity;

	/**
	 * 交付物料单位编码
	 */
   	@Column(name = "delivery_material_unit_code" )
	private String deliveryMaterialUnitCode;

	/**
	 * 交付物料单位名称
	 */
   	@Column(name = "delivery_material_unit_name" )
	private String deliveryMaterialUnitName;

	/**
	 * 预生产车间编号
	 */
   	@Column(name = "pre_work_room_no" )
	private String preWorkRoomNo;

	/**
	 * 预生产车间名称
	 */
   	@Column(name = "pre_work_room_name" )
	private String preWorkRoomName;

	/**
	 * 预开始时间
	 */
   	@Column(name = "pre_start_time" )
	private Date preStartTime;

	/**
	 * 预结束时间
	 */
   	@Column(name = "pre_end_time" )
	private Date preEndTime;

	/**
	 * 检查项编号
	 */
   	@Column(name = "check_itme_no" )
	private String checkItmeNo;

	/**
	 * 检查项名称
	 */
   	@Column(name = "check_item_name" )
	private String checkItemName;

	/**
	 * 检查结果
	 */
   	@Column(name = "check_item_result" )
	private String checkItemResult;

	/**
	 * 检查顺序
	 */
   	@Column(name = "check_seq" )
	private Long checkSeq;

	/**
	 * 检查时间
	 */
   	@Column(name = "check_time" )
	private Date checkTime;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getStartWorkItemNo() {
    return startWorkItemNo;
  }

  public void setStartWorkItemNo(String startWorkItemNo) {
    this.startWorkItemNo = startWorkItemNo;
  }


  public String getSemiWorkOrderNo() {
    return semiWorkOrderNo;
  }

  public void setSemiWorkOrderNo(String semiWorkOrderNo) {
    this.semiWorkOrderNo = semiWorkOrderNo;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getDeliveryMaterialNo() {
    return deliveryMaterialNo;
  }

  public void setDeliveryMaterialNo(String deliveryMaterialNo) {
    this.deliveryMaterialNo = deliveryMaterialNo;
  }


  public String getDeliveryMaterialName() {
    return deliveryMaterialName;
  }

  public void setDeliveryMaterialName(String deliveryMaterialName) {
    this.deliveryMaterialName = deliveryMaterialName;
  }


  public Long getDeliveryQuantity() {
    return deliveryQuantity;
  }

  public void setDeliveryQuantity(Long deliveryQuantity) {
    this.deliveryQuantity = deliveryQuantity;
  }


  public String getDeliveryMaterialUnitCode() {
    return deliveryMaterialUnitCode;
  }

  public void setDeliveryMaterialUnitCode(String deliveryMaterialUnitCode) {
    this.deliveryMaterialUnitCode = deliveryMaterialUnitCode;
  }


  public String getDeliveryMaterialUnitName() {
    return deliveryMaterialUnitName;
  }

  public void setDeliveryMaterialUnitName(String deliveryMaterialUnitName) {
    this.deliveryMaterialUnitName = deliveryMaterialUnitName;
  }


  public String getPreWorkRoomNo() {
    return preWorkRoomNo;
  }

  public void setPreWorkRoomNo(String preWorkRoomNo) {
    this.preWorkRoomNo = preWorkRoomNo;
  }


  public String getPreWorkRoomName() {
    return preWorkRoomName;
  }

  public void setPreWorkRoomName(String preWorkRoomName) {
    this.preWorkRoomName = preWorkRoomName;
  }


  public Date getPreStartTime() {
    return preStartTime;
  }

  public void setPreStartTime(Date preStartTime) {
    this.preStartTime = preStartTime;
  }


  public Date getPreEndTime() {
    return preEndTime;
  }

  public void setPreEndTime(Date preEndTime) {
    this.preEndTime = preEndTime;
  }


  public String getCheckItmeNo() {
    return checkItmeNo;
  }

  public void setCheckItmeNo(String checkItmeNo) {
    this.checkItmeNo = checkItmeNo;
  }


  public String getCheckItemName() {
    return checkItemName;
  }

  public void setCheckItemName(String checkItemName) {
    this.checkItemName = checkItemName;
  }


  public String getCheckItemResult() {
    return checkItemResult;
  }

  public void setCheckItemResult(String checkItemResult) {
    this.checkItemResult = checkItemResult;
  }


  public Long getCheckSeq() {
    return checkSeq;
  }

  public void setCheckSeq(Long checkSeq) {
    this.checkSeq = checkSeq;
  }


  public Date getCheckTime() {
    return checkTime;
  }

  public void setCheckTime(Date checkTime) {
    this.checkTime = checkTime;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
