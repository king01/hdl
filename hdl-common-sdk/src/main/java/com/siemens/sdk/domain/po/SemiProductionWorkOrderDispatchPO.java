package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_semi_production_work_order_dispatch" )
public class SemiProductionWorkOrderDispatchPO  implements Serializable {

	private static final long serialVersionUID =  2722988440894639784L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 半成品派工单号
	 */
   	@Column(name = "semi_production_dispatch_no" )
	private String semiProductionDispatchNo;

	/**
	 * 半成品工单编号
	 */
   	@Column(name = "semi_work_order_no" )
	private String semiWorkOrderNo;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 交付数量
	 */
   	@Column(name = "delivery_quantity" )
	private Long deliveryQuantity;

	/**
	 * 物料单位编号
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 物料单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 预开始时间
	 */
   	@Column(name = "pre_start_time" )
	private Date preStartTime;

	/**
	 * 预结束时间
	 */
   	@Column(name = "pre_end_time" )
	private Date preEndTime;

	/**
	 * 生产日期
	 */
   	@Column(name = "production_date" )
	private Date productionDate;

	/**
	 * 预生产车间编号
	 */
   	@Column(name = "pre_work_room_no" )
	private String preWorkRoomNo;

	/**
	 * 预生产车间名称
	 */
   	@Column(name = "pre_work_room_name" )
	private String preWorkRoomName;

	/**
	 * 预估人工数
	 */
   	@Column(name = "pre_worker_number" )
	private Long preWorkerNumber;

	/**
	 * 班组编号
	 */
   	@Column(name = "team_no" )
	private String teamNo;

	/**
	 * 班组名称
	 */
   	@Column(name = "team_name" )
	private String teamName;

	/**
	 * 派工时间
	 */
   	@Column(name = "dispatch_time" )
	private Date dispatchTime;

	/**
	 * 派工方式：20111001=人工派工，20111002=自动派工
	 */
   	@Column(name = "dispatch_type" )
	private String dispatchType;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getSemiProductionDispatchNo() {
    return semiProductionDispatchNo;
  }

  public void setSemiProductionDispatchNo(String semiProductionDispatchNo) {
    this.semiProductionDispatchNo = semiProductionDispatchNo;
  }


  public String getSemiWorkOrderNo() {
    return semiWorkOrderNo;
  }

  public void setSemiWorkOrderNo(String semiWorkOrderNo) {
    this.semiWorkOrderNo = semiWorkOrderNo;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Long getDeliveryQuantity() {
    return deliveryQuantity;
  }

  public void setDeliveryQuantity(Long deliveryQuantity) {
    this.deliveryQuantity = deliveryQuantity;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Date getPreStartTime() {
    return preStartTime;
  }

  public void setPreStartTime(Date preStartTime) {
    this.preStartTime = preStartTime;
  }


  public Date getPreEndTime() {
    return preEndTime;
  }

  public void setPreEndTime(Date preEndTime) {
    this.preEndTime = preEndTime;
  }


  public Date getProductionDate() {
    return productionDate;
  }

  public void setProductionDate(Date productionDate) {
    this.productionDate = productionDate;
  }


  public String getPreWorkRoomNo() {
    return preWorkRoomNo;
  }

  public void setPreWorkRoomNo(String preWorkRoomNo) {
    this.preWorkRoomNo = preWorkRoomNo;
  }


  public String getPreWorkRoomName() {
    return preWorkRoomName;
  }

  public void setPreWorkRoomName(String preWorkRoomName) {
    this.preWorkRoomName = preWorkRoomName;
  }


  public Long getPreWorkerNumber() {
    return preWorkerNumber;
  }

  public void setPreWorkerNumber(Long preWorkerNumber) {
    this.preWorkerNumber = preWorkerNumber;
  }


  public String getTeamNo() {
    return teamNo;
  }

  public void setTeamNo(String teamNo) {
    this.teamNo = teamNo;
  }


  public String getTeamName() {
    return teamName;
  }

  public void setTeamName(String teamName) {
    this.teamName = teamName;
  }


  public Date getDispatchTime() {
    return dispatchTime;
  }

  public void setDispatchTime(Date dispatchTime) {
    this.dispatchTime = dispatchTime;
  }


  public String getDispatchType() {
    return dispatchType;
  }

  public void setDispatchType(String dispatchType) {
    this.dispatchType = dispatchType;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
