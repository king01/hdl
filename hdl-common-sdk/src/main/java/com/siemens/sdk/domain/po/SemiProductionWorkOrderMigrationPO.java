package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_semi_production_work_order_migration" )
public class SemiProductionWorkOrderMigrationPO  implements Serializable {

	private static final long serialVersionUID =  3359174381038912166L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 半工单迁移编号
	 */
   	@Column(name = "semi_work_order_migration_no" )
	private String semiWorkOrderMigrationNo;

	/**
	 * 半成品工单编号
	 */
   	@Column(name = "semi_work_order_no" )
	private String semiWorkOrderNo;

	/**
	 * 波次
	 */
   	@Column(name = "delivery_wave" )
	private Long deliveryWave;

	/**
	 * 物料编号
	 */
   	@Column(name = "material_no" )
	private String materialNo;

	/**
	 * 物料名称
	 */
   	@Column(name = "material_name" )
	private String materialName;

	/**
	 * 生产日期
	 */
   	@Column(name = "production_date" )
	private Date productionDate;

	/**
	 * 原车间编号
	 */
   	@Column(name = "src_work_room_no" )
	private String srcWorkRoomNo;

	/**
	 * 原车间名称
	 */
   	@Column(name = "src_work_room_name" )
	private String srcWorkRoomName;

	/**
	 * 目标车间编号
	 */
   	@Column(name = "dest_work_room_no" )
	private String destWorkRoomNo;

	/**
	 * 目标车间名称
	 */
   	@Column(name = "dest_work_room_name" )
	private String destWorkRoomName;

	/**
	 * 迁移时间
	 */
   	@Column(name = "migration_time" )
	private Date migrationTime;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getSemiWorkOrderMigrationNo() {
    return semiWorkOrderMigrationNo;
  }

  public void setSemiWorkOrderMigrationNo(String semiWorkOrderMigrationNo) {
    this.semiWorkOrderMigrationNo = semiWorkOrderMigrationNo;
  }


  public String getSemiWorkOrderNo() {
    return semiWorkOrderNo;
  }

  public void setSemiWorkOrderNo(String semiWorkOrderNo) {
    this.semiWorkOrderNo = semiWorkOrderNo;
  }


  public Long getDeliveryWave() {
    return deliveryWave;
  }

  public void setDeliveryWave(Long deliveryWave) {
    this.deliveryWave = deliveryWave;
  }


  public String getMaterialNo() {
    return materialNo;
  }

  public void setMaterialNo(String materialNo) {
    this.materialNo = materialNo;
  }


  public String getMaterialName() {
    return materialName;
  }

  public void setMaterialName(String materialName) {
    this.materialName = materialName;
  }


  public Date getProductionDate() {
    return productionDate;
  }

  public void setProductionDate(Date productionDate) {
    this.productionDate = productionDate;
  }


  public String getSrcWorkRoomNo() {
    return srcWorkRoomNo;
  }

  public void setSrcWorkRoomNo(String srcWorkRoomNo) {
    this.srcWorkRoomNo = srcWorkRoomNo;
  }


  public String getSrcWorkRoomName() {
    return srcWorkRoomName;
  }

  public void setSrcWorkRoomName(String srcWorkRoomName) {
    this.srcWorkRoomName = srcWorkRoomName;
  }


  public String getDestWorkRoomNo() {
    return destWorkRoomNo;
  }

  public void setDestWorkRoomNo(String destWorkRoomNo) {
    this.destWorkRoomNo = destWorkRoomNo;
  }


  public String getDestWorkRoomName() {
    return destWorkRoomName;
  }

  public void setDestWorkRoomName(String destWorkRoomName) {
    this.destWorkRoomName = destWorkRoomName;
  }


  public Date getMigrationTime() {
    return migrationTime;
  }

  public void setMigrationTime(Date migrationTime) {
    this.migrationTime = migrationTime;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
