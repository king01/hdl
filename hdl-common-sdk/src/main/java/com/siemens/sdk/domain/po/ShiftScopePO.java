package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_shift_scope" )
public class ShiftScopePO  implements Serializable {

	private static final long serialVersionUID =  130770957818905989L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 班次范围编号
	 */
   	@Column(name = "shift_scope_no" )
	private String shiftScopeNo;

	/**
	 * 班次编号
	 */
   	@Column(name = "shift_no" )
	private String shiftNo;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 车间所属类型：20031001=预生产车间，20031002=分装车间
	 */
   	@Column(name = "belong_type" )
	private String belongType;

	/**
	 * 所属车间/产线编号
	 */
   	@Column(name = "belong_no" )
	private String belongNo;

	/**
	 * 所属车间/产线名称
	 */
   	@Column(name = "belong_name" )
	private String belongName;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getShiftScopeNo() {
    return shiftScopeNo;
  }

  public void setShiftScopeNo(String shiftScopeNo) {
    this.shiftScopeNo = shiftScopeNo;
  }


  public String getShiftNo() {
    return shiftNo;
  }

  public void setShiftNo(String shiftNo) {
    this.shiftNo = shiftNo;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getBelongType() {
    return belongType;
  }

  public void setBelongType(String belongType) {
    this.belongType = belongType;
  }


  public String getBelongNo() {
    return belongNo;
  }

  public void setBelongNo(String belongNo) {
    this.belongNo = belongNo;
  }


  public String getBelongName() {
    return belongName;
  }

  public void setBelongName(String belongName) {
    this.belongName = belongName;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
