package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_shop_order_item" )
public class ShopOrderItemPO  implements Serializable {

	private static final long serialVersionUID =  620559928277359727L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 明细编号
	 */
   	@Column(name = "order_item_no" )
	private String orderItemNo;

	/**
	 * 接收编号
	 */
   	@Column(name = "receive_order_no" )
	private String receiveOrderNo;

	/**
	 * 下单时间
	 */
   	@Column(name = "order_create_time" )
	private Date orderCreateTime;

	/**
	 * 订单主体编码
	 */
   	@Column(name = "delivery_id" )
	private String deliveryId;

	/**
	 * 订单主体名称
	 */
   	@Column(name = "delivery_name" )
	private String deliveryName;

	/**
	 * 商品编号
	 */
   	@Column(name = "sku_id" )
	private String skuId;

	/**
	 * 商品名称
	 */
   	@Column(name = "sku_name" )
	private String skuName;

	/**
	 * 商品别名
	 */
   	@Column(name = "sku_alias_name" )
	private String skuAliasName;

	/**
	 * 商品单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 商品单位
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 商品规格
	 */
   	@Column(name = "sku_spec" )
	private String skuSpec;

	/**
	 * 采购数量
	 */
   	@Column(name = "purchase_quantity" )
	private BigDecimal purchaseQuantity;

	/**
	 * 换算后的单位编码
	 */
   	@Column(name = "after_conversion_unit_code" )
	private String afterConversionUnitCode;

	/**
	 * 换算后的单位名称
	 */
   	@Column(name = "after_conversion_unit_name" )
	private String afterConversionUnitName;

	/**
	 * 换算后的数量
	 */
   	@Column(name = "after_conversion_quantity" )
	private BigDecimal afterConversionQuantity;

	/**
	 * 换算后的收货数量
	 */
   	@Column(name = "after_conversion_receiving_quantity" )
	private BigDecimal afterConversionReceivingQuantity;

	/**
	 * 换算比
	 */
   	@Column(name = "conversion_ratio" )
	private BigDecimal conversionRatio;

	/**
	 * 允差值上浮动
	 */
   	@Column(name = "floating_tolerance" )
	private BigDecimal floatingTolerance;

	/**
	 * 允差值下浮动
	 */
   	@Column(name = "lower_tolerance" )
	private BigDecimal lowerTolerance;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getOrderItemNo() {
    return orderItemNo;
  }

  public void setOrderItemNo(String orderItemNo) {
    this.orderItemNo = orderItemNo;
  }


  public String getReceiveOrderNo() {
    return receiveOrderNo;
  }

  public void setReceiveOrderNo(String receiveOrderNo) {
    this.receiveOrderNo = receiveOrderNo;
  }


  public Date getOrderCreateTime() {
    return orderCreateTime;
  }

  public void setOrderCreateTime(Date orderCreateTime) {
    this.orderCreateTime = orderCreateTime;
  }


  public String getDeliveryId() {
    return deliveryId;
  }

  public void setDeliveryId(String deliveryId) {
    this.deliveryId = deliveryId;
  }


  public String getDeliveryName() {
    return deliveryName;
  }

  public void setDeliveryName(String deliveryName) {
    this.deliveryName = deliveryName;
  }


  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }


  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }


  public String getSkuAliasName() {
    return skuAliasName;
  }

  public void setSkuAliasName(String skuAliasName) {
    this.skuAliasName = skuAliasName;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getSkuSpec() {
    return skuSpec;
  }

  public void setSkuSpec(String skuSpec) {
    this.skuSpec = skuSpec;
  }


  public BigDecimal getPurchaseQuantity() {
    return purchaseQuantity;
  }

  public void setPurchaseQuantity(BigDecimal purchaseQuantity) {
    this.purchaseQuantity = purchaseQuantity;
  }


  public String getAfterConversionUnitCode() {
    return afterConversionUnitCode;
  }

  public void setAfterConversionUnitCode(String afterConversionUnitCode) {
    this.afterConversionUnitCode = afterConversionUnitCode;
  }


  public String getAfterConversionUnitName() {
    return afterConversionUnitName;
  }

  public void setAfterConversionUnitName(String afterConversionUnitName) {
    this.afterConversionUnitName = afterConversionUnitName;
  }


  public BigDecimal getAfterConversionQuantity() {
    return afterConversionQuantity;
  }

  public void setAfterConversionQuantity(BigDecimal afterConversionQuantity) {
    this.afterConversionQuantity = afterConversionQuantity;
  }


  public BigDecimal getAfterConversionReceivingQuantity() {
    return afterConversionReceivingQuantity;
  }

  public void setAfterConversionReceivingQuantity(BigDecimal afterConversionReceivingQuantity) {
    this.afterConversionReceivingQuantity = afterConversionReceivingQuantity;
  }


  public BigDecimal getConversionRatio() {
    return conversionRatio;
  }

  public void setConversionRatio(BigDecimal conversionRatio) {
    this.conversionRatio = conversionRatio;
  }


  public BigDecimal getFloatingTolerance() {
    return floatingTolerance;
  }

  public void setFloatingTolerance(BigDecimal floatingTolerance) {
    this.floatingTolerance = floatingTolerance;
  }


  public BigDecimal getLowerTolerance() {
    return lowerTolerance;
  }

  public void setLowerTolerance(BigDecimal lowerTolerance) {
    this.lowerTolerance = lowerTolerance;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
