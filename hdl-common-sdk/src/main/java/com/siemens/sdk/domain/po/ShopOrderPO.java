package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_shop_order" )
public class ShopOrderPO  implements Serializable {

	private static final long serialVersionUID =  1442286193174452382L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 接收编号
	 */
   	@Column(name = "receive_order_no" )
	private String receiveOrderNo;

	/**
	 * 执行单号
	 */
   	@Column(name = "execution_id" )
	private String executionId;

	/**
	 * 城市区域编码
	 */
   	@Column(name = "region_code" )
	private String regionCode;

	/**
	 * 城市区域名称
	 */
   	@Column(name = "region_name" )
	private String regionName;

	/**
	 * 配送类型：20151001=直配，20151002=直发
	 */
   	@Column(name = "business_type" )
	private String businessType;

	/**
	 * 订单状态
	 */
   	@Column(name = "status" )
	private String status;

	/**
	 * 交货日期
	 */
   	@Column(name = "delivery_date" )
	private Date deliveryDate;

	/**
	 * 收货方编码
	 */
   	@Column(name = "receiver_id" )
	private String receiverId;

	/**
	 * 收货联系人
	 */
   	@Column(name = "delivery_contact" )
	private String deliveryContact;

	/**
	 * 收货联系人电话
	 */
   	@Column(name = "delivery_contact_tel" )
	private String deliveryContactTel;

	/**
	 * 收货地址
	 */
   	@Column(name = "delivery_address" )
	private String deliveryAddress;

	/**
	 * 订单类型：
	 */
   	@Column(name = "order_type" )
	private String orderType;

	/**
	 * 合作业务主体编码
	 */
   	@Column(name = "cooperate_business_id" )
	private String cooperateBusinessId;

	/**
	 * 合作业务主体名称
	 */
   	@Column(name = "cooperate_business_name" )
	private String cooperateBusinessName;

	/**
	 * 是否加急：0=否，1=是
	 */
   	@Column(name = "urgent_flag" )
	private Long urgentFlag;

	/**
	 * 接收时间
	 */
   	@Column(name = "receive_time" )
	private Date receiveTime;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getReceiveOrderNo() {
    return receiveOrderNo;
  }

  public void setReceiveOrderNo(String receiveOrderNo) {
    this.receiveOrderNo = receiveOrderNo;
  }


  public String getExecutionId() {
    return executionId;
  }

  public void setExecutionId(String executionId) {
    this.executionId = executionId;
  }


  public String getRegionCode() {
    return regionCode;
  }

  public void setRegionCode(String regionCode) {
    this.regionCode = regionCode;
  }


  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }


  public String getBusinessType() {
    return businessType;
  }

  public void setBusinessType(String businessType) {
    this.businessType = businessType;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public Date getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(Date deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public String getReceiverId() {
    return receiverId;
  }

  public void setReceiverId(String receiverId) {
    this.receiverId = receiverId;
  }


  public String getDeliveryContact() {
    return deliveryContact;
  }

  public void setDeliveryContact(String deliveryContact) {
    this.deliveryContact = deliveryContact;
  }


  public String getDeliveryContactTel() {
    return deliveryContactTel;
  }

  public void setDeliveryContactTel(String deliveryContactTel) {
    this.deliveryContactTel = deliveryContactTel;
  }


  public String getDeliveryAddress() {
    return deliveryAddress;
  }

  public void setDeliveryAddress(String deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }


  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }


  public String getCooperateBusinessId() {
    return cooperateBusinessId;
  }

  public void setCooperateBusinessId(String cooperateBusinessId) {
    this.cooperateBusinessId = cooperateBusinessId;
  }


  public String getCooperateBusinessName() {
    return cooperateBusinessName;
  }

  public void setCooperateBusinessName(String cooperateBusinessName) {
    this.cooperateBusinessName = cooperateBusinessName;
  }


  public Long getUrgentFlag() {
    return urgentFlag;
  }

  public void setUrgentFlag(Long urgentFlag) {
    this.urgentFlag = urgentFlag;
  }


  public Date getReceiveTime() {
    return receiveTime;
  }

  public void setReceiveTime(Date receiveTime) {
    this.receiveTime = receiveTime;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
