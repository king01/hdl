package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_shop_wave" )
public class ShopWavePO  implements Serializable {

	private static final long serialVersionUID =  4168288111087233108L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 门店编号
	 */
   	@Column(name = "shop_no" )
	private String shopNo;

	/**
	 * 门店名称
	 */
   	@Column(name = "shop_name" )
	private String shopName;

	/**
	 * 门店地址
	 */
   	@Column(name = "shop_address" )
	private String shopAddress;

	/**
	 * 联系人
	 */
   	@Column(name = "shop_contact" )
	private String shopContact;

	/**
	 * 联系电话
	 */
   	@Column(name = "shop_contact_tel" )
	private String shopContactTel;

	/**
	 * 波次编号
	 */
   	@Column(name = "wave_no" )
	private String waveNo;

	/**
	 * 波次数值
	 */
   	@Column(name = "wave_value" )
	private String waveValue;

	/**
	 * 是否启用：10011001=启用，10011002=停用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getShopNo() {
    return shopNo;
  }

  public void setShopNo(String shopNo) {
    this.shopNo = shopNo;
  }


  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }


  public String getShopAddress() {
    return shopAddress;
  }

  public void setShopAddress(String shopAddress) {
    this.shopAddress = shopAddress;
  }


  public String getShopContact() {
    return shopContact;
  }

  public void setShopContact(String shopContact) {
    this.shopContact = shopContact;
  }


  public String getShopContactTel() {
    return shopContactTel;
  }

  public void setShopContactTel(String shopContactTel) {
    this.shopContactTel = shopContactTel;
  }


  public String getWaveNo() {
    return waveNo;
  }

  public void setWaveNo(String waveNo) {
    this.waveNo = waveNo;
  }


  public String getWaveValue() {
    return waveValue;
  }

  public void setWaveValue(String waveValue) {
    this.waveValue = waveValue;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
