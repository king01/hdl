package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:53 
 */
@Entity
@Table ( name ="t_step" )
public class StepPO  implements Serializable {

	private static final long serialVersionUID =  5347171520121107245L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 工步编号
	 */
   	@Column(name = "step_no" )
	private String stepNo;

	/**
	 * 工步名称
	 */
   	@Column(name = "step_name" )
	private String stepName;

	/**
	 * 工步序号
	 */
   	@Column(name = "step_order" )
	private Long stepOrder;

	/**
	 * 工步内容
	 */
   	@Column(name = "step_context" )
	private String stepContext;

	/**
	 * 工序编号
	 */
   	@Column(name = "process_no" )
	private String processNo;

	/**
	 * 检测值标识：0=否，1=是
	 */
   	@Column(name = "detection_value" )
	private Long detectionValue;

	/**
	 * 检验标识：0=否，1=是
	 */
   	@Column(name = "inspection_identification" )
	private Long inspectionIdentification;

	/**
	 * 计件配额
	 */
   	@Column(name = "piecework_quota" )
	private BigDecimal pieceworkQuota;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getStepNo() {
    return stepNo;
  }

  public void setStepNo(String stepNo) {
    this.stepNo = stepNo;
  }


  public String getStepName() {
    return stepName;
  }

  public void setStepName(String stepName) {
    this.stepName = stepName;
  }


  public Long getStepOrder() {
    return stepOrder;
  }

  public void setStepOrder(Long stepOrder) {
    this.stepOrder = stepOrder;
  }


  public String getStepContext() {
    return stepContext;
  }

  public void setStepContext(String stepContext) {
    this.stepContext = stepContext;
  }


  public String getProcessNo() {
    return processNo;
  }

  public void setProcessNo(String processNo) {
    this.processNo = processNo;
  }


  public Long getDetectionValue() {
    return detectionValue;
  }

  public void setDetectionValue(Long detectionValue) {
    this.detectionValue = detectionValue;
  }


  public Long getInspectionIdentification() {
    return inspectionIdentification;
  }

  public void setInspectionIdentification(Long inspectionIdentification) {
    this.inspectionIdentification = inspectionIdentification;
  }


  public BigDecimal getPieceworkQuota() {
    return pieceworkQuota;
  }

  public void setPieceworkQuota(BigDecimal pieceworkQuota) {
    this.pieceworkQuota = pieceworkQuota;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
