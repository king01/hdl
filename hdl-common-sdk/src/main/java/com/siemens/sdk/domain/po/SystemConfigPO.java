package com.siemens.sdk.domain.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @ClassName: SystemConfigPO
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 15:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_system_config")
public class SystemConfigPO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 自增长策略
    private Long id;

    @Column(name = "config_no")
    private String configNo;

    @Column(name = "config_name")
    private String configName;

    @Column(name = "config_label")
    private String configLabel;

    @Column(name = "config_val")
    private String configVal;

    @Column(name = "status")
    private String status;

    @Column(name = "config_source")
    private String configSource;

    @Column(name = "remark")
    private String remark;

    @Override
    public String toString() {
        return "SystemConfigPO{" +
                "id=" + id +
                ", configNo='" + configNo + '\'' +
                ", configName='" + configName + '\'' +
                ", configLabel='" + configLabel + '\'' +
                ", configVal='" + configVal + '\'' +
                ", status='" + status + '\'' +
                ", configSource='" + configSource + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
