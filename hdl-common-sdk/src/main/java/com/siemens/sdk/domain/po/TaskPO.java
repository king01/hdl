package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_task" )
public class TaskPO  implements Serializable {

	private static final long serialVersionUID =  6067302608491208747L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 任务编号
	 */
   	@Column(name = "task_no" )
	private String taskNo;

	/**
	 * 任务名称
	 */
   	@Column(name = "task_name" )
	private String taskName;

	/**
	 * 任务策略
	 */
   	@Column(name = "task_cron" )
	private String taskCron;

	/**
	 * 状态：10011001=启用，10011002=停用
	 */
   	@Column(name = "status" )
	private String status;

	/**
	 * 配置来源：20181001=系统内置，20181002=自定义
	 */
   	@Column(name = "config_source" )
	private String configSource;

	/**
	 * 描述
	 */
   	@Column(name = "remark" )
	private String remark;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getTaskNo() {
    return taskNo;
  }

  public void setTaskNo(String taskNo) {
    this.taskNo = taskNo;
  }


  public String getTaskName() {
    return taskName;
  }

  public void setTaskName(String taskName) {
    this.taskName = taskName;
  }


  public String getTaskCron() {
    return taskCron;
  }

  public void setTaskCron(String taskCron) {
    this.taskCron = taskCron;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getConfigSource() {
    return configSource;
  }

  public void setConfigSource(String configSource) {
    this.configSource = configSource;
  }


  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
