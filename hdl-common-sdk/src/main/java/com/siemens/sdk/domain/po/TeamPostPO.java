package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_team_post" )
public class TeamPostPO  implements Serializable {

	private static final long serialVersionUID =  3084188987799730805L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 班组岗位编号
	 */
   	@Column(name = "team_post_no" )
	private String teamPostNo;

	/**
	 * 班组编号
	 */
   	@Column(name = "team_no" )
	private String teamNo;

	/**
	 * 班组名称
	 */
   	@Column(name = "team_name" )
	private String teamName;

	/**
	 * 岗位编号
	 */
   	@Column(name = "post_code" )
	private String postCode;

	/**
	 * 岗位名称
	 */
   	@Column(name = "post_name" )
	private String postName;

	/**
	 * 排列顺序
	 */
   	@Column(name = "order_seq" )
	private Long orderSeq;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getTeamPostNo() {
    return teamPostNo;
  }

  public void setTeamPostNo(String teamPostNo) {
    this.teamPostNo = teamPostNo;
  }


  public String getTeamNo() {
    return teamNo;
  }

  public void setTeamNo(String teamNo) {
    this.teamNo = teamNo;
  }


  public String getTeamName() {
    return teamName;
  }

  public void setTeamName(String teamName) {
    this.teamName = teamName;
  }


  public String getPostCode() {
    return postCode;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }


  public String getPostName() {
    return postName;
  }

  public void setPostName(String postName) {
    this.postName = postName;
  }


  public Long getOrderSeq() {
    return orderSeq;
  }

  public void setOrderSeq(Long orderSeq) {
    this.orderSeq = orderSeq;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
