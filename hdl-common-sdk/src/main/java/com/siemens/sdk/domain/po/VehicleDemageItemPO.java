package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_vehicle_demage_item" )
public class VehicleDemageItemPO  implements Serializable {

	private static final long serialVersionUID =  4637159707755729092L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 餐具报损明细编号
	 */
   	@Column(name = "vehicle_demage_no" )
	private String vehicleDemageNo;

	/**
	 * 报损记录编号
	 */
   	@Column(name = "damage_record_no" )
	private String damageRecordNo;

	/**
	 * 报损单号
	 */
   	@Column(name = "damage_bill_no" )
	private String damageBillNo;

	/**
	 * 载具编号
	 */
   	@Column(name = "vehicle_no" )
	private String vehicleNo;

	/**
	 * 载具名称
	 */
   	@Column(name = "vehicle_name" )
	private String vehicleName;

	/**
	 * 载具规格
	 */
   	@Column(name = "vehicle_spec" )
	private String vehicleSpec;

	/**
	 * 报损数量
	 */
   	@Column(name = "demage_quantity" )
	private Long demageQuantity;

	/**
	 * 库存单位编号
	 */
   	@Column(name = "unit_no" )
	private String unitNo;

	/**
	 * 库存单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getVehicleDemageNo() {
    return vehicleDemageNo;
  }

  public void setVehicleDemageNo(String vehicleDemageNo) {
    this.vehicleDemageNo = vehicleDemageNo;
  }


  public String getDamageRecordNo() {
    return damageRecordNo;
  }

  public void setDamageRecordNo(String damageRecordNo) {
    this.damageRecordNo = damageRecordNo;
  }


  public String getDamageBillNo() {
    return damageBillNo;
  }

  public void setDamageBillNo(String damageBillNo) {
    this.damageBillNo = damageBillNo;
  }


  public String getVehicleNo() {
    return vehicleNo;
  }

  public void setVehicleNo(String vehicleNo) {
    this.vehicleNo = vehicleNo;
  }


  public String getVehicleName() {
    return vehicleName;
  }

  public void setVehicleName(String vehicleName) {
    this.vehicleName = vehicleName;
  }


  public String getVehicleSpec() {
    return vehicleSpec;
  }

  public void setVehicleSpec(String vehicleSpec) {
    this.vehicleSpec = vehicleSpec;
  }


  public Long getDemageQuantity() {
    return demageQuantity;
  }

  public void setDemageQuantity(Long demageQuantity) {
    this.demageQuantity = demageQuantity;
  }


  public String getUnitNo() {
    return unitNo;
  }

  public void setUnitNo(String unitNo) {
    this.unitNo = unitNo;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
