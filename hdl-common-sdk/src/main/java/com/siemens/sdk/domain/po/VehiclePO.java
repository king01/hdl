package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_vehicle" )
public class VehiclePO  implements Serializable {

	private static final long serialVersionUID =  2954313430249156489L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 载具编号
	 */
   	@Column(name = "vehicle_no" )
	private String vehicleNo;

	/**
	 * 载具名称
	 */
   	@Column(name = "vehicle_name" )
	private String vehicleName;

	/**
	 * 载具型号
	 */
   	@Column(name = "vehicle_mode" )
	private String vehicleMode;

	/**
	 * 载具规格
	 */
   	@Column(name = "vehicle_spec" )
	private String vehicleSpec;

	/**
	 * 用途
	 */
   	@Column(name = "use" )
	private String use;

	/**
	 * 载具类型：20201001=餐盒，20201002=餐盖，20201003=周转箱
	 */
   	@Column(name = "vehicle_type" )
	private String vehicleType;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getVehicleNo() {
    return vehicleNo;
  }

  public void setVehicleNo(String vehicleNo) {
    this.vehicleNo = vehicleNo;
  }


  public String getVehicleName() {
    return vehicleName;
  }

  public void setVehicleName(String vehicleName) {
    this.vehicleName = vehicleName;
  }


  public String getVehicleMode() {
    return vehicleMode;
  }

  public void setVehicleMode(String vehicleMode) {
    this.vehicleMode = vehicleMode;
  }


  public String getVehicleSpec() {
    return vehicleSpec;
  }

  public void setVehicleSpec(String vehicleSpec) {
    this.vehicleSpec = vehicleSpec;
  }


  public String getUse() {
    return use;
  }

  public void setUse(String use) {
    this.use = use;
  }


  public String getVehicleType() {
    return vehicleType;
  }

  public void setVehicleType(String vehicleType) {
    this.vehicleType = vehicleType;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
