package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_vehicle_parameter" )
public class VehicleParameterPO  implements Serializable {

	private static final long serialVersionUID =  5716675825186589422L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 载具参数编号
	 */
   	@Column(name = "vehicle_parameter_no" )
	private String vehicleParameterNo;

	/**
	 * 周转箱编号
	 */
   	@Column(name = "turnover_no" )
	private String turnoverNo;

	/**
	 * 周转箱名称
	 */
   	@Column(name = "turnover_name" )
	private String turnoverName;

	/**
	 * 周转箱型号
	 */
   	@Column(name = "turnover_mode" )
	private String turnoverMode;

	/**
	 * 满箱数量
	 */
   	@Column(name = "full_quantity" )
	private Long fullQuantity;

	/**
	 * 半箱数量
	 */
   	@Column(name = "half_quantity" )
	private Long halfQuantity;

	/**
	 * 装载单位编号
	 */
   	@Column(name = "load_unit_code" )
	private String loadUnitCode;

	/**
	 * 装载单位名称
	 */
   	@Column(name = "load_unit_name" )
	private String loadUnitName;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getVehicleParameterNo() {
    return vehicleParameterNo;
  }

  public void setVehicleParameterNo(String vehicleParameterNo) {
    this.vehicleParameterNo = vehicleParameterNo;
  }


  public String getTurnoverNo() {
    return turnoverNo;
  }

  public void setTurnoverNo(String turnoverNo) {
    this.turnoverNo = turnoverNo;
  }


  public String getTurnoverName() {
    return turnoverName;
  }

  public void setTurnoverName(String turnoverName) {
    this.turnoverName = turnoverName;
  }


  public String getTurnoverMode() {
    return turnoverMode;
  }

  public void setTurnoverMode(String turnoverMode) {
    this.turnoverMode = turnoverMode;
  }


  public Long getFullQuantity() {
    return fullQuantity;
  }

  public void setFullQuantity(Long fullQuantity) {
    this.fullQuantity = fullQuantity;
  }


  public Long getHalfQuantity() {
    return halfQuantity;
  }

  public void setHalfQuantity(Long halfQuantity) {
    this.halfQuantity = halfQuantity;
  }


  public String getLoadUnitCode() {
    return loadUnitCode;
  }

  public void setLoadUnitCode(String loadUnitCode) {
    this.loadUnitCode = loadUnitCode;
  }


  public String getLoadUnitName() {
    return loadUnitName;
  }

  public void setLoadUnitName(String loadUnitName) {
    this.loadUnitName = loadUnitName;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
