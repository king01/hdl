package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_work_shift" )
public class WorkShiftPO  implements Serializable {

	private static final long serialVersionUID =  5482687232387357559L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 班次编号
	 */
   	@Column(name = "shift_no" )
	private String shiftNo;

	/**
	 * 班次名称
	 */
   	@Column(name = "shift_name" )
	private String shiftName;

	/**
	 * 序号
	 */
   	@Column(name = "shift_seq" )
	private Long shiftSeq;

	/**
	 * 范围类型：20211001=预处理车间，20211002=分装车间，20211003=选择车间范围
	 */
   	@Column(name = "scope_type" )
	private String scopeType;

	/**
	 * 开始时间
	 */
   	@Column(name = "start_time" )
	private Date startTime;

	/**
	 * 结束时间
	 */
   	@Column(name = "end_time" )
	private Date endTime;

	/**
	 * 是否第二天：0=否，1=是
	 */
   	@Column(name = "is_second_day" )
	private Long isSecondDay;

	/**
	 * 是否启用：10011001=启用，10011002=停用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getShiftNo() {
    return shiftNo;
  }

  public void setShiftNo(String shiftNo) {
    this.shiftNo = shiftNo;
  }


  public String getShiftName() {
    return shiftName;
  }

  public void setShiftName(String shiftName) {
    this.shiftName = shiftName;
  }


  public Long getShiftSeq() {
    return shiftSeq;
  }

  public void setShiftSeq(Long shiftSeq) {
    this.shiftSeq = shiftSeq;
  }


  public String getScopeType() {
    return scopeType;
  }

  public void setScopeType(String scopeType) {
    this.scopeType = scopeType;
  }


  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }


  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }


  public Long getIsSecondDay() {
    return isSecondDay;
  }

  public void setIsSecondDay(Long isSecondDay) {
    this.isSecondDay = isSecondDay;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
