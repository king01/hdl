package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_work_shop" )
public class WorkShopPO  implements Serializable {

	private static final long serialVersionUID =  3830588435532822977L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 车间编号
	 */
   	@Column(name = "work_shop_no" )
	private String workShopNo;

	/**
	 * 车间名称
	 */
   	@Column(name = "work_shop_name" )
	private String workShopName;

	/**
	 * 车间类型：20221001=拆外包，20221002=拆内包，20221003=素菜加工区，20221004=荤菜加工区，20221005=水产品加工区，20221006=根茎漂汤区
	 */
   	@Column(name = "work_shop_type" )
	private String workShopType;

	/**
	 * 车间位置
	 */
   	@Column(name = "position" )
	private String position;

	/**
	 * 描述
	 */
   	@Column(name = "desc" )
	private String desc;

	/**
	 * 所属工厂编号
	 */
   	@Column(name = "factory_no" )
	private String factoryNo;

	/**
	 * 所属工厂名称
	 */
   	@Column(name = "factory_name" )
	private String factoryName;

	/**
	 * 是否启用：10011001=启用，10011002=停用
	 */
   	@Column(name = "is_enable" )
	private String isEnable;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getWorkShopNo() {
    return workShopNo;
  }

  public void setWorkShopNo(String workShopNo) {
    this.workShopNo = workShopNo;
  }


  public String getWorkShopName() {
    return workShopName;
  }

  public void setWorkShopName(String workShopName) {
    this.workShopName = workShopName;
  }


  public String getWorkShopType() {
    return workShopType;
  }

  public void setWorkShopType(String workShopType) {
    this.workShopType = workShopType;
  }


  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }


  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }


  public String getFactoryNo() {
    return factoryNo;
  }

  public void setFactoryNo(String factoryNo) {
    this.factoryNo = factoryNo;
  }


  public String getFactoryName() {
    return factoryName;
  }

  public void setFactoryName(String factoryName) {
    this.factoryName = factoryName;
  }


  public String getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(String isEnable) {
    this.isEnable = isEnable;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
