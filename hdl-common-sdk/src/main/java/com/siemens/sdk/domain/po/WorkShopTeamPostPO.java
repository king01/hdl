package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_work_shop_team_post" )
public class WorkShopTeamPostPO  implements Serializable {

	private static final long serialVersionUID =  7635014669860877374L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 车间班组岗位编号
	 */
   	@Column(name = "work_shop_team_post_no" )
	private String workShopTeamPostNo;

	/**
	 * 车间班组编号
	 */
   	@Column(name = "work_shop_team_no" )
	private String workShopTeamNo;

	/**
	 * 车间班组名称
	 */
   	@Column(name = "work_shop_team_name" )
	private String workShopTeamName;

	/**
	 * 岗位编号
	 */
   	@Column(name = "post_no" )
	private String postNo;

	/**
	 * 岗位名称
	 */
   	@Column(name = "post_name" )
	private String postName;

	/**
	 * 排列顺序
	 */
   	@Column(name = "order_seq" )
	private Long orderSeq;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getWorkShopTeamPostNo() {
    return workShopTeamPostNo;
  }

  public void setWorkShopTeamPostNo(String workShopTeamPostNo) {
    this.workShopTeamPostNo = workShopTeamPostNo;
  }


  public String getWorkShopTeamNo() {
    return workShopTeamNo;
  }

  public void setWorkShopTeamNo(String workShopTeamNo) {
    this.workShopTeamNo = workShopTeamNo;
  }


  public String getWorkShopTeamName() {
    return workShopTeamName;
  }

  public void setWorkShopTeamName(String workShopTeamName) {
    this.workShopTeamName = workShopTeamName;
  }


  public String getPostNo() {
    return postNo;
  }

  public void setPostNo(String postNo) {
    this.postNo = postNo;
  }


  public String getPostName() {
    return postName;
  }

  public void setPostName(String postName) {
    this.postName = postName;
  }


  public Long getOrderSeq() {
    return orderSeq;
  }

  public void setOrderSeq(Long orderSeq) {
    this.orderSeq = orderSeq;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
