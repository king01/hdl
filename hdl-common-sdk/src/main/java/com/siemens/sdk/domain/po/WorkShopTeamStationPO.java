package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_work_shop_team_station" )
public class WorkShopTeamStationPO  implements Serializable {

	private static final long serialVersionUID =  3427975954899878263L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 车间班组工段工位编号
	 */
   	@Column(name = "work_shop_team_station_no" )
	private String workShopTeamStationNo;

	/**
	 * 车间班组工段编号
	 */
   	@Column(name = "work_shop_team_section_no" )
	private String workShopTeamSectionNo;

	/**
	 * 工位编号
	 */
   	@Column(name = "work_station_no" )
	private String workStationNo;

	/**
	 * 工位名称
	 */
   	@Column(name = "work_station_name" )
	private String workStationName;

	/**
	 * 排列顺序
	 */
   	@Column(name = "order_seq" )
	private Long orderSeq;

	/**
	 * 是否删除:0=未删除；1=删除
	 */
   	@Column(name = "is_del" )
	private Long isDel;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getWorkShopTeamStationNo() {
    return workShopTeamStationNo;
  }

  public void setWorkShopTeamStationNo(String workShopTeamStationNo) {
    this.workShopTeamStationNo = workShopTeamStationNo;
  }


  public String getWorkShopTeamSectionNo() {
    return workShopTeamSectionNo;
  }

  public void setWorkShopTeamSectionNo(String workShopTeamSectionNo) {
    this.workShopTeamSectionNo = workShopTeamSectionNo;
  }


  public String getWorkStationNo() {
    return workStationNo;
  }

  public void setWorkStationNo(String workStationNo) {
    this.workStationNo = workStationNo;
  }


  public String getWorkStationName() {
    return workStationName;
  }

  public void setWorkStationName(String workStationName) {
    this.workStationName = workStationName;
  }


  public Long getOrderSeq() {
    return orderSeq;
  }

  public void setOrderSeq(Long orderSeq) {
    this.orderSeq = orderSeq;
  }


  public Long getIsDel() {
    return isDel;
  }

  public void setIsDel(Long isDel) {
    this.isDel = isDel;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
