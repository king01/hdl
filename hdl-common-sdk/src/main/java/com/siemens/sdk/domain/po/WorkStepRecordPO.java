package com.siemens.sdk.domain.po;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
/** 
 * @Author swang 
 * @Date 2021-07-23 10:59:37 
 */
@Entity
@Table ( name ="t_work_step_record" )
public class WorkStepRecordPO  implements Serializable {

	private static final long serialVersionUID =  1419369558048015653L;

	/**
	 * 主键，20位
	 */
	@Id
   	@Column(name = "ID" )
	private Long id;

	/**
	 * 工厂主体
	 */
   	@Column(name = "subject_name" )
	private String subjectName;

	/**
	 * 工厂实例
	 */
   	@Column(name = "instance_name" )
	private String instanceName;

	/**
	 * 工步记录编号
	 */
   	@Column(name = "step_record_no" )
	private String stepRecordNo;

	/**
	 * 派工编号
	 */
   	@Column(name = "dispatch_no" )
	private String dispatchNo;

	/**
	 * 工单类型：20231001=半成品工单，20231002=成品工单
	 */
   	@Column(name = "work_order_type" )
	private String workOrderType;

	/**
	 * 工单编号
	 */
   	@Column(name = "work_order_no" )
	private String workOrderNo;

	/**
	 * 生产执行编号
	 */
   	@Column(name = "production_execute_no" )
	private String productionExecuteNo;

	/**
	 * 工步编号
	 */
   	@Column(name = "work_step_no" )
	private String workStepNo;

	/**
	 * 工步名称
	 */
   	@Column(name = "work_step_name" )
	private String workStepName;

	/**
	 * 记录值
	 */
   	@Column(name = "record_value" )
	private String recordValue;

	/**
	 * 单位编码
	 */
   	@Column(name = "unit_code" )
	private String unitCode;

	/**
	 * 单位名称
	 */
   	@Column(name = "unit_name" )
	private String unitName;

	/**
	 * 记录人
	 */
   	@Column(name = "record_person" )
	private String recordPerson;

	/**
	 * 记录时间
	 */
   	@Column(name = "record_time" )
	private Date recordTime;

	/**
	 * 创建者
	 */
   	@Column(name = "creator" )
	private Long creator;

	/**
	 * 创建时间
	 */
   	@Column(name = "created_time" )
	private Date createdTime;

	/**
	 * 更新者
	 */
   	@Column(name = "updated" )
	private Long updated;

	/**
	 * 更新时间
	 */
   	@Column(name = "update_time" )
	private Date updateTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }


  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }


  public String getStepRecordNo() {
    return stepRecordNo;
  }

  public void setStepRecordNo(String stepRecordNo) {
    this.stepRecordNo = stepRecordNo;
  }


  public String getDispatchNo() {
    return dispatchNo;
  }

  public void setDispatchNo(String dispatchNo) {
    this.dispatchNo = dispatchNo;
  }


  public String getWorkOrderType() {
    return workOrderType;
  }

  public void setWorkOrderType(String workOrderType) {
    this.workOrderType = workOrderType;
  }


  public String getWorkOrderNo() {
    return workOrderNo;
  }

  public void setWorkOrderNo(String workOrderNo) {
    this.workOrderNo = workOrderNo;
  }


  public String getProductionExecuteNo() {
    return productionExecuteNo;
  }

  public void setProductionExecuteNo(String productionExecuteNo) {
    this.productionExecuteNo = productionExecuteNo;
  }


  public String getWorkStepNo() {
    return workStepNo;
  }

  public void setWorkStepNo(String workStepNo) {
    this.workStepNo = workStepNo;
  }


  public String getWorkStepName() {
    return workStepName;
  }

  public void setWorkStepName(String workStepName) {
    this.workStepName = workStepName;
  }


  public String getRecordValue() {
    return recordValue;
  }

  public void setRecordValue(String recordValue) {
    this.recordValue = recordValue;
  }


  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public String getRecordPerson() {
    return recordPerson;
  }

  public void setRecordPerson(String recordPerson) {
    this.recordPerson = recordPerson;
  }


  public Date getRecordTime() {
    return recordTime;
  }

  public void setRecordTime(Date recordTime) {
    this.recordTime = recordTime;
  }


  public Long getCreator() {
    return creator;
  }

  public void setCreator(Long creator) {
    this.creator = creator;
  }


  public Date getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(Date createdTime) {
    this.createdTime = createdTime;
  }


  public Long getUpdated() {
    return updated;
  }

  public void setUpdated(Long updated) {
    this.updated = updated;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
