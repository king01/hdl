package com.siemens.sdk.mapper;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @ClassName: BaseMapper
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 10:18
 */
@NoRepositoryBean
public interface BaseMapper<T, E> extends JpaRepository<T, E> {

}
