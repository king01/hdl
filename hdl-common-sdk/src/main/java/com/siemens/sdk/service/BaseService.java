package com.siemens.sdk.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

/**
 * @ClassName: BaseService
 * @Description: JPA增删改查基础接口
 * @Author: swang
 * @Date: 2021/7/21 10:16
 */
@Repository
@NoRepositoryBean
public interface BaseService<T, E> extends JpaRepository<T, E> {
}
