package com.siemens.sdk.service;

import com.siemens.sdk.domain.po.DictTypePO;
import com.siemens.sdk.domain.po.SystemConfigPO;

import java.util.List;

/**
 * @ClassName: CommonService
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 16:00
 */
public interface CommonService {

    /*
     * @Description: 获取字段数据
     * @Author: swang
     * @Date: 2021/7/22 14:47
     * @param
     * @Return: java.util.List<com.siemens.sdk.domain.po.DictTypePO>
     */
    List<DictTypePO> queryDictList();

    /*
     * @Description: 获取系统配置
     * @Author: swang
     * @Date: 2021/7/22 14:47
     * @param
     * @Return: java.util.List<com.siemens.sdk.domain.po.SystemConfigPO>
     */
    List<SystemConfigPO> querySystemConfig();
}
