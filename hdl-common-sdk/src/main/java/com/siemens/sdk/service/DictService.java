package com.siemens.sdk.service;

import com.siemens.sdk.domain.po.DictPO;
import org.springframework.stereotype.Repository;

/**
 * @ClassName: DictService
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 11:57
 */
@Repository
public interface DictService extends BaseService<DictPO, Long> {

}
