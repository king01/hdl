package com.siemens.sdk.service;

import com.siemens.sdk.domain.po.DictTypePO;
import org.springframework.stereotype.Repository;

/**
 * @ClassName: DictTypeService
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 10:20
 */
@Repository
public interface DictTypeService extends BaseService<DictTypePO, Long> {
}
