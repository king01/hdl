package com.siemens.sdk.service;

import com.siemens.sdk.domain.po.SystemConfigPO;
import org.springframework.stereotype.Repository;

/**
 * @ClassName: SystemConfigService
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 15:22
 */
@Repository
public interface SystemConfigService extends BaseService<SystemConfigPO, Long> {
}
