package com.siemens.sdk.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.siemens.sdk.config.MyRedisProperties;
import com.siemens.sdk.domain.po.DictTypePO;
import com.siemens.sdk.domain.po.SystemConfigPO;
import com.siemens.sdk.service.CommonService;
import com.siemens.sdk.service.DictTypeService;
import com.siemens.sdk.service.SystemConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: DictServiceImpl
 * @Description:
 * @Author: swang
 * @Date: 2021/7/21 10:55
 */
@Service
public class CommonServiceImpl implements CommonService {

    @Autowired
    private DictTypeService dictTypeMapper;
    @Autowired
    private SystemConfigService systemConfigService;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public List<DictTypePO> queryDictList() {
        List<DictTypePO> dictList = new ArrayList<>();
        String dictJSON = redisTemplate.opsForValue().get(MyRedisProperties.DICT_KEY);
        if(StringUtils.isBlank(dictJSON)) {
            dictList = dictTypeMapper.findAll();
            if(!CollectionUtils.isEmpty(dictList)) {
                redisTemplate.opsForValue().set(MyRedisProperties.DICT_KEY, JSONObject.toJSONString(dictList));
            }
            return null;
        } else {
            dictList = JSON.parseObject(dictJSON, List.class);
        }
        return dictList;
    }

    @Override
    public List<SystemConfigPO> querySystemConfig() {
        List<SystemConfigPO> configPOList = new ArrayList<>();
        String json = redisTemplate.opsForValue().get(MyRedisProperties.CONFIG_KEY);
        if(StringUtils.isBlank(json)) {
            configPOList = systemConfigService.findAll();
            if(!CollectionUtils.isEmpty(configPOList)) {
                redisTemplate.opsForValue().set(MyRedisProperties.CONFIG_KEY, JSONObject.toJSONString(configPOList));
            }
            return null;
        } else {
            configPOList = JSON.parseObject(json, List.class);
        }
        return configPOList;
    }
}
