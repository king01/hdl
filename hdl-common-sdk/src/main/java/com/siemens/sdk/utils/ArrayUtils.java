package com.siemens.sdk.utils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 工具类
 * @author wang. shuai
 */
public class ArrayUtils {

    /**
     * 判断数组是否为空
     */
    public static <T> boolean isEmpty(T[] array) {
        return getLength(array) == 0;
    }

    /**
     * 判断数组不为空
     */
    public static <T> boolean isNotEmpty(T[] array) {
        return getLength(array) > 0;
    }

    /**
     * 获取数组长度
     */
    public static <T> int getLength(T[] array) {
        return array == null ? 0 : Array.getLength(array);
    }

    /**
     * 转换list
     */
    public static <T> List<T> toList(T[] array) {
        return Arrays.asList(array);
    }

    /**
     * 转换Set
     */
    public static <T> Set<T> toSet(T[] array) {
        return new HashSet<>(toList(array));
    }

}
