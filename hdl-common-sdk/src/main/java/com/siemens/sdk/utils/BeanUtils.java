package com.siemens.sdk.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * 拷贝
 * @author wang. shuai
 */
@Slf4j
public class BeanUtils {

    /**
     * 转换
     */
    public static <T> T convert(String value, Class<T> tClass){
        return ConverUtils.convertValue(value, tClass);
    }

    /**
     * bean 拷贝
     */
    public static <T, E> T toT(E source, Class<T> tClass){
        try {
            // 构建bean
            T t = tClass.newInstance();
            return toT(source, t);
        }catch (Exception e){

            log.error("bean 拷贝 ", e);
            return null;
        }
    }

    /**
     * bean 拷贝
     */
    public static <T, E> T toT(E source, T target){
        try {
            // 获取bean信息
            BeanInfo beanInfo = Introspector.getBeanInfo(target.getClass());
            // 获取bean的所有属性列表
            PropertyDescriptor[] properties = beanInfo.getPropertyDescriptors();
            // 验证字段是否存在
            if (ArrayUtils.isEmpty(properties)) {
               return target;
            }
            // 遍历属性列表，查找指定的属性
            for (PropertyDescriptor propDesc : properties) {
                // 写入字段值
                Method writeMethod = propDesc.getWriteMethod();
                if (writeMethod != null) {
                    try {
                        // 读取字段值
                        PropertyDescriptor descriptor = new PropertyDescriptor(propDesc.getName(), source.getClass());
                        Method readMethod = descriptor.getReadMethod();
                        if (readMethod != null &&
                                ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }
                            Object value = readMethod.invoke(source);
                            // 写入字段值
                            writeMethod = propDesc.getWriteMethod();
                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }
                            writeMethod.invoke(target, value);
                        }
                    }catch (Exception e){

                    }
                }
            }
            return target;
        }catch (Exception e){
            log.error("bean 拷贝 ", e);
            return null;
        }
    }

    /**
     * 拷贝
     */
    public static <T, E> List<T> toList(List<E> source, Class<T> tClass){
        return toList(source, item->toT(item, tClass));
    }

    /**
     * 拷贝
     */
    public static <T, E> List<T> toList(List<E> source, Function<? super E, ? extends T> function){
        return toCollection(source,
                function,
                Collectors.toList());
    }

    /**
     * 拷贝
     */
    public static <C extends Collection<T>, T, E> C toCollection(Collection<E> source, Class<T> tClass, Collector<? super T, ?, C> collector){
        return toCollection(source, item->toT(item, tClass), collector);
    }

    /**
     * 拷贝
     */
    public static <C extends Collection<T>, T, E> C toCollection(Collection<E> source, Function<? super E, ? extends T> function, Collector<? super T, ?, C> collector){
        if (CollectionUtils.isEmpty(source)){
            return null;
        }
        return source.stream().map(function).collect(collector);
    }

}