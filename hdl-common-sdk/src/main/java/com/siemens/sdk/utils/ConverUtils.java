package com.siemens.sdk.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

/**
 * 对象转换
 * @author wang. shuai
 */
public class ConverUtils {

    @SneakyThrows
    public static <T> T convertObject (String o, Class<T> beanClass) {
        if (o == null) {
            return null;
        }
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(o, beanClass);
    }

    @SneakyThrows
    public static <T> T convertObject (String o, TypeReference<T> toValueTypeRef) {
        if (o == null) {
            return null;
        }
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(o, toValueTypeRef);
    }

    public static <T> T convertValue (Object o, Class<T> beanClass) {
        if (o == null) {
            return null;
        }
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(o, beanClass);
    }

    public static <T> T convertValue (Object o, TypeReference<T> toValueTypeRef) {
        if (o == null) {
            return null;
        }
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(o, toValueTypeRef);
    }

    @SneakyThrows
    public static String convertString(Object o) {
        if (o == null) {
            return null;
        }
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(o);
    }

}