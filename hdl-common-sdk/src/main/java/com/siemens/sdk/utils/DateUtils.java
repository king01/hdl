package com.siemens.sdk.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 * @author wang. shuai
 */
public class DateUtils {

    /**
     * 每天的毫秒数 8640000.
     */
    public static final long MILLISECONDS_PER_DAY = 86400000L;

    /**
     * 仅显示年月日，例如 2020-11-11.
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * 显示年月日时分秒，例如 2020-11-11 16:51:53.
     */
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 仅显示时分秒，例如 16:51:53.
     */
    public static final String TIME_FORMAT = "HH:mm:ss";

    /**
     * 计算年龄
     * @param birthday 生日
     * @return 年龄
     */
    public static int getAgeByBirth(Date birthday) {
        int age = 0;
        try {
            Calendar now = Calendar.getInstance();
            now.setTime(new Date());// 当前时间

            Calendar birth = Calendar.getInstance();
            birth.setTime(birthday);

            // 如果传入的时间，在当前时间的后面，返回0岁
            if (!birth.after(now)) {
                age = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
                if (now.get(Calendar.DAY_OF_YEAR) > birth.get(Calendar.DAY_OF_YEAR)) {
                    age += 1;
                }
            }
            return age;
        } catch (Exception e) {//兼容性更强,异常后返回数据
            return 0;
        }
    }


    /**
     * 获取日期时间字符串，默认格式为（yyyy-MM-dd）.
     * @param date 需要转化的日期时间
     */
    public static String formatDate(Date date) {
        return formatDate(date, DATE_FORMAT);
    }

    public static String formatDateTime(Date date) {
        return formatDate(date, DATETIME_FORMAT);
    }

    /**
     * 获取日期时间字符串
     * @param date 需要转化的日期时间
     * @param pattern 时间格式，例如"yyyy-MM-dd" "HH:mm:ss" "E"等
     */
    public static String formatDate(Date date, String pattern) {
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }

    /**
     * yyyy-MM-dd HH:mm:ss
     * @param str 如：2020-11-11 16:51:53
     */
    public static Date parseDate(String str) {
        return parseDate(str, DATETIME_FORMAT);
    }

    /**
     * 将日期型字符串转换为日期格式.
     * 支持的日期字符串格式包括"yyyy-MM-dd","yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
     * "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm"
     * @param str 如：2020-11-11 16:51:53
     * @param pattern 如：yyyy-MM-dd HH:mm:ss
     */
    public static Date parseDate(String str, String pattern) {
        if (str == null) {
            return null;
        }
        try {
            DateFormat df = new SimpleDateFormat(pattern);
            return df.parse(str);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 计算两个日期之间相差天数.
     * @param start     计算开始日期
     * @param end       计算结束日期
     * @return long 相隔天数
     * @since 1.0
     */
    public static long getDaysBetween(Date start, Date end) {
        long diff = 0;
        if (start != null && end != null) {
            diff = (end.getTime() - start.getTime()) / DateUtils.MILLISECONDS_PER_DAY;
        }
        return diff;
    }

    /**
     * 判断是否是同一天时间
     */
    public static boolean isSameDay(Date d1,Date d2) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(d1);
        c2.setTime(d2);
        return (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
                && (c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH))
                && (c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH));
    }

    private static Calendar calendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar;
    }

    /**
     * 一天的开始
     */
    public static Date startDay(Date date){
        Calendar calendar = calendar(date);
        return calendar.getTime();
    }

    /**
     * 一天的结束
     */
    public static Date endDay(Date date){
        Calendar calendar = calendar(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.SECOND, -1);
        return calendar.getTime();
    }

}
