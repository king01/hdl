package com.siemens.sdk.utils;

import lombok.Getter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 加密算法
 * @author wang. shuai
 */
public class EncryptUtils {

    public static String MD5(String str){
        byte[] encrypt = encrypt(str.getBytes(), EncryptType.MD5);
        if (encrypt == null) {
            return null;
        }
        return byteToHexString(encrypt).toLowerCase();
    }

    public static String MD5(byte[] encrypt){
        return byteToHexString(encrypt).toLowerCase();
    }

    private static byte[] encrypt(byte[] data, EncryptType encryptType){
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(encryptType.getName());

            messageDigest.update(data);
            return messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    private static String byteToHexString(byte[] b) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            String hex = Integer.toHexString(b[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            hexString.append(hex.toUpperCase());
        }
        return hexString.toString();
    }

    public static enum EncryptType{

        /**
         * MD2算法
         */
        MD2("MD2"),

        /**
         * MD5算法
         */
        MD5("MD5"),


        SHA1("SHA-1"),

        SHA256("SHA-256"),

        SHA384("SHA-384"),

        SHA512("SHA-512");

        @Getter
        private String name;

        EncryptType(String name){
            this.name = name;
        }

    }

//    public static void main(String[] args) {
//        String key = MD5("shzl");
//        System.out.println(key);
//        String secret = MD5("shzl-password");
//        System.out.println(secret);
//    }

}