package com.siemens.sdk.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 身份证解析
 * @author wang. shuai
 */
public class IdCardUtils {

    /** 中国公民身份证号码最小长度。 */
    private static final int CHINA_ID_MIN_LENGTH = 15;

    /** 中国公民身份证号码最大长度。 */
    private static final int CHINA_ID_MAX_LENGTH = 18;

    /**
     * 根据身份编号获取年龄
     * @param idCard 身份编号
     * @return 年龄
     */
    public static int getAgeByIdCard(String idCard) {
        Calendar cal = Calendar.getInstance();
        Integer year = getYearByIdCard(idCard);
        int iCurrYear = cal.get(Calendar.YEAR);
        return iCurrYear - year;
    }

    /**
     * 根据身份编号获取生日
     * @param idCard 身份编号
     * @return 生日(yyyyMMdd)
     */
    public static String getBirthByIdCard(String idCard) {
        return idCard.substring(6, 14);
    }

    /**
     * 根据身份编号获取生日年
     * @param idCard 身份编号
     * @return 生日
     */
    public static Integer getYearByIdCard(String idCard) {
        if (idCard.length() == CHINA_ID_MIN_LENGTH){
            String year = "19" + idCard.substring(6, 8);
            return Integer.valueOf(year);
        }
        return Integer.valueOf(idCard.substring(6, 10));
    }

    /**
     * 根据身份编号获取生日月
     * @param idCard 身份编号
     * @return 生日
     */
    public static Integer getMonthByIdCard(String idCard) {
        if (idCard.length() == CHINA_ID_MIN_LENGTH){
            return Integer.valueOf(idCard.substring(8, 10));
        }
        return Integer.valueOf(idCard.substring(10, 12));
    }

    /**
     * 根据身份编号获取生日天
     * @param idCard 身份编号
     * @return 生日
     */
    public static Integer getDayByIdCard(String idCard) {
        if (idCard.length() == CHINA_ID_MIN_LENGTH){
            return Integer.valueOf(idCard.substring(10, 12));
        }
        return Integer.valueOf(idCard.substring(12, 14));
    }

    /**
     * 根据身份编号获取生日
     * @param idCard 身份编号
     * @return 生日
     */
    public static Date getDateByIdCard(String idCard) {
        try {
            String birthday = getBirthByIdCard(idCard);
            DateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            //定义一个时间转换格式“年月日”
            //捕获类型转换（解析）异常
            return fmt.parse(birthday);
        } catch (ParseException e) {
            return null;
        }
    }


    /**
     * 根据身份编号获取性别
     * @param idCard 身份编号
     * @return 性别 1:男，2:女，其他数字:未知
     */
    public static Integer getGenderByIdCard(String idCard) {
        String sCardNum = (idCard.length() == CHINA_ID_MIN_LENGTH)? idCard.substring(13, 15) : idCard.substring(15, 17);
        // 1:男; 2:女
        return (Integer.parseInt(sCardNum) % 2 != 0) ? 1 : 2;
    }

}
