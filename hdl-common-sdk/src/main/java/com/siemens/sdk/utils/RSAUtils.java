package com.siemens.sdk.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Base64Utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * RSA工具类
 * @author wang. shuai
 */
@Slf4j
public class RSAUtils {

    /**
     * 随机生成密钥对
     */
    public static RSAKeyPair genKeyPair() {
        // KeyPairGenerator类用于生成公钥和私钥对，基于RSA算法生成对象
        KeyPairGenerator keyPairGen;
        try {
            keyPairGen = KeyPairGenerator.getInstance("RSA");
            // 初始化密钥对生成器，密钥大小为96-1024位
            assert keyPairGen != null;
            keyPairGen.initialize(1024, new SecureRandom());
            // 生成一个密钥对，保存在keyPair中
            KeyPair keyPair = keyPairGen.generateKeyPair();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();   // 得到私钥
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();  // 得到公钥
            String publicKeyString = new String(Base64Utils.encode(publicKey.getEncoded()));
            // 得到私钥字符串
            String privateKeyString = new String(Base64Utils.encode((privateKey.getEncoded())));
            // 将公钥和私钥保存到Map
            return new RSAKeyPair(publicKeyString, privateKeyString);
        } catch (NoSuchAlgorithmException e) {
            log.error("随机生成密钥对失败", e);
            return null;
        }
    }
    
    /** RSA公钥加密
     * @param str  加密字符串
     * @param publicKey  公钥
     * @return  密文
     */
    public static String encrypt(String str, String publicKey) {
        //base64编码的公钥
        byte[] decoded = Base64Utils.decode(publicKey.getBytes());
        RSAPublicKey pubKey;
        try {
            pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            return new String(Base64Utils.encode(cipher.doFinal(str.getBytes(StandardCharsets.UTF_8))));
        } catch (InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException e) {
            log.error("RSA公钥加密失败", e);
            return null;
        }
    }
    
    /**
     * RSA私钥解密
     * @param str 加密字符串
     * @param privateKey  私钥
     * @return 铭文
     */
    public static String decrypt(String str, String privateKey) {
        //64位解码加密后的字符串
        byte[] inputByte = Base64Utils.decode(str.getBytes(StandardCharsets.UTF_8));
        //base64编码的私钥
        byte[] decoded = Base64Utils.decode(privateKey.getBytes());
        RSAPrivateKey priKey;
        //RSA解密
        Cipher cipher;
        try {
            priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded));
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, priKey);
            return new String(cipher.doFinal(inputByte));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
            log.error("RSA私钥解密失败", e);
            return null;
        }
    }

    /**
     * 密钥对
     * @author hao.yan
     */
    @Data
    @AllArgsConstructor
    static class RSAKeyPair {

        /**
         * 公钥
         */
        private String publicKey;

        /**
         * 私钥
         */
        private String privateKey;

    }

//    public static void main(String[] args) {
//        //生成公钥和私钥
//        RSAKeyPair keyPair = genKeyPair();
//        if (keyPair == null){
//            return;
//        }
//        //加密字符串
//        String message = "123456";
//        String mysqlEn = encrypt(message, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDiLvB0uByPOVS7mGWTebX21o7+/bERaRVq8M3L3v3ZaVW7WaOg/3QYD4nEGCU7Risvanv20stszgErWY/z9RxvxK1aSXoqf8JsSnUXlQDALd2zTaoUr/fnyjXKMRckIa+wGa6WTSBVrkv5TtkzEfg3O/f8qjSi1MgIpKrK00FlGwIDAQAB");
//        System.out.println("加密后的字符串为:" + mysqlEn);
//        System.out.println("随机生成的公钥为:" + keyPair.getPublicKey());
//        System.out.println("随机生成的私钥为:" + keyPair.getPrivateKey());
//        String messageEn = encrypt(message, keyPair.getPublicKey());
//        if (ObjectUtils.isEmpty(messageEn)){
//            return;
//        }
//        System.out.println("加密后的字符串为:" + messageEn);
//        String messageDe = decrypt(messageEn, keyPair.getPrivateKey());
//        System.out.println("还原后的字符串为:" + messageDe);
//    }

}