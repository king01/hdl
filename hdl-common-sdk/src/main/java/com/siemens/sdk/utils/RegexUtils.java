package com.siemens.sdk.utils;

import java.util.regex.Pattern;

/**
 * 正则验证
 * @author wang. shuai
 */
public class RegexUtils {

    public final static String MOBILE_PATTERN = "^[1]\\d{10}$";

    public final static String EMAIL_PATTERN = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

    public final static String USERNAME_PATTERN = "^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){4,19}$";

    public final static String PASSWORD_PATTERN = "^([A-Z]|[a-z]|[0-9]|[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）――+|{}【】‘；：”“'。，、？]){6,32}$";

    public final static String ID_CARD_PATTERN = "^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([\\d|x|X]{1})$";

    public static final String TEL = "^0[1-9](\\d{1,2}\\-?)\\d{7,8}";

    public static final String TEL_MOBILE = "^(0[1-9](\\d{1,2}\\-?)\\d{7,8})|([1]\\d{10})$";

    public static boolean validate(String value, String regex) {
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(value).matches();
    }

    /**
     *  正则：手机号（简单）, 1字头＋10位数字即可.
     * @param mobile 手机号
     */
    public static boolean validateMobile(String mobile) {
        return validate(mobile, MOBILE_PATTERN);
    }

    /**
     *  正则：邮箱
     * @param email 邮箱
     */
    public static boolean validateEmail(String email) {
        return validate(email, EMAIL_PATTERN);
    }

    /**
     *  正则：username
     * @param username 用户名
     */
    public static boolean validateUsername(String username) {
        return validate(username, USERNAME_PATTERN);
    }

    /**
     *  正则：password
     * @param password 密码
     */
    public static boolean validatePassword(String password) {
        return validate(password, PASSWORD_PATTERN);
    }

}
