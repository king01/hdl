package com.siemens.sdk.utils;

/**
 * 字符串工具
 * @author wang. shuai
 */
public class StringUtils {

    /**
     * 判断字符串为空
     */
    public static boolean isEmpty(Object str) {
        return (str == null || "".equals(str));
    }

    /**
     * 判断字符串不为空
     */
    public static boolean isNotEmpty(Object str) {
        return !isEmpty(str);
    }

}
