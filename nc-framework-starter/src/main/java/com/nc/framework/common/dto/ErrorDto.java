package com.nc.framework.common.dto;

import java.util.Objects;

/**
 * 校验失败后，向前台返回的错误信息
 *
 * @author zjc
 * @date 2021-05-30
 */
public class ErrorDto {

    /**
     * 字段名
     */
    private String filedName;

    /**
     * 校验信息
     */
    private String errorMsg;

    public ErrorDto() {
    }

    public ErrorDto(String filedName, String errorMsg) {
        this.filedName = filedName;
        this.errorMsg = errorMsg;
    }

    public String getFiledName() {
        return filedName;
    }

    public void setFiledName(String filedName) {
        this.filedName = filedName;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ErrorDto errorDto = (ErrorDto) o;
        return Objects.equals(filedName, errorDto.filedName) && Objects.equals(errorMsg, errorDto.errorMsg);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filedName, errorMsg);
    }

    @Override
    public String toString() {
        return "ErrorDto{" +
                "filedName='" + filedName + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
