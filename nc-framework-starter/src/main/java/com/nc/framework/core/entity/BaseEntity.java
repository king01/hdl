package com.nc.framework.core.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * 数据库实体类统一父Entity类
 *
 * @author zjc
 * @date 2021-05-30
 */
public class BaseEntity implements Serializable {

    /**
     * 创建者编号
     */
    private Long creator;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新者编号
     */
    private Long updated;

    /**
     * 更新时间
     */
    private Long updatedTime;

    public BaseEntity() {
    }

    public BaseEntity(Long creator, Long createTime, Long updated, Long updatedTime) {
        this.creator = creator;
        this.createTime = createTime;
        this.updated = updated;
        this.updatedTime = updatedTime;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public Long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Long updatedTime) {
        this.updatedTime = updatedTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BaseEntity that = (BaseEntity) o;
        return Objects.equals(creator, that.creator) && Objects.equals(createTime, that.createTime) && Objects.equals(updated, that.updated) && Objects.equals(updatedTime, that.updatedTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creator, createTime, updated, updatedTime);
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "creator=" + creator +
                ", createTime=" + createTime +
                ", updated=" + updated +
                ", updatedTime=" + updatedTime +
                '}';
    }
}
