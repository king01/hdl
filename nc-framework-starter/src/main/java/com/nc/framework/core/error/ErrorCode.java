package com.nc.framework.core.error;

/**
 * @author wang. shuai
 *
 */
public interface ErrorCode {

    int SUCCESS_CODE = 200;

    int getStatus();

    String getMessage();
}
