package com.nc.framework.core.handler;

import com.nc.framework.common.dto.ErrorDto;
import com.nc.framework.core.web.Result;
import com.nc.framework.core.web.ResultCode;
import com.nc.framework.core.web.ResultHelper;
import com.nc.framework.exception.BusinessException;
import com.nc.framework.exception.HdlException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 处理全局的Api调用异常时的异常处理类
 *
 * @author zjc
 * @date 2021-05-30
 */
@RestControllerAdvice
public class ApiExceptionHandler {

    /**
     * 请求参数校验异常处理
     *
     * @param exception 参数校验异常
     * @return Result
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public Result validateParameterExceptionHandler(MethodArgumentNotValidException exception) {

        Set<String> ignoredSet = new HashSet<>();

        BindingResult result = exception.getBindingResult();
        List<FieldError> list = result.getFieldErrors();

        List<ErrorDto> errorList = new ArrayList<>();

        ErrorDto errorDto = null;

        for (FieldError error : list) {

            if (!ignoredSet.contains(error.getField())) {
                errorDto = new ErrorDto(error.getField(), error.getDefaultMessage());
                errorList.add(errorDto);
                ignoredSet.add(error.getField());
            }
        }

        return ResultHelper.error(errorList);
    }

    /**
     * 系统对应的海底捞异常处理
     *
     * @param e 海底捞异常
     * @return Result
     */
    @ExceptionHandler(HdlException.class)
    public Result hdlExceptionHandler(HdlException e) {
        return ResultHelper.fail(ResultCode.INTERNAL_SERVER_ERROR, e.getMessage());
    }

    /**
     * 系统对应的海底捞业务异常处理
     *
     * @param e 海底捞业务异常
     * @return Result
     */
    @ExceptionHandler(BusinessException.class)
    public Result businessExceptionHandler(BusinessException e) {
        return ResultHelper.fail(e.getMessage());
    }
}
