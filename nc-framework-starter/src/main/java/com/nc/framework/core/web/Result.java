package com.nc.framework.core.web;

/**
 * 响应消息对象
 *
 * @author zjc
 * @date 2021-05-30
 */
public class Result<T> {

    /**
     * 消息状态码
     */
    private int status;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 消息内容
     */
    private T data;

    public int getStatus() {
        return status;
    }

    public Result<T> setStatus(ResultCode resultCode) {
        this.status = resultCode.getStatus();
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public Result<T> setData(T data) {
        this.data = data;
        return this;
    }

    public Result(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public Result() {

    }

}
