package com.nc.framework.core.web;

import com.nc.framework.core.error.ErrorCode;

/**
 * Http请求的返回状态编码
 *
 * @author zjc
 * @date 2021-05-30
 */
public enum ResultCode implements ErrorCode {

    SUCCESS(200, "HTTP请求成功处理"),
    FAIL(300, "HTTP请求处理失败"),
    PARAMETER_VALIDATED_ERROR(301, "请求参数校验失败"),
    NOT_FOUND(404, "请求接口不存在"),
    UNAUTHORIZED(401, "认证失败"),
    INTERNAL_SERVER_ERROR(500, "服务器内部异常");

    /**
     * 响应码
     */
    private int status;

    /**
     * 响应消息
     */
    private String message;

    ResultCode(int status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * 根据响应码获取到枚举对象
     *
     * @param status 响应码
     * @return 枚举对象
     */
    public static ResultCode getResponseCode(int status) {

        for (ResultCode resultCode : ResultCode.values()) {

            if (resultCode.getStatus() == status) {
                return resultCode;
            }
        }

        return null;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
