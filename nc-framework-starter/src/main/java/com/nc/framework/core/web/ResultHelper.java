package com.nc.framework.core.web;

import com.nc.framework.core.error.ErrorCode;

/**
 * Response响应消息工具类
 *
 * @author zjc
 * @date 2021-05-30
 */
public class ResultHelper {

    /**
     * 成功处理: 使用系统默认的返回参数
     *
     * @return 返回成功消息
     */
    public static Result<Void> success() {
        return new Result().setStatus(ResultCode.SUCCESS).setMessage(ResultCode.SUCCESS.getMessage());
    }

    /**
     * 成功处理: 包含返回对象的响应消息
     *
     * @param data 响应的数据对象
     * @return 返回对象
     */
    public static <T> Result<T> success(T data) {
        return new Result<T>().setStatus(ResultCode.SUCCESS).setMessage(null).setData(data);
    }

    /**
     * 成功处理: 包含Message和返回对象的响应消息
     *
     * @param message 消息信息
     * @param data 响应的数据对象
     * @return 返回对象
     */
    public static <T> Result<T> success(String message, T data) {
        return new Result<T>().setStatus(ResultCode.SUCCESS).setMessage(message).setData(data);
    }

    /**
     * 失败处理：使用系统默认的返回参数
     *
     * @return 返回成功消息
     */
    public static Result fail() {
        return new Result().setStatus(ResultCode.FAIL).setMessage(ResultCode.FAIL.getMessage());
    }

    /**
     * 失败处理: 包含返回对象的响应消息
     *
     * @param errorMsg 错误消息内容
     * @return 返回对象
     */
    public static <T> Result<T> fail(String errorMsg) {
        return new Result<T>().setStatus(ResultCode.FAIL).setMessage(errorMsg);
    }

    /**
     * 失败处理: 包含错误码和返回对象的响应消息
     *
     * @param errorMsg 错误消息内容
     * @return 返回对象
     */
    public static <T> Result<T> fail(ResultCode resultCode, String errorMsg) {
        return new Result<T>().setStatus(resultCode).setMessage(errorMsg);
    }

    /**
     * 失败处理: 包含错误消息和错误数据
     *
     * @param data 错误数据
     * @return 返回对象
     */
    public static <T> Result<T> error(T data) {
        return new Result<T>().setStatus(ResultCode.PARAMETER_VALIDATED_ERROR).setMessage(ResultCode.PARAMETER_VALIDATED_ERROR.getMessage()).setData(data);
    }


    public static <T> Result<T> error(ErrorCode code) {
        return new Result<T>(code.getStatus(), code.getMessage());
    }
}
