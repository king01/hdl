package com.nc.framework.exception;

import com.nc.framework.core.error.ErrorCode;
import com.nc.framework.core.web.ResultCode;

/**
 * 自定义业务异常
 *
 * @author zjc
 * @date 2021-05-30
 */
public class BusinessException extends RuntimeException {

    private int status;

    public BusinessException() {
        this((ErrorCode)ResultCode.SUCCESS);
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(ErrorCode code) {
        this(code.getStatus(), code.getMessage());
    }

    public BusinessException(int status, String message) {
        super(message);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
