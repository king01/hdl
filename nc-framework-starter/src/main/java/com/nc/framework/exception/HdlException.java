package com.nc.framework.exception;

/**
 * HDL系统异常
 *
 * @author zjc
 * @date 2021-05-30
 */
public class HdlException extends RuntimeException {

    public HdlException() {
        super();
    }

    public HdlException(String message) {
        super(message);
    }
}
