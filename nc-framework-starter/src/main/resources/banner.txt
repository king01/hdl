${AnsiColor.BLUE}
##########################################################################################################
   _____    _ _____  _           _____ _  ______
  / / / |  | |  __ \| |         / ____| |/ /\ \ \
 / / /| |__| | |  | | |  ______| |    | ' /  \ \ \
< < < |  __  | |  | | | |______| |    |  <    > > >
 \ \ \| |  | | |__| | |____    | |____| . \  / / /
  \_\_\_|  |_|_____/|______|    \_____|_|\_\/_/_/
=================================================
Application Version: HDL v3.0.0 (v3.0.0)
Spring Boot Version: ${spring-boot.version}${spring-boot.formatted-version}

##########################################################################################################
${AnsiColor.BLACK}